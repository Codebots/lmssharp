/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Lmssharp.Enums;
using Lmssharp.Services;
using GraphQL.Types;
using GraphQL.EntityFramework;
using Microsoft.AspNetCore.Identity;
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

namespace Lmssharp.Models
{
	/// <summary>
	/// The GraphQL type for returning data in GraphQL queries
	/// </summary>
	public class CourseEntityType : EfObjectGraphType<LmssharpDBContext, CourseEntity>
	{
		public CourseEntityType(IEfGraphQLService<LmssharpDBContext> service) : base(service)
		{
			Description = @"A course is a collection of many lessons";

			// Add model fields to type
			Field(o => o.Id, type: typeof(IdGraphType));
			Field(o => o.Created, type: typeof(DateTimeGraphType));
			Field(o => o.Modified, type: typeof(DateTimeGraphType));
			Field(o => o.Name, type: typeof(StringGraphType)).Description(@"Name of the course");
			Field(o => o.Summary, type: typeof(StringGraphType)).Description(@"A short summary of the course");
			Field(o => o.CoverImageId, type: typeof(IdGraphType)).Description(@"A cover image for the course");
			Field(o => o.Difficulty, type: typeof(EnumerationGraphType<Difficulty>)).Description(@"The difficulty of the course");
			// % protected region % [Add any extra GraphQL fields here] off begin
			// % protected region % [Add any extra GraphQL fields here] end

			// Add entity references
			Field(o => o.CourseCategoryId, type: typeof(IdGraphType));

			// GraphQL reference to entity CourseLessonsEntity via reference CourseLessons
			IEnumerable<CourseLessonsEntity> CourseLessonssResolveFunction(ResolveFieldContext<CourseEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<CourseLessonsEntity>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.CourseLessonss.Where(filter.Compile());
			}
			AddNavigationListField("CourseLessonss", (Func<ResolveFieldContext<CourseEntity>, IEnumerable<CourseLessonsEntity>>) CourseLessonssResolveFunction);
			AddNavigationConnectionField("CourseLessonssConnection", CourseLessonssResolveFunction);

			// GraphQL reference to entity CourseCategoryEntity via reference CourseCategory
			AddNavigationField("CourseCategory", context => {
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<CourseCategoryEntity>(
					graphQlContext.IdentityService,
					graphQlContext.UserManager,
					graphQlContext.DbContext,
					graphQlContext.ServiceProvider);
				var value = context.Source.CourseCategory;

				if (value != null)
				{
					return new List<CourseCategoryEntity> {value}.All(filter.Compile()) ? value : null;
				}
				return null;
			});

			// % protected region % [Add any extra GraphQL references here] off begin
			// % protected region % [Add any extra GraphQL references here] end
		}
	}

	/// <summary>
	/// The GraphQL input type for mutation input
	/// </summary>
	public class CourseEntityInputType : InputObjectGraphType<CourseEntity>
	{
		public CourseEntityInputType()
		{
			Name = "CourseEntityInput";
			Description = "The input object for adding a new CourseEntity";

			// Add entity fields
			Field<IdGraphType>("Id");
			Field<DateTimeGraphType>("Created");
			Field<DateTimeGraphType>("Modified");
			Field<StringGraphType>("Name").Description = @"Name of the course";
			Field<StringGraphType>("Summary").Description = @"A short summary of the course";
			Field(o => o.CoverImageId, type: typeof(IdGraphType)).Description(@"A cover image for the course");
			Field<EnumerationGraphType<Difficulty>>("Difficulty").Description = @"The difficulty of the course";

			// Add entity references
			Field<IdGraphType>("CourseCategoryId");

			// Add references to foreign models to allow nested creation
			Field<ListGraphType<CourseLessonsEntityInputType>>("CourseLessonss");
			Field<CourseCategoryEntityInputType>("CourseCategory");

			// % protected region % [Add any extra GraphQL input fields here] off begin
			// % protected region % [Add any extra GraphQL input fields here] end
		}
	}

}