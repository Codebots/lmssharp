/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Linq;
using System.Collections.Generic;
using Lmssharp.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
// % protected region % [Add any extra imports here] off begin
// % protected region % [Add any extra imports here] end

namespace Lmssharp.Models
{
	/// <summary>
	/// A course is a collection of many lessons
	/// </summary>
	public class CourseEntityDto : ModelDto<CourseEntity>
	{
		// % protected region % [Customise Name here] off begin
		/// <summary>
		/// Name of the course
		/// </summary>
		public String Name { get; set; }
		// % protected region % [Customise Name here] end

		// % protected region % [Customise Summary here] off begin
		/// <summary>
		/// A short summary of the course
		/// </summary>
		public String Summary { get; set; }
		// % protected region % [Customise Summary here] end

		// % protected region % [Customise CoverImage here] off begin
		/// <summary>
		/// A cover image for the course
		/// </summary>
		public Guid? CoverImageId { get; set; }
		// % protected region % [Customise CoverImage here] end

		// % protected region % [Customise Difficulty here] off begin
		/// <summary>
		/// The difficulty of the course
		/// </summary>
		[JsonProperty("difficulty")]
		[JsonConverter(typeof(StringEnumConverter))]
		public Difficulty Difficulty { get; set; }
		// % protected region % [Customise Difficulty here] end


		// % protected region % [Customise CourseCategoryId here] off begin
		public Guid? CourseCategoryId { get; set; }
		// % protected region % [Customise CourseCategoryId here] end

		// % protected region % [Add any extra attributes here] off begin
		// % protected region % [Add any extra attributes here] end

		public CourseEntityDto(CourseEntity model)
		{
			LoadModelData(model);
			// % protected region % [Add any constructor logic here] off begin
			// % protected region % [Add any constructor logic here] end
		}

		public CourseEntityDto()
		{
			// % protected region % [Add any parameterless constructor logic here] off begin
			// % protected region % [Add any parameterless constructor logic here] end
		}

		public override CourseEntity ToModel()
		{
			// % protected region % [Add any extra ToModel logic here] off begin
			// % protected region % [Add any extra ToModel logic here] end

			return new CourseEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Name = Name,
				Summary = Summary,
				CoverImageId = CoverImageId,
				Difficulty = Difficulty,
				CourseCategoryId  = CourseCategoryId,
				// % protected region % [Add any extra model properties here] off begin
				// % protected region % [Add any extra model properties here] end
			};
		}

		public override ModelDto<CourseEntity> LoadModelData(CourseEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Name = model.Name;
			Summary = model.Summary;
			CoverImageId = model.CoverImageId;
			Difficulty = model.Difficulty;
			CourseCategoryId  = model.CourseCategoryId;

			// % protected region % [Add any extra loading data logic here] off begin
			// % protected region % [Add any extra loading data logic here] end

			return this;
		}
	}
}