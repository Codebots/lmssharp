/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Lmssharp.Helpers;
using Lmssharp.Models.Interfaces;
using Lmssharp.Enums;
using Lmssharp.Security;
using Lmssharp.Security.Acl;
using Lmssharp.Validators;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

namespace Lmssharp.Models {
	// % protected region % [Configure entity attributes here] off begin
	[Table("Article")]
	// % protected region % [Configure entity attributes here] end
	public class ArticleEntity : IOwnerAbstractModel, ITimelineEntity 	{
		[Key]
		public Guid Id { get; set; }
		public Guid Owner { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }

		/// <summary>
		/// Title of the article
		/// </summary>
		// % protected region % [Customise Title here] off begin
		[EntityAttribute]
		public String Title { get; set; }
		// % protected region % [Customise Title here] end

		/// <summary>
		/// Short summary of the article
		/// </summary>
		// % protected region % [Customise Summary here] off begin
		[EntityAttribute]
		public String Summary { get; set; }
		// % protected region % [Customise Summary here] end

		/// <summary>
		/// The main content of the article
		/// </summary>
		// % protected region % [Customise Content here] off begin
		[EntityAttribute]
		public String Content { get; set; }
		// % protected region % [Customise Content here] end

		[NotMapped]
		public List<Guid> WorkflowBehaviourStateIds { get; set; }

		// % protected region % [Add any further attributes here] off begin
		// % protected region % [Add any further attributes here] end

		public ArticleEntity()
		{
			// % protected region % [Add any constructor logic here] off begin
			// % protected region % [Add any constructor logic here] end
		}

		// % protected region % [Customise ACL attributes here] off begin
		[NotMapped]
		[JsonIgnore]
		// % protected region % [Customise ACL attributes here] end
		public IEnumerable<IAcl> Acls => new List<IAcl>
		{
			// % protected region % [Override ACLs here] off begin
			new SuperAdministratorsScheme(),
			new AdministratorArticleEntity(),
			new VisitorsArticleEntity(),
			new UserArticleEntity(),
			// % protected region % [Override ACLs here] end
			// % protected region % [Add any further ACL entries here] off begin
			// % protected region % [Add any further ACL entries here] end
		};

		// % protected region % [Customise Book here] off begin
		/// <summary>
		/// Outgoing one to many reference
		/// </summary>
		/// <see cref="Lmssharp.Models.BookEntity"/>
		public Guid? BookId { get; set; }
		[EntityForeignKey("Book", "Articless", false, typeof(BookEntity))]
		public BookEntity Book { get; set; }
		// % protected region % [Customise Book here] end

		// % protected region % [Customise CreatedBy here] off begin
		/// <summary>
		/// Outgoing one to many reference
		/// </summary>
		/// <see cref="Lmssharp.Models.UserEntity"/>
		public Guid? CreatedById { get; set; }
		[EntityForeignKey("CreatedBy", "CreatedArticles", false, typeof(UserEntity))]
		public UserEntity CreatedBy { get; set; }
		// % protected region % [Customise CreatedBy here] end

		// % protected region % [Customise UpdatedBy here] off begin
		/// <summary>
		/// Outgoing one to many reference
		/// </summary>
		/// <see cref="Lmssharp.Models.UserEntity"/>
		public Guid? UpdatedById { get; set; }
		[EntityForeignKey("UpdatedBy", "UpdatedArticles", false, typeof(UserEntity))]
		public UserEntity UpdatedBy { get; set; }
		// % protected region % [Customise UpdatedBy here] end

		// % protected region % [Customise LoggedEvents here] off begin
		/// <summary>
		/// Incoming one to many reference
		/// </summary>
		/// <see cref="Lmssharp.Models.ArticleTimelineEventsEntity"/>
		[EntityForeignKey("LoggedEvents", "Entity", false, typeof(ArticleTimelineEventsEntity))]
		public ICollection<ArticleTimelineEventsEntity> LoggedEvents { get; set; }
		// % protected region % [Customise LoggedEvents here] end

		// % protected region % [Customise Articless here] off begin
		/// <summary>
		/// Outgoing many to many reference
		/// </summary>
		/// <see cref="Lmssharp.Models.ArticlesTags"/>
		[EntityForeignKey("Articless", "Tags", false, typeof(ArticlesTags))]
		public ICollection<ArticlesTags> Articless { get; set; }
		// % protected region % [Customise Articless here] end

		// % protected region % [Customise WorkflowStatess here] off begin
		/// <summary>
		/// Incoming many to many reference
		/// </summary>
		/// <see cref="Lmssharp.Models.ArticleWorkflowStates"/>
		[EntityForeignKey("WorkflowStatess", "Article", false, typeof(ArticleWorkflowStates))]
		public ICollection<ArticleWorkflowStates> WorkflowStatess { get; set; }
		// % protected region % [Customise WorkflowStatess here] end

		public async Task BeforeSave(
			EntityState operation,
			LmssharpDBContext dbContext,
			IServiceProvider serviceProvider,
			CancellationToken cancellationToken = default)
		{
			// % protected region % [Add any initial before save logic here] off begin
			// % protected region % [Add any initial before save logic here] end

			// Create timeline event when an entity is added
			if (operation == EntityState.Added)
			{
				await CreateTimelineCreateEventsAsync(dbContext, serviceProvider, cancellationToken);
			}

			// Create a timeline event when an entity is modified
			if (operation == EntityState.Modified)
			{
				var original = await dbContext.ArticleEntity
					.AsNoTracking()
					.Where(x => x.Id == Id)
					.FirstOrDefaultAsync(cancellationToken);
				await CreateTimelineEventsAsync(original, dbContext, serviceProvider, cancellationToken);
			}

			// Create a timeline event when the entity is going to be deleted
			if (operation == EntityState.Deleted)
			{
				await CreateTimelineDeleteEventsAsync(dbContext, serviceProvider, cancellationToken);
			}
			if ((operation == EntityState.Added || operation == EntityState.Modified) && WorkflowBehaviourStateIds != null)
			{
				// Any states that are associated with this entity are going to be untracked so we can rewrite them
				var existingAssociations = dbContext.ChangeTracker
					.Entries<ArticleWorkflowStates>()
					.Where(e => e.Entity.ArticleId == Id)
					.ToList();

				// Only add states that are published and associated with this entity
				var statesToAdd = dbContext.WorkflowStateEntity
					.AsNoTracking()
					.Include(s => s.WorkflowVersion)
					.ThenInclude(v => v.Workflow)
					.Where(s => WorkflowBehaviourStateIds.Contains(s.Id))
					.Where(s => s.WorkflowVersion.ArticleAssociation == true)
					.Where(s => s.WorkflowVersion.Id == s.WorkflowVersion.Workflow.CurrentVersionId)
					.ToList();

				foreach (var association in existingAssociations)
				{
					if (statesToAdd.Select(s => s.WorkflowVersionId).Contains(association.Entity.WorkflowStatesId))
					{
						association.State = EntityState.Unchanged;
					}
				}

				// Delete any old states that are going to be replaced
				var statesToDelete = dbContext.ArticleWorkflowStates
					.AsNoTracking()
					.Include(s => s.WorkflowStates)
					.ThenInclude(s => s.WorkflowVersion)
					.Where(s => statesToAdd.Select(a => a.WorkflowVersionId).Contains(s.WorkflowStates.WorkflowVersionId) && s.ArticleId == Id)
					.ToList();

				foreach (var workflowState in dbContext.ChangeTracker.Entries<ArticleWorkflowStates>())
				{
					if (statesToDelete.Select(s => s.Id).Contains(workflowState.Entity.Id))
					{
						workflowState.State = EntityState.Detached;
					}
				}
				dbContext.ArticleWorkflowStates.RemoveRange(statesToDelete);

				dbContext.ArticleWorkflowStates.AddRange(statesToAdd.Select(s => new ArticleWorkflowStates
				{
					Owner = Owner,
					ArticleId = Id,
					WorkflowStatesId = s.Id,
				}));

				var timelineEvents = new List<ITimelineEventEntity>();

				var newStates = statesToAdd
					.Where(s => !statesToDelete.Select(d => d.WorkflowStates.Id).Contains(s.Id));

				foreach (var newState in newStates)
				{
					var previousState = statesToDelete.FirstOrDefault(x =>
						x.WorkflowStates.WorkflowVersionId == newState.WorkflowVersionId)?.WorkflowStates;

					if (previousState != null)
					{
						timelineEvents.AddWorkflowEvent<ArticleTimelineEventsEntity>(
							newState.WorkflowVersion.WorkflowName,
							previousState.StepName,
							newState.StepName,
							Id);
					}
					// % protected region % [Add any custom workflow logic here] off begin
					// % protected region % [Add any custom workflow logic here] end
				}
				await dbContext.AddRangeAsync(timelineEvents, cancellationToken);
			}

			// % protected region % [Add any before save logic here] off begin
			// % protected region % [Add any before save logic here] end
		}

		public async Task AfterSave(
			EntityState operation,
			LmssharpDBContext dbContext,
			IServiceProvider serviceProvider,
			ICollection<ChangeState> changes,
			CancellationToken cancellationToken = default)
		{
			// % protected region % [Add any initial after save logic here] off begin
			// % protected region % [Add any initial after save logic here] end

			// % protected region % [Add any after save logic here] off begin
			// % protected region % [Add any after save logic here] end
		}

		public async Task<int> CleanReference<T>(
			string reference,
			IEnumerable<T> models,
			LmssharpDBContext dbContext,
			CancellationToken cancellation = default)
			where T : IOwnerAbstractModel
		{
			var modelList = models.Cast<ArticleEntity>().ToList();
			var ids = modelList.Select(t => t.Id).ToList();

			switch (reference)
			{
				case "LoggedEvents":
					var loggedEventIds = modelList.SelectMany(x => x.LoggedEvents.Select(m => m.Id)).ToList();
					var oldloggedEvent = await dbContext.ArticleTimelineEventsEntity
						.Where(m => m.EntityId.HasValue && ids.Contains(m.EntityId.Value))
						.Where(m => !loggedEventIds.Contains(m.Id))
						.ToListAsync(cancellation);

					foreach (var loggedEvent in oldloggedEvent)
					{
						loggedEvent.EntityId = null;
					}

					dbContext.ArticleTimelineEventsEntity.UpdateRange(oldloggedEvent);
					return oldloggedEvent.Count;
				case "Articless":
					var articlesEntities = modelList
						.SelectMany(m => m.Articless)
						.Select(m => m.Id);
					var oldArticles = await dbContext.ArticlesTags
						.Where(m => ids.Contains(m.TagsId) && !articlesEntities.Contains(m.Id))
						.ToListAsync(cancellation);
					dbContext.ArticlesTags.RemoveRange(oldArticles);

					return oldArticles.Count;
				case "WorkflowStatess":
					var workflowStatesEntities = modelList
						.SelectMany(m => m.WorkflowStatess)
						.Select(m => m.Id);
					var oldWorkflowStates = await dbContext.ArticleWorkflowStates
						.Where(m => ids.Contains(m.ArticleId) && !workflowStatesEntities.Contains(m.Id))
						.ToListAsync(cancellation);
					dbContext.ArticleWorkflowStates.RemoveRange(oldWorkflowStates);

					return oldWorkflowStates.Count;
				// % protected region % [Add any extra clean reference logic here] off begin
				// % protected region % [Add any extra clean reference logic here] end
				default:
					return 0;
			}
		}

		// % protected region % [Add any further references here] off begin
		// % protected region % [Add any further references here] end

		// % protected region % [Override CreateTimelineEventsAsync method here] off begin
		public async Task CreateTimelineEventsAsync<TEntity>(
			TEntity original,
			LmssharpDBContext dbContext,
			IServiceProvider serviceProvider,
			CancellationToken cancellationToken = default)
			where TEntity : IOwnerAbstractModel
		// % protected region % [Override CreateTimelineEventsAsync method here] end
		{
			// % protected region % [Override CreateTimelineEventsAsync type check here] off begin
			if (!(original is ArticleEntity originalEntity))
			{
				return;
			}
			// % protected region % [Override CreateTimelineEventsAsync type check here] end

			var timelineEvents = new List<ITimelineEventEntity>();

			// % protected region % [Override CreateTimelineEventsAsync 'Title' case here] off begin
			timelineEvents.ConditionalAddUpdateEvent<ArticleTimelineEventsEntity>(
				"ArticleEntity",
				"Title",
				 originalEntity.Title,
				 Title,
				 Id);
			// % protected region % [Override CreateTimelineEventsAsync 'Title' case here] end
			// % protected region % [Override CreateTimelineEventsAsync 'Summary' case here] off begin
			timelineEvents.ConditionalAddUpdateEvent<ArticleTimelineEventsEntity>(
				"ArticleEntity",
				"Summary",
				 originalEntity.Summary,
				 Summary,
				 Id);
			// % protected region % [Override CreateTimelineEventsAsync 'Summary' case here] end
			// % protected region % [Override CreateTimelineEventsAsync 'Content' case here] off begin
			timelineEvents.ConditionalAddUpdateEvent<ArticleTimelineEventsEntity>(
				"ArticleEntity",
				"Content",
				 originalEntity.Content,
				 Content,
				 Id);
			// % protected region % [Override CreateTimelineEventsAsync 'Content' case here] end
			// % protected region % [Override CreateTimelineEventsAsync 'Book' case here] off begin
			timelineEvents.ConditionalAddUpdateEvent<ArticleTimelineEventsEntity>(
				"ArticleEntity",
				"BookId",
				originalEntity.BookId,
				BookId,
				Id);
			// % protected region % [Override CreateTimelineEventsAsync 'Book' case here] end
			// % protected region % [Override CreateTimelineEventsAsync 'CreatedBy' case here] off begin
			timelineEvents.ConditionalAddUpdateEvent<ArticleTimelineEventsEntity>(
				"ArticleEntity",
				"CreatedById",
				originalEntity.CreatedById,
				CreatedById,
				Id);
			// % protected region % [Override CreateTimelineEventsAsync 'CreatedBy' case here] end
			// % protected region % [Override CreateTimelineEventsAsync 'UpdatedBy' case here] off begin
			timelineEvents.ConditionalAddUpdateEvent<ArticleTimelineEventsEntity>(
				"ArticleEntity",
				"UpdatedById",
				originalEntity.UpdatedById,
				UpdatedById,
				Id);
			// % protected region % [Override CreateTimelineEventsAsync 'UpdatedBy' case here] end

			// % protected region % [Add any further timeline update events here] off begin
			// % protected region % [Add any further timeline update events here] end

			// % protected region % [Override CreateTimelineEventsAsync database call here] off begin
			await dbContext.AddRangeAsync(timelineEvents, cancellationToken);
			// % protected region % [Override CreateTimelineEventsAsync database call here] end
		}

		// % protected region % [Override CreateTimelineCreateEventsAsync here] off begin
		public async Task CreateTimelineCreateEventsAsync(
			LmssharpDBContext dbContext,
			IServiceProvider serviceProvider,
			CancellationToken cancellationToken = default)
		{
			var timelineEvents = new List<ITimelineEventEntity>();
			timelineEvents.AddCreateEvent<ArticleTimelineEventsEntity>("ArticleEntity", Id);
			await dbContext.AddRangeAsync(timelineEvents, cancellationToken);
		}
		// % protected region % [Override CreateTimelineCreateEventsAsync here] end

		// % protected region % [Override CreateTimelineDeleteEventsAsync here] off begin
		public async Task CreateTimelineDeleteEventsAsync(
			LmssharpDBContext dbContext,
			IServiceProvider serviceProvider,
			CancellationToken cancellationToken = default)
		{
			var timelineEvents = new List<ITimelineEventEntity>();
			timelineEvents.AddDeleteEvent<ArticleTimelineEventsEntity>("ArticleEntity", Id);
			await dbContext.AddRangeAsync(timelineEvents, cancellationToken);
		}
		// % protected region % [Override CreateTimelineDeleteEventsAsync here] end
	}
}