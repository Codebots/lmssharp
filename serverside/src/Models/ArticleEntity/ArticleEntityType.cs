/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Lmssharp.Services;
using GraphQL.Types;
using GraphQL.EntityFramework;
using Microsoft.AspNetCore.Identity;
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

namespace Lmssharp.Models
{
	/// <summary>
	/// The GraphQL type for returning data in GraphQL queries
	/// </summary>
	public class ArticleEntityType : EfObjectGraphType<LmssharpDBContext, ArticleEntity>
	{
		public ArticleEntityType(IEfGraphQLService<LmssharpDBContext> service) : base(service)
		{

			// Add model fields to type
			Field(o => o.Id, type: typeof(IdGraphType));
			Field(o => o.Created, type: typeof(DateTimeGraphType));
			Field(o => o.Modified, type: typeof(DateTimeGraphType));
			Field(o => o.Title, type: typeof(StringGraphType)).Description(@"Title of the article");
			Field(o => o.Summary, type: typeof(StringGraphType)).Description(@"Short summary of the article");
			Field(o => o.Content, type: typeof(StringGraphType)).Description(@"The main content of the article");
			// % protected region % [Add any extra GraphQL fields here] off begin
			// % protected region % [Add any extra GraphQL fields here] end

			// Add entity references
			Field(o => o.BookId, type: typeof(IdGraphType));
			Field(o => o.CreatedById, type: typeof(IdGraphType));
			Field(o => o.UpdatedById, type: typeof(IdGraphType));

			// GraphQL reference to entity BookEntity via reference Book
			AddNavigationField("Book", context => {
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<BookEntity>(
					graphQlContext.IdentityService,
					graphQlContext.UserManager,
					graphQlContext.DbContext,
					graphQlContext.ServiceProvider);
				var value = context.Source.Book;

				if (value != null)
				{
					return new List<BookEntity> {value}.All(filter.Compile()) ? value : null;
				}
				return null;
			});

			// GraphQL reference to entity UserEntity via reference CreatedBy
			AddNavigationField("CreatedBy", context => {
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<UserEntity>(
					graphQlContext.IdentityService,
					graphQlContext.UserManager,
					graphQlContext.DbContext,
					graphQlContext.ServiceProvider);
				var value = context.Source.CreatedBy;

				if (value != null)
				{
					return new List<UserEntity> {value}.All(filter.Compile()) ? value : null;
				}
				return null;
			});

			// GraphQL reference to entity UserEntity via reference UpdatedBy
			AddNavigationField("UpdatedBy", context => {
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<UserEntity>(
					graphQlContext.IdentityService,
					graphQlContext.UserManager,
					graphQlContext.DbContext,
					graphQlContext.ServiceProvider);
				var value = context.Source.UpdatedBy;

				if (value != null)
				{
					return new List<UserEntity> {value}.All(filter.Compile()) ? value : null;
				}
				return null;
			});

			// GraphQL reference to entity ArticleTimelineEventsEntity via reference LoggedEvent
			IEnumerable<ArticleTimelineEventsEntity> LoggedEventsResolveFunction(ResolveFieldContext<ArticleEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<ArticleTimelineEventsEntity>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.LoggedEvents.Where(filter.Compile());
			}
			AddNavigationListField("LoggedEvents", (Func<ResolveFieldContext<ArticleEntity>, IEnumerable<ArticleTimelineEventsEntity>>) LoggedEventsResolveFunction);
			AddNavigationConnectionField("LoggedEventsConnection", LoggedEventsResolveFunction);

			// GraphQL many to many reference to entity  via reference Tags
			IEnumerable<ArticlesTags> ArticlessResolveFunction(ResolveFieldContext<ArticleEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<ArticlesTags>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.Articless.Where(filter.Compile());
			}
			AddNavigationListField("Articless", (Func<ResolveFieldContext<ArticleEntity>, IEnumerable<ArticlesTags>>) ArticlessResolveFunction);
			AddNavigationConnectionField("ArticlessConnection", ArticlessResolveFunction);

			// GraphQL many to many reference to entity WorkflowStateEntity via reference WorkflowStates
			IEnumerable<ArticleWorkflowStates> WorkflowStatessResolveFunction(ResolveFieldContext<ArticleEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<ArticleWorkflowStates>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.WorkflowStatess.Where(filter.Compile());
			}
			AddNavigationListField("WorkflowStatess", (Func<ResolveFieldContext<ArticleEntity>, IEnumerable<ArticleWorkflowStates>>) WorkflowStatessResolveFunction);
			AddNavigationConnectionField("WorkflowStatessConnection", WorkflowStatessResolveFunction);

			// % protected region % [Add any extra GraphQL references here] off begin
			// % protected region % [Add any extra GraphQL references here] end
		}
	}

	/// <summary>
	/// The GraphQL input type for mutation input
	/// </summary>
	public class ArticleEntityInputType : InputObjectGraphType<ArticleEntity>
	{
		public ArticleEntityInputType()
		{
			Name = "ArticleEntityInput";
			Description = "The input object for adding a new ArticleEntity";

			// Add entity fields
			Field<IdGraphType>("Id");
			Field<DateTimeGraphType>("Created");
			Field<DateTimeGraphType>("Modified");
			Field<StringGraphType>("Title").Description = @"Title of the article";
			Field<StringGraphType>("Summary").Description = @"Short summary of the article";
			Field<StringGraphType>("Content").Description = @"The main content of the article";

			Field<ListGraphType<IdGraphType>>("WorkflowBehaviourStateIds");

			// Add entity references
			Field<IdGraphType>("BookId");
			Field<IdGraphType>("CreatedById");
			Field<IdGraphType>("UpdatedById");

			// Add references to foreign models to allow nested creation
			Field<BookEntityInputType>("Book");
			Field<UserEntityInputType>("CreatedBy");
			Field<UserEntityInputType>("UpdatedBy");
			Field<ListGraphType<ArticleTimelineEventsEntityInputType>>("LoggedEvents");
			Field<ListGraphType<ArticlesTagsInputType>>("Articless");
			Field<ListGraphType<ArticleWorkflowStatesInputType>>("WorkflowStatess");

			// % protected region % [Add any extra GraphQL input fields here] off begin
			// % protected region % [Add any extra GraphQL input fields here] end
		}
	}

}