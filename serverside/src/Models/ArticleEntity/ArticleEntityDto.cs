/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Linq;
using System.Collections.Generic;
// % protected region % [Add any extra imports here] off begin
// % protected region % [Add any extra imports here] end

namespace Lmssharp.Models
{
	public class ArticleEntityDto : ModelDto<ArticleEntity>
	{
		// % protected region % [Customise Title here] off begin
		/// <summary>
		/// Title of the article
		/// </summary>
		public String Title { get; set; }
		// % protected region % [Customise Title here] end

		// % protected region % [Customise Summary here] off begin
		/// <summary>
		/// Short summary of the article
		/// </summary>
		public String Summary { get; set; }
		// % protected region % [Customise Summary here] end

		// % protected region % [Customise Content here] off begin
		/// <summary>
		/// The main content of the article
		/// </summary>
		public String Content { get; set; }
		// % protected region % [Customise Content here] end

		// % protected region % [Customise WorkflowBehaviourStateIds here] off begin
		public List<Guid> WorkflowBehaviourStateIds { get; set; }
		// % protected region % [Customise WorkflowBehaviourStateIds here] end


		// % protected region % [Customise BookId here] off begin
		public Guid? BookId { get; set; }
		// % protected region % [Customise BookId here] end

		// % protected region % [Customise CreatedById here] off begin
		public Guid? CreatedById { get; set; }
		// % protected region % [Customise CreatedById here] end

		// % protected region % [Customise UpdatedById here] off begin
		public Guid? UpdatedById { get; set; }
		// % protected region % [Customise UpdatedById here] end

		// % protected region % [Add any extra attributes here] off begin
		// % protected region % [Add any extra attributes here] end

		public ArticleEntityDto(ArticleEntity model)
		{
			LoadModelData(model);
			// % protected region % [Add any constructor logic here] off begin
			// % protected region % [Add any constructor logic here] end
		}

		public ArticleEntityDto()
		{
			// % protected region % [Add any parameterless constructor logic here] off begin
			// % protected region % [Add any parameterless constructor logic here] end
		}

		public override ArticleEntity ToModel()
		{
			// % protected region % [Add any extra ToModel logic here] off begin
			// % protected region % [Add any extra ToModel logic here] end

			return new ArticleEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Title = Title,
				Summary = Summary,
				Content = Content,
				WorkflowBehaviourStateIds = WorkflowBehaviourStateIds?.ToList(),
				BookId  = BookId,
				CreatedById  = CreatedById,
				UpdatedById  = UpdatedById,
				// % protected region % [Add any extra model properties here] off begin
				// % protected region % [Add any extra model properties here] end
			};
		}

		public override ModelDto<ArticleEntity> LoadModelData(ArticleEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Title = model.Title;
			Summary = model.Summary;
			Content = model.Content;
			BookId  = model.BookId;
			CreatedById  = model.CreatedById;
			UpdatedById  = model.UpdatedById;

			// % protected region % [Add any extra loading data logic here] off begin
			// % protected region % [Add any extra loading data logic here] end

			return this;
		}
	}
}