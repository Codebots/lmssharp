/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Lmssharp.Services;
using GraphQL.Types;
using GraphQL.EntityFramework;
using Microsoft.AspNetCore.Identity;
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

namespace Lmssharp.Models
{
	/// <summary>
	/// The GraphQL type for returning data in GraphQL queries
	/// </summary>
	public class UserEntityType : EfObjectGraphType<LmssharpDBContext, UserEntity>
	{
		public UserEntityType(IEfGraphQLService<LmssharpDBContext> service) : base(service)
		{
			Description = @"Users of the library";

			// Add model fields to type
			Field(o => o.Id, type: typeof(IdGraphType));
			Field(o => o.Created, type: typeof(DateTimeGraphType));
			Field(o => o.Modified, type: typeof(DateTimeGraphType));
			Field(o => o.Email, type: typeof(StringGraphType));
			Field(o => o.FirstName, type: typeof(StringGraphType)).Description(@"First name of the user");
			Field(o => o.LastName, type: typeof(StringGraphType)).Description(@"Last name of the user");
			// % protected region % [Add any extra GraphQL fields here] off begin
			// % protected region % [Add any extra GraphQL fields here] end

			// Add entity references

			// GraphQL reference to entity ArticleEntity via reference CreatedArticle
			IEnumerable<ArticleEntity> CreatedArticlesResolveFunction(ResolveFieldContext<UserEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<ArticleEntity>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.CreatedArticles.Where(filter.Compile());
			}
			AddNavigationListField("CreatedArticles", (Func<ResolveFieldContext<UserEntity>, IEnumerable<ArticleEntity>>) CreatedArticlesResolveFunction);
			AddNavigationConnectionField("CreatedArticlesConnection", CreatedArticlesResolveFunction);

			// GraphQL reference to entity LessonSubmissionEntity via reference LessonSubmissions
			IEnumerable<LessonSubmissionEntity> LessonSubmissionssResolveFunction(ResolveFieldContext<UserEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<LessonSubmissionEntity>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.LessonSubmissionss.Where(filter.Compile());
			}
			AddNavigationListField("LessonSubmissionss", (Func<ResolveFieldContext<UserEntity>, IEnumerable<LessonSubmissionEntity>>) LessonSubmissionssResolveFunction);
			AddNavigationConnectionField("LessonSubmissionssConnection", LessonSubmissionssResolveFunction);

			// GraphQL reference to entity ArticleEntity via reference UpdatedArticle
			IEnumerable<ArticleEntity> UpdatedArticlesResolveFunction(ResolveFieldContext<UserEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<ArticleEntity>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.UpdatedArticles.Where(filter.Compile());
			}
			AddNavigationListField("UpdatedArticles", (Func<ResolveFieldContext<UserEntity>, IEnumerable<ArticleEntity>>) UpdatedArticlesResolveFunction);
			AddNavigationConnectionField("UpdatedArticlesConnection", UpdatedArticlesResolveFunction);

			// % protected region % [Add any extra GraphQL references here] off begin
			// % protected region % [Add any extra GraphQL references here] end
		}
	}

	/// <summary>
	/// The GraphQL input type for mutation input
	/// </summary>
	public class UserEntityInputType : InputObjectGraphType<UserEntity>
	{
		public UserEntityInputType()
		{
			Name = "UserEntityInput";
			Description = "The input object for adding a new UserEntity";

			// Add entity fields
			Field<IdGraphType>("Id");
			Field<DateTimeGraphType>("Created");
			Field<DateTimeGraphType>("Modified");
			Field<StringGraphType>("FirstName").Description = @"First name of the user";
			Field<StringGraphType>("LastName").Description = @"Last name of the user";

			// Add entity references

			// Add references to foreign models to allow nested creation
			Field<ListGraphType<ArticleEntityInputType>>("CreatedArticles");
			Field<ListGraphType<LessonSubmissionEntityInputType>>("LessonSubmissionss");
			Field<ListGraphType<ArticleEntityInputType>>("UpdatedArticles");

			// % protected region % [Add any extra GraphQL input fields here] off begin
			// % protected region % [Add any extra GraphQL input fields here] end
		}
	}

	/// <summary>
	/// The GraphQL input type for creating a user entity
	/// </summary>
	public class UserEntityCreateInputType : InputObjectGraphType<UserEntity>
	{
		public UserEntityCreateInputType()
		{
			Name = "UserEntityCreateInput";
			Description = "The input object for creating a new UserEntity";

			// Add entity fields
			Field<IdGraphType>("Id");
			Field<DateTimeGraphType>("Created");
			Field<DateTimeGraphType>("Modified");

			// Add fields specific to a user entity
			Field<StringGraphType>("Email");
			Field<StringGraphType>("Password");

			Field<StringGraphType>("FirstName").Description = @"First name of the user";
			Field<StringGraphType>("LastName").Description = @"Last name of the user";

			// Add entity references


			// Add references to foreign models to allow nested creation
			Field<ListGraphType<ArticleEntityInputType>>("CreatedArticles");
			Field<ListGraphType<LessonSubmissionEntityInputType>>("LessonSubmissionss");
			Field<ListGraphType<ArticleEntityInputType>>("UpdatedArticles");

			// % protected region % [Add any extra GraphQL create input fields here] off begin
			// % protected region % [Add any extra GraphQL create input fields here] end
		}
	}
}