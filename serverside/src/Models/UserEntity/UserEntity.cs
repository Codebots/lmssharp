/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Lmssharp.Enums;
using Lmssharp.Security;
using Lmssharp.Security.Acl;
using Lmssharp.Validators;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Z.EntityFramework.Plus;
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

namespace Lmssharp.Models {
	/// <summary>
	/// Users of the library
	/// </summary>
	// % protected region % [Configure entity attributes here] off begin
	[Table("User")]
	// % protected region % [Configure entity attributes here] end
	public class UserEntity : User, IOwnerAbstractModel	{
		/// <summary>
		/// First name of the user
		/// </summary>
		// % protected region % [Customise FirstName here] off begin
		[EntityAttribute]
		public String FirstName { get; set; }
		// % protected region % [Customise FirstName here] end

		/// <summary>
		/// Last name of the user
		/// </summary>
		// % protected region % [Customise LastName here] off begin
		[EntityAttribute]
		public String LastName { get; set; }
		// % protected region % [Customise LastName here] end

		// % protected region % [Add any further attributes here] off begin
		// % protected region % [Add any further attributes here] end

		public UserEntity()
		{
			// % protected region % [Add any constructor logic here] off begin
			// % protected region % [Add any constructor logic here] end
		}

		// % protected region % [Customise ACL attributes here] off begin
		[NotMapped]
		[JsonIgnore]
		// % protected region % [Customise ACL attributes here] end
		public override IEnumerable<IAcl> Acls => new List<IAcl>
		{
			// % protected region % [Override ACLs here] off begin
			new SuperAdministratorsScheme(),
			new AdministratorUserEntity(),
			new UserUserEntity(),
			// % protected region % [Override ACLs here] end
			// % protected region % [Add any further ACL entries here] off begin
			// % protected region % [Add any further ACL entries here] end
		};

		// % protected region % [Customise CreatedArticles here] off begin
		/// <summary>
		/// Incoming one to many reference
		/// </summary>
		/// <see cref="Lmssharp.Models.ArticleEntity"/>
		[EntityForeignKey("CreatedArticles", "CreatedBy", false, typeof(ArticleEntity))]
		public ICollection<ArticleEntity> CreatedArticles { get; set; }
		// % protected region % [Customise CreatedArticles here] end

		// % protected region % [Customise LessonSubmissionss here] off begin
		/// <summary>
		/// Incoming one to many reference
		/// </summary>
		/// <see cref="Lmssharp.Models.LessonSubmissionEntity"/>
		[EntityForeignKey("LessonSubmissionss", "User", false, typeof(LessonSubmissionEntity))]
		public ICollection<LessonSubmissionEntity> LessonSubmissionss { get; set; }
		// % protected region % [Customise LessonSubmissionss here] end

		// % protected region % [Customise UpdatedArticles here] off begin
		/// <summary>
		/// Incoming one to many reference
		/// </summary>
		/// <see cref="Lmssharp.Models.ArticleEntity"/>
		[EntityForeignKey("UpdatedArticles", "UpdatedBy", false, typeof(ArticleEntity))]
		public ICollection<ArticleEntity> UpdatedArticles { get; set; }
		// % protected region % [Customise UpdatedArticles here] end

		public override async Task BeforeSave(
			EntityState operation,
			LmssharpDBContext dbContext,
			IServiceProvider serviceProvider,
			CancellationToken cancellationToken = default)
		{
			// % protected region % [Add any initial before save logic here] off begin
			// % protected region % [Add any initial before save logic here] end

			// % protected region % [Add any before save logic here] off begin
			// % protected region % [Add any before save logic here] end
		}

		public override async Task AfterSave(
			EntityState operation,
			LmssharpDBContext dbContext,
			IServiceProvider serviceProvider,
			ICollection<ChangeState> changes,
			CancellationToken cancellationToken = default)
		{
			// % protected region % [Add any initial after save logic here] off begin
			// % protected region % [Add any initial after save logic here] end

			// % protected region % [Add any after save logic here] off begin
			// % protected region % [Add any after save logic here] end
		}

		public async override Task<int> CleanReference<T>(
			string reference,
			IEnumerable<T> models,
			LmssharpDBContext dbContext,
			CancellationToken cancellation = default)
		{
			var modelList = models.Cast<UserEntity>().ToList();
			var ids = modelList.Select(t => t.Id).ToList();

			switch (reference)
			{
				case "CreatedArticles":
					var createdArticleIds = modelList.SelectMany(x => x.CreatedArticles.Select(m => m.Id)).ToList();
					var oldcreatedArticle = await dbContext.ArticleEntity
						.Where(m => m.CreatedById.HasValue && ids.Contains(m.CreatedById.Value))
						.Where(m => !createdArticleIds.Contains(m.Id))
						.ToListAsync(cancellation);

					foreach (var createdArticle in oldcreatedArticle)
					{
						createdArticle.CreatedById = null;
					}

					dbContext.ArticleEntity.UpdateRange(oldcreatedArticle);
					return oldcreatedArticle.Count;
				case "LessonSubmissionss":
					var lessonSubmissionsIds = modelList.SelectMany(x => x.LessonSubmissionss.Select(m => m.Id)).ToList();
					var oldlessonSubmissions = await dbContext.LessonSubmissionEntity
						.Where(m => m.UserId.HasValue && ids.Contains(m.UserId.Value))
						.Where(m => !lessonSubmissionsIds.Contains(m.Id))
						.ToListAsync(cancellation);

					foreach (var lessonSubmissions in oldlessonSubmissions)
					{
						lessonSubmissions.UserId = null;
					}

					dbContext.LessonSubmissionEntity.UpdateRange(oldlessonSubmissions);
					return oldlessonSubmissions.Count;
				case "UpdatedArticles":
					var updatedArticleIds = modelList.SelectMany(x => x.UpdatedArticles.Select(m => m.Id)).ToList();
					var oldupdatedArticle = await dbContext.ArticleEntity
						.Where(m => m.UpdatedById.HasValue && ids.Contains(m.UpdatedById.Value))
						.Where(m => !updatedArticleIds.Contains(m.Id))
						.ToListAsync(cancellation);

					foreach (var updatedArticle in oldupdatedArticle)
					{
						updatedArticle.UpdatedById = null;
					}

					dbContext.ArticleEntity.UpdateRange(oldupdatedArticle);
					return oldupdatedArticle.Count;
				// % protected region % [Add any extra clean reference logic here] off begin
				// % protected region % [Add any extra clean reference logic here] end
				default:
					return 0;
			}
		}

		// % protected region % [Add any further references here] off begin
		// % protected region % [Add any further references here] end
	}
}