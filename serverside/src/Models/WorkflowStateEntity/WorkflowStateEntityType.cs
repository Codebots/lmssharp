/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Lmssharp.Services;
using GraphQL.Types;
using GraphQL.EntityFramework;
using Microsoft.AspNetCore.Identity;
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

namespace Lmssharp.Models
{
	/// <summary>
	/// The GraphQL type for returning data in GraphQL queries
	/// </summary>
	public class WorkflowStateEntityType : EfObjectGraphType<LmssharpDBContext, WorkflowStateEntity>
	{
		public WorkflowStateEntityType(IEfGraphQLService<LmssharpDBContext> service) : base(service)
		{
			Description = @"State within a workflow";

			// Add model fields to type
			Field(o => o.Id, type: typeof(IdGraphType));
			Field(o => o.Created, type: typeof(DateTimeGraphType));
			Field(o => o.Modified, type: typeof(DateTimeGraphType));
			Field(o => o.DisplayIndex, type: typeof(IntGraphType));
			Field(o => o.StepName, type: typeof(StringGraphType)).Description(@"The name of the state");
			Field(o => o.StateDescription, type: typeof(StringGraphType));
			Field(o => o.IsStartState, type: typeof(BooleanGraphType));
			// % protected region % [Add any extra GraphQL fields here] off begin
			// % protected region % [Add any extra GraphQL fields here] end

			// Add entity references
			Field(o => o.WorkflowVersionId, type: typeof(IdGraphType));

			// GraphQL reference to entity WorkflowVersionEntity via reference WorkflowVersion
			AddNavigationField("WorkflowVersion", context => {
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<WorkflowVersionEntity>(
					graphQlContext.IdentityService,
					graphQlContext.UserManager,
					graphQlContext.DbContext,
					graphQlContext.ServiceProvider);
				var value = context.Source.WorkflowVersion;

				if (value != null)
				{
					return new List<WorkflowVersionEntity> {value}.All(filter.Compile()) ? value : null;
				}
				return null;
			});

			// GraphQL reference to entity WorkflowTransitionEntity via reference OutgoingTransitions
			IEnumerable<WorkflowTransitionEntity> OutgoingTransitionssResolveFunction(ResolveFieldContext<WorkflowStateEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<WorkflowTransitionEntity>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.OutgoingTransitionss.Where(filter.Compile());
			}
			AddNavigationListField("OutgoingTransitionss", (Func<ResolveFieldContext<WorkflowStateEntity>, IEnumerable<WorkflowTransitionEntity>>) OutgoingTransitionssResolveFunction);
			AddNavigationConnectionField("OutgoingTransitionssConnection", OutgoingTransitionssResolveFunction);

			// GraphQL reference to entity WorkflowTransitionEntity via reference IncomingTransitions
			IEnumerable<WorkflowTransitionEntity> IncomingTransitionssResolveFunction(ResolveFieldContext<WorkflowStateEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<WorkflowTransitionEntity>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.IncomingTransitionss.Where(filter.Compile());
			}
			AddNavigationListField("IncomingTransitionss", (Func<ResolveFieldContext<WorkflowStateEntity>, IEnumerable<WorkflowTransitionEntity>>) IncomingTransitionssResolveFunction);
			AddNavigationConnectionField("IncomingTransitionssConnection", IncomingTransitionssResolveFunction);

			// GraphQL many to many reference to entity  via reference WorkflowStates
			IEnumerable<ArticleWorkflowStates> ArticlesResolveFunction(ResolveFieldContext<WorkflowStateEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<ArticleWorkflowStates>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.Articles.Where(filter.Compile());
			}
			AddNavigationListField("Articles", (Func<ResolveFieldContext<WorkflowStateEntity>, IEnumerable<ArticleWorkflowStates>>) ArticlesResolveFunction);
			AddNavigationConnectionField("ArticlesConnection", ArticlesResolveFunction);

			// % protected region % [Add any extra GraphQL references here] off begin
			// % protected region % [Add any extra GraphQL references here] end
		}
	}

	/// <summary>
	/// The GraphQL input type for mutation input
	/// </summary>
	public class WorkflowStateEntityInputType : InputObjectGraphType<WorkflowStateEntity>
	{
		public WorkflowStateEntityInputType()
		{
			Name = "WorkflowStateEntityInput";
			Description = "The input object for adding a new WorkflowStateEntity";

			// Add entity fields
			Field<IdGraphType>("Id");
			Field<DateTimeGraphType>("Created");
			Field<DateTimeGraphType>("Modified");
			Field<IntGraphType>("DisplayIndex");
			Field<StringGraphType>("StepName").Description = @"The name of the state";
			Field<StringGraphType>("StateDescription");
			Field<BooleanGraphType>("IsStartState");

			// Add entity references
			Field<IdGraphType>("WorkflowVersionId");

			// Add references to foreign models to allow nested creation
			Field<WorkflowVersionEntityInputType>("WorkflowVersion");
			Field<ListGraphType<WorkflowTransitionEntityInputType>>("OutgoingTransitionss");
			Field<ListGraphType<WorkflowTransitionEntityInputType>>("IncomingTransitionss");
			Field<ListGraphType<ArticleWorkflowStatesInputType>>("Articles");

			// % protected region % [Add any extra GraphQL input fields here] off begin
			// % protected region % [Add any extra GraphQL input fields here] end
		}
	}

}