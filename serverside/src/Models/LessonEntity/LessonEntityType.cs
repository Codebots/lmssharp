/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Lmssharp.Enums;
using Lmssharp.Services;
using GraphQL.Types;
using GraphQL.EntityFramework;
using Microsoft.AspNetCore.Identity;
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

namespace Lmssharp.Models
{
	/// <summary>
	/// The GraphQL type for returning data in GraphQL queries
	/// </summary>
	public class LessonEntityType : EfObjectGraphType<LmssharpDBContext, LessonEntity>
	{
		public LessonEntityType(IEfGraphQLService<LmssharpDBContext> service) : base(service)
		{
			Description = @"A lesson is a part of a course";

			// Add model fields to type
			Field(o => o.Id, type: typeof(IdGraphType));
			Field(o => o.Created, type: typeof(DateTimeGraphType));
			Field(o => o.Modified, type: typeof(DateTimeGraphType));
			Field(o => o.Summary, type: typeof(StringGraphType)).Description(@"A short summary of the lesson");
			Field(o => o.Description, type: typeof(StringGraphType)).Description(@"Longer piece of content for the cover page of the lesson");
			Field(o => o.CoverImageId, type: typeof(IdGraphType)).Description(@"A cover image for the lesson");
			Field(o => o.Duration, type: typeof(IntGraphType)).Description(@"The estimated duration of the lesson in minutes");
			Field(o => o.Difficulty, type: typeof(EnumerationGraphType<Difficulty>)).Description(@"The difficulty of the lesson");
			Field(o => o.Name, type: typeof(StringGraphType));
			Field(o => o.PublishedVersionId, type: typeof(IdGraphType));
			// % protected region % [Add any extra GraphQL fields here] off begin
			// % protected region % [Add any extra GraphQL fields here] end

			// Add entity references
			AddNavigationListField("FormVersions", context => context.Source.FormVersions);
			AddNavigationConnectionField("FormVersionConnection", context => context.Source.FormVersions);
			AddNavigationField("PublishedVersion", context => context.Source.PublishedVersion);


			// GraphQL reference to entity CourseLessonsEntity via reference CourseLessons
			IEnumerable<CourseLessonsEntity> CourseLessonssResolveFunction(ResolveFieldContext<LessonEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<CourseLessonsEntity>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.CourseLessonss.Where(filter.Compile());
			}
			AddNavigationListField("CourseLessonss", (Func<ResolveFieldContext<LessonEntity>, IEnumerable<CourseLessonsEntity>>) CourseLessonssResolveFunction);
			AddNavigationConnectionField("CourseLessonssConnection", CourseLessonssResolveFunction);

			// GraphQL reference to entity LessonEntityFormTileEntity via reference FormPage
			IEnumerable<LessonEntityFormTileEntity> FormPagesResolveFunction(ResolveFieldContext<LessonEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<LessonEntityFormTileEntity>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.FormPages.Where(filter.Compile());
			}
			AddNavigationListField("FormPages", (Func<ResolveFieldContext<LessonEntity>, IEnumerable<LessonEntityFormTileEntity>>) FormPagesResolveFunction);
			AddNavigationConnectionField("FormPagesConnection", FormPagesResolveFunction);

			// % protected region % [Add any extra GraphQL references here] off begin
			// % protected region % [Add any extra GraphQL references here] end
		}
	}

	/// <summary>
	/// The GraphQL input type for mutation input
	/// </summary>
	public class LessonEntityInputType : InputObjectGraphType<LessonEntity>
	{
		public LessonEntityInputType()
		{
			Name = "LessonEntityInput";
			Description = "The input object for adding a new LessonEntity";

			// Add entity fields
			Field<IdGraphType>("Id");
			Field<DateTimeGraphType>("Created");
			Field<DateTimeGraphType>("Modified");
			Field<StringGraphType>("Summary").Description = @"A short summary of the lesson";
			Field<StringGraphType>("Description").Description = @"Longer piece of content for the cover page of the lesson";
			Field(o => o.CoverImageId, type: typeof(IdGraphType)).Description(@"A cover image for the lesson");
			Field<IntGraphType>("Duration").Description = @"The estimated duration of the lesson in minutes";
			Field<EnumerationGraphType<Difficulty>>("Difficulty").Description = @"The difficulty of the lesson";
			Field<StringGraphType>("Name");
			Field<IdGraphType>("PublishedVersionId").Description = "The current published version for the form";
			Field<ListGraphType<LessonEntityFormVersionInputType>>("FormVersions").Description = "The versions for this form";

			// Add entity references

			// Add references to foreign models to allow nested creation
			Field<ListGraphType<CourseLessonsEntityInputType>>("CourseLessonss");
			Field<ListGraphType<LessonEntityFormTileEntityInputType>>("FormPages");

			// % protected region % [Add any extra GraphQL input fields here] off begin
			// % protected region % [Add any extra GraphQL input fields here] end
		}
	}

}