/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Lmssharp.Services;
using GraphQL.Types;
using GraphQL.EntityFramework;
using Microsoft.AspNetCore.Identity;
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

namespace Lmssharp.Models
{
	/// <summary>
	/// The GraphQL type for returning data in GraphQL queries
	/// </summary>
	public class WorkflowVersionEntityType : EfObjectGraphType<LmssharpDBContext, WorkflowVersionEntity>
	{
		public WorkflowVersionEntityType(IEfGraphQLService<LmssharpDBContext> service) : base(service)
		{
			Description = @"Version of Workflow";

			// Add model fields to type
			Field(o => o.Id, type: typeof(IdGraphType));
			Field(o => o.Created, type: typeof(DateTimeGraphType));
			Field(o => o.Modified, type: typeof(DateTimeGraphType));
			Field(o => o.WorkflowName, type: typeof(StringGraphType)).Description(@"Workflow Name");
			Field(o => o.WorkflowDescription, type: typeof(StringGraphType)).Description(@"Description of Workflow");
			Field(o => o.VersionNumber, type: typeof(IntGraphType)).Description(@"Version Number of Workflow Version");
			Field(o => o.ArticleAssociation, type: typeof(BooleanGraphType)).Description(@"If Article's are associated with this workflow version");
			// % protected region % [Add any extra GraphQL fields here] off begin
			// % protected region % [Add any extra GraphQL fields here] end

			// Add entity references
			Field(o => o.WorkflowId, type: typeof(IdGraphType));

			// GraphQL reference to entity WorkflowStateEntity via reference States
			IEnumerable<WorkflowStateEntity> StatessResolveFunction(ResolveFieldContext<WorkflowVersionEntity> context)
			{
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<WorkflowStateEntity>(graphQlContext.IdentityService, graphQlContext.UserManager, graphQlContext.DbContext, graphQlContext.ServiceProvider);
				return context.Source.Statess.Where(filter.Compile());
			}
			AddNavigationListField("Statess", (Func<ResolveFieldContext<WorkflowVersionEntity>, IEnumerable<WorkflowStateEntity>>) StatessResolveFunction);
			AddNavigationConnectionField("StatessConnection", StatessResolveFunction);

			// GraphQL reference to entity WorkflowEntity via reference Workflow
			AddNavigationField("Workflow", context => {
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<WorkflowEntity>(
					graphQlContext.IdentityService,
					graphQlContext.UserManager,
					graphQlContext.DbContext,
					graphQlContext.ServiceProvider);
				var value = context.Source.Workflow;

				if (value != null)
				{
					return new List<WorkflowEntity> {value}.All(filter.Compile()) ? value : null;
				}
				return null;
			});

			// GraphQL reference to entity WorkflowEntity via reference CurrentWorkflow
			AddNavigationField("CurrentWorkflow", context => {
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<WorkflowEntity>(
					graphQlContext.IdentityService,
					graphQlContext.UserManager,
					graphQlContext.DbContext,
					graphQlContext.ServiceProvider);
				var value = context.Source.CurrentWorkflow;

				if (value != null)
				{
					return new List<WorkflowEntity> {value}.All(filter.Compile()) ? value : null;
				}
				return null;
			});

			// % protected region % [Add any extra GraphQL references here] off begin
			// % protected region % [Add any extra GraphQL references here] end
		}
	}

	/// <summary>
	/// The GraphQL input type for mutation input
	/// </summary>
	public class WorkflowVersionEntityInputType : InputObjectGraphType<WorkflowVersionEntity>
	{
		public WorkflowVersionEntityInputType()
		{
			Name = "WorkflowVersionEntityInput";
			Description = "The input object for adding a new WorkflowVersionEntity";

			// Add entity fields
			Field<IdGraphType>("Id");
			Field<DateTimeGraphType>("Created");
			Field<DateTimeGraphType>("Modified");
			Field<StringGraphType>("WorkflowName").Description = @"Workflow Name";
			Field<StringGraphType>("WorkflowDescription").Description = @"Description of Workflow";
			Field<IntGraphType>("VersionNumber").Description = @"Version Number of Workflow Version";
			Field<BooleanGraphType>("ArticleAssociation").Description = @"If Article's are associated with this workflow version";

			// Add entity references
			Field<IdGraphType>("WorkflowId");

			// Add references to foreign models to allow nested creation
			Field<ListGraphType<WorkflowStateEntityInputType>>("Statess");
			Field<WorkflowEntityInputType>("Workflow");
			Field<WorkflowEntityInputType>("CurrentWorkflow");

			// % protected region % [Add any extra GraphQL input fields here] off begin
			// % protected region % [Add any extra GraphQL input fields here] end
		}
	}

}