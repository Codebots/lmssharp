/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Lmssharp.Services;
using GraphQL.Types;
using GraphQL.EntityFramework;
using Microsoft.AspNetCore.Identity;
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

namespace Lmssharp.Models
{
	/// <summary>
	/// The GraphQL type for returning data in GraphQL queries
	/// </summary>
	public class LessonSubmissionEntityType : EfObjectGraphType<LmssharpDBContext, LessonSubmissionEntity>
	{
		public LessonSubmissionEntityType(IEfGraphQLService<LmssharpDBContext> service) : base(service)
		{
			Description = @"User submissions for a lesson";

			// Add model fields to type
			Field(o => o.Id, type: typeof(IdGraphType));
			Field(o => o.Created, type: typeof(DateTimeGraphType));
			Field(o => o.Modified, type: typeof(DateTimeGraphType));
			Field(o => o.Completed, type: typeof(BooleanGraphType)).Description(@"Is this lesson completed");
			Field(o => o.Score, type: typeof(IntGraphType)).Description(@"The users score on the lesson");
			Field(o => o.Passed, type: typeof(BooleanGraphType)).Description(@"Did the user pass the lesson");
			Field(o => o.SubmissionData, type: typeof(StringGraphType));
			Field(o => o.FormVersionId, type: typeof(IdGraphType));
			// % protected region % [Add any extra GraphQL fields here] off begin
			// % protected region % [Add any extra GraphQL fields here] end

			// Add entity references
			AddNavigationField("FormVersion", context => context.Source.FormVersion);

			Field(o => o.UserId, type: typeof(IdGraphType));

			// GraphQL reference to entity UserEntity via reference User
			AddNavigationField("User", context => {
				var graphQlContext = (LmssharpGraphQlContext) context.UserContext;
				var filter = SecurityService.CreateReadSecurityFilter<UserEntity>(
					graphQlContext.IdentityService,
					graphQlContext.UserManager,
					graphQlContext.DbContext,
					graphQlContext.ServiceProvider);
				var value = context.Source.User;

				if (value != null)
				{
					return new List<UserEntity> {value}.All(filter.Compile()) ? value : null;
				}
				return null;
			});

			// % protected region % [Add any extra GraphQL references here] off begin
			// % protected region % [Add any extra GraphQL references here] end
		}
	}

	/// <summary>
	/// The GraphQL input type for mutation input
	/// </summary>
	public class LessonSubmissionEntityInputType : InputObjectGraphType<LessonSubmissionEntity>
	{
		public LessonSubmissionEntityInputType()
		{
			Name = "LessonSubmissionEntityInput";
			Description = "The input object for adding a new LessonSubmissionEntity";

			// Add entity fields
			Field<IdGraphType>("Id");
			Field<DateTimeGraphType>("Created");
			Field<DateTimeGraphType>("Modified");
			Field<BooleanGraphType>("Completed").Description = @"Is this lesson completed";
			Field<IntGraphType>("Score").Description = @"The users score on the lesson";
			Field<BooleanGraphType>("Passed").Description = @"Did the user pass the lesson";
			Field<StringGraphType>("SubmissionData");
			Field<IdGraphType>("FormVersionId");

			// Add entity references
			Field<IdGraphType>("UserId");

			// Add references to foreign models to allow nested creation
			Field<UserEntityInputType>("User");

			// % protected region % [Add any extra GraphQL input fields here] off begin
			// % protected region % [Add any extra GraphQL input fields here] end
		}
	}

}