/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Linq;
using System.Collections.Generic;
// % protected region % [Add any extra imports here] off begin
// % protected region % [Add any extra imports here] end

namespace Lmssharp.Models
{
	/// <summary>
	/// User submissions for a lesson
	/// </summary>
	public class LessonSubmissionEntityDto : ModelDto<LessonSubmissionEntity>
	{
		// % protected region % [Customise Completed here] off begin
		/// <summary>
		/// Is this lesson completed
		/// </summary>
		public Boolean? Completed { get; set; }
		// % protected region % [Customise Completed here] end

		// % protected region % [Customise Score here] off begin
		/// <summary>
		/// The users score on the lesson
		/// </summary>
		public int? Score { get; set; }
		// % protected region % [Customise Score here] end

		// % protected region % [Customise Passed here] off begin
		/// <summary>
		/// Did the user pass the lesson
		/// </summary>
		public Boolean? Passed { get; set; }
		// % protected region % [Customise Passed here] end

		// % protected region % [Customise SubmissionData here] off begin
		public string SubmissionData { get; set; }
		// % protected region % [Customise SubmissionData here] end


		// % protected region % [Customise UserId here] off begin
		public Guid? UserId { get; set; }
		// % protected region % [Customise UserId here] end

		// % protected region % [Add any extra attributes here] off begin
		// % protected region % [Add any extra attributes here] end

		public LessonSubmissionEntityDto(LessonSubmissionEntity model)
		{
			LoadModelData(model);
			// % protected region % [Add any constructor logic here] off begin
			// % protected region % [Add any constructor logic here] end
		}

		public LessonSubmissionEntityDto()
		{
			// % protected region % [Add any parameterless constructor logic here] off begin
			// % protected region % [Add any parameterless constructor logic here] end
		}

		public override LessonSubmissionEntity ToModel()
		{
			// % protected region % [Add any extra ToModel logic here] off begin
			// % protected region % [Add any extra ToModel logic here] end

			return new LessonSubmissionEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Completed = Completed,
				Score = Score,
				Passed = Passed,
				SubmissionData = SubmissionData,
				UserId  = UserId,
				// % protected region % [Add any extra model properties here] off begin
				// % protected region % [Add any extra model properties here] end
			};
		}

		public override ModelDto<LessonSubmissionEntity> LoadModelData(LessonSubmissionEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Completed = model.Completed;
			Score = model.Score;
			Passed = model.Passed;
			SubmissionData = model.SubmissionData;
			UserId  = model.UserId;

			// % protected region % [Add any extra loading data logic here] off begin
			// % protected region % [Add any extra loading data logic here] end

			return this;
		}
	}
}