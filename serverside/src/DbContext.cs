/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Linq;
using Npgsql;
using Audit.EntityFramework;
using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Lmssharp.Enums;
using Microsoft.Extensions.Logging;
// % protected region % [Add any extra imports here] off begin
// % protected region % [Add any extra imports here] end

namespace Lmssharp.Models {
	// % protected region % [Change the class signature here] off begin
	public class LmssharpDBContext : AuditIdentityDbContext<User, Group, Guid>, IDataProtectionKeyContext
	// % protected region % [Change the class signature here] end
	{
		private readonly ILogger<LmssharpDBContext> _logger;

		public string SessionUserId { get; }
		public string SessionUser { get; }
		public string SessionId { get; }

		// % protected region % [Add any custom class variables] off begin
		// % protected region % [Add any custom class variables] end

		public DbSet<UploadFile> Files { get; set; }
		public DbSet<AdministratorEntity> AdministratorEntity { get; set; }
		public DbSet<ArticleEntity> ArticleEntity { get; set; }
		public DbSet<BookEntity> BookEntity { get; set; }
		public DbSet<ContentFileEntity> ContentFileEntity { get; set; }
		public DbSet<CourseEntity> CourseEntity { get; set; }
		public DbSet<CourseCategoryEntity> CourseCategoryEntity { get; set; }
		public DbSet<CourseLessonsEntity> CourseLessonsEntity { get; set; }
		public DbSet<LessonEntity> LessonEntity { get; set; }
		public DbSet<LessonEntityFormVersion> LessonEntityFormVersion { get; set; }
		public DbSet<LessonSubmissionEntity> LessonSubmissionEntity { get; set; }
		public DbSet<TagEntity> TagEntity { get; set; }
		public DbSet<UserEntity> UserEntity { get; set; }
		public DbSet<WorkflowEntity> WorkflowEntity { get; set; }
		public DbSet<WorkflowStateEntity> WorkflowStateEntity { get; set; }
		public DbSet<WorkflowTransitionEntity> WorkflowTransitionEntity { get; set; }
		public DbSet<WorkflowVersionEntity> WorkflowVersionEntity { get; set; }
		public DbSet<LessonEntityFormTileEntity> LessonEntityFormTileEntity { get; set; }
		public DbSet<ArticleTimelineEventsEntity> ArticleTimelineEventsEntity { get; set; }
		public DbSet<ArticlesTags> ArticlesTags { get; set; }
		public DbSet<ArticleWorkflowStates> ArticleWorkflowStates { get; set; }
		public DbSet<DataProtectionKey> DataProtectionKeys { get; set; }

		static LmssharpDBContext()
		{
			NpgsqlConnection.GlobalTypeMapper.MapEnum<Difficulty>();
			// % protected region % [Add extra methods to the static constructor here] off begin
			// % protected region % [Add extra methods to the static constructor here] end
		}

		public LmssharpDBContext(
			// % protected region % [Add any custom constructor paramaters] off begin
			// % protected region % [Add any custom constructor paramaters] end
			DbContextOptions<LmssharpDBContext> options,
			IHttpContextAccessor httpContextAccessor,
			ILogger<LmssharpDBContext> logger) : base(options)
		{
			_logger = logger;

			SessionUser = httpContextAccessor?.HttpContext?.User?.Identity?.Name;
			SessionUserId = httpContextAccessor?.HttpContext?.User?.FindFirst("UserId")?.Value;
			SessionId = httpContextAccessor?.HttpContext?.TraceIdentifier;

			// % protected region % [Add any constructor config here] off begin
			// % protected region % [Add any constructor config here] end
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.HasPostgresEnum<Difficulty>();
			// Configure models from the entity diagram
			modelBuilder.HasPostgresExtension("uuid-ossp");
			modelBuilder.ApplyConfiguration(new AdministratorEntityConfiguration());
			modelBuilder.ApplyConfiguration(new ArticleEntityConfiguration());
			modelBuilder.ApplyConfiguration(new BookEntityConfiguration());
			modelBuilder.ApplyConfiguration(new ContentFileEntityConfiguration());
			modelBuilder.ApplyConfiguration(new CourseEntityConfiguration());
			modelBuilder.ApplyConfiguration(new CourseCategoryEntityConfiguration());
			modelBuilder.ApplyConfiguration(new CourseLessonsEntityConfiguration());
			modelBuilder.ApplyConfiguration(new LessonEntityConfiguration());
			modelBuilder.ApplyConfiguration(new LessonSubmissionEntityConfiguration());
			modelBuilder.ApplyConfiguration(new TagEntityConfiguration());
			modelBuilder.ApplyConfiguration(new UserEntityConfiguration());
			modelBuilder.ApplyConfiguration(new WorkflowEntityConfiguration());
			modelBuilder.ApplyConfiguration(new WorkflowStateEntityConfiguration());
			modelBuilder.ApplyConfiguration(new WorkflowTransitionEntityConfiguration());
			modelBuilder.ApplyConfiguration(new WorkflowVersionEntityConfiguration());
			modelBuilder.ApplyConfiguration(new LessonEntityFormTileEntityConfiguration());
			modelBuilder.ApplyConfiguration(new ArticleTimelineEventsEntityConfiguration());
			modelBuilder.ApplyConfiguration(new ArticlesTagsConfiguration());
			modelBuilder.ApplyConfiguration(new ArticleWorkflowStatesConfiguration());

			// Configure the user and group models
			modelBuilder.ApplyConfiguration(new UserConfiguration());
			modelBuilder.ApplyConfiguration(new GroupConfiguration());

			// Configure the file upload models
			modelBuilder.ApplyConfiguration(new UploadFileConfiguration());

			// % protected region % [Add any further model config here] off begin
			// % protected region % [Add any further model config here] end
		}

		/// <summary>
		/// Gets a DbSet of a certain type from the context
		/// </summary>
		/// <param name="name">The name of the DbSet to retrieve</param>
		/// <typeparam name="T">The type to cast the DbSet to</typeparam>
		/// <returns>A DbSet of the given type</returns>
		[Obsolete("Please obtain the db set from the db context with generic type param instead.")]
		public DbSet<T> GetDbSet<T>(string name = null) where T : class, IAbstractModel
		{
			// % protected region % [Add any extra logic on GetDbSet here] off begin
			// % protected region % [Add any extra logic on GetDbSet here] end

			return GetType().GetProperty(name ?? typeof(T).Name).GetValue(this, null) as DbSet<T>;
		}

		/// <summary>
		/// Gets a DbSet as an IQueryable over the owner abstract model
		/// </summary>
		/// <param name="name">The name of the DbSet to retrieve</param>
		/// <returns>The DbSet as an IQueryable over the OwnerAbstractModel or null if it doesn't exist</returns>
		public IQueryable GetOwnerDbSet(string name)
		{
			return GetType().GetProperty(name).GetValue(this, null) as IQueryable;
		}

		// % protected region % [Add any extra db config here] off begin
		// % protected region % [Add any extra db config here] end
	}
}
