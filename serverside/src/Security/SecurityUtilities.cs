/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System.Collections.Generic;
using Lmssharp.Security.Acl;

namespace Lmssharp.Security
{
	public static class SecurityUtilities
	{
		public static IEnumerable<IAcl> GetAllAcls()
		{
			return new List<IAcl>
			{
				new SuperAdministratorsScheme(),
				new VisitorsAcademyHome(),
				new UserAcademyHome(),
				new AdministratorUserEntity(),
				new AdministratorTagsEntity(),
				new AdministratorTagEntity(),
				new AdministratorLessonSubmissionEntity(),
				new AdministratorLessonEntity(),
				new AdministratorCourseLessonsEntity(),
				new AdministratorCourseCategoryEntity(),
				new AdministratorCourseEntity(),
				new AdministratorContentFileEntity(),
				new AdministratorBookEntity(),
				new AdministratorArticleEntity(),
				new AdministratorAdministratorEntity(),
				new AdministratorLesson(),
				new AdministratorCourse(),
				new AdministratorArticle(),
				new VisitorsTagsEntity(),
				new VisitorsTagEntity(),
				new VisitorsLessonEntity(),
				new VisitorsCourseLessonsEntity(),
				new VisitorsCourseCategoryEntity(),
				new VisitorsCourseEntity(),
				new VisitorsContentFileEntity(),
				new VisitorsBookEntity(),
				new VisitorsArticleEntity(),
				new VisitorsAdministratorEntity(),
				new VisitorsCourse(),
				new VisitorsArticle(),
				new UserUserEntity(),
				new UserTagsEntity(),
				new UserTagEntity(),
				new UserLessonSubmissionEntity(),
				new UserLessonEntity(),
				new UserCourseLessonsEntity(),
				new UserCourseCategoryEntity(),
				new UserCourseEntity(),
				new UserContentFileEntity(),
				new UserBookEntity(),
				new UserArticleEntity(),
				new UserLesson(),
				new UserCourse(),
				new UserArticle(),
				new UserWorkflowBehaviour(),
				new AdministratorWorkflowBehaviour(),
			};
		}
	}
}