/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Linq;
using FluentAssertions;
using Lmssharp.Controllers.Entities;
using Lmssharp.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using ServersideTests.Helpers;
using ServersideTests.Helpers.EntityFactory;
using Xunit;
// % protected region % [Add any extra imports here] off begin
// % protected region % [Add any extra imports here] end

// % protected region % [Add any additional imports here] off begin
// % protected region % [Add any additional imports here] end

namespace ServersideTests.Tests.Integration.BotWritten
{
	[Trait("Category", "BotWritten")]
	[Trait("Category", "Unit")]
	public class CrudTests : IDisposable
	{
		private readonly IWebHost _host;
		private readonly LmssharpDBContext _database;
		private readonly IServiceScope _scope;
		private readonly IServiceProvider _serviceProvider;
		// % protected region % [Add any additional members here] off begin
		// % protected region % [Add any additional members here] end

		public CrudTests()
		{
			// % protected region % [Configure constructor here] off begin
			_host = ServerBuilder.CreateServer();
			_scope = _host.Services.CreateScope();
			_serviceProvider = _scope.ServiceProvider;
			_database = _serviceProvider.GetRequiredService<LmssharpDBContext>();
			// % protected region % [Configure constructor here] end
		}
		
		public void Dispose()
		{
			// % protected region % [Configure dispose here] off begin
			_host?.Dispose();
			_database?.Dispose();
			_scope?.Dispose();
			// % protected region % [Configure dispose here] end
		}


		// % protected region % [Customise Administrator Entity crud tests here] off begin
		[Fact]
		public async void AdministratorEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<AdministratorEntityController>();
			var entities = new EntityFactory<AdministratorEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Administrator Entity crud tests here] end

		// % protected region % [Customise Article Entity crud tests here] off begin
		[Fact]
		public async void ArticleEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<ArticleEntityController>();
			var entities = new EntityFactory<ArticleEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Article Entity crud tests here] end

		// % protected region % [Customise Book Entity crud tests here] off begin
		[Fact]
		public async void BookEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<BookEntityController>();
			var entities = new EntityFactory<BookEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Book Entity crud tests here] end

		// % protected region % [Customise Content File Entity crud tests here] off begin
		[Fact]
		public async void ContentFileEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<ContentFileEntityController>();
			var entities = new EntityFactory<ContentFileEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Content File Entity crud tests here] end

		// % protected region % [Customise Course Entity crud tests here] off begin
		[Fact]
		public async void CourseEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<CourseEntityController>();
			var entities = new EntityFactory<CourseEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Course Entity crud tests here] end

		// % protected region % [Customise Course Category Entity crud tests here] off begin
		[Fact]
		public async void CourseCategoryEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<CourseCategoryEntityController>();
			var entities = new EntityFactory<CourseCategoryEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Course Category Entity crud tests here] end

		// % protected region % [Customise Course Lessons Entity crud tests here] off begin
		[Fact]
		public async void CourseLessonsEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<CourseLessonsEntityController>();
			var entities = new EntityFactory<CourseLessonsEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Course Lessons Entity crud tests here] end

		// % protected region % [Customise Lesson Entity crud tests here] off begin
		[Fact]
		public async void LessonEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<LessonEntityController>();
			var entities = new EntityFactory<LessonEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Lesson Entity crud tests here] end

		// % protected region % [Customise Lesson Submission Entity crud tests here] off begin
		[Fact]
		public async void LessonSubmissionEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<LessonSubmissionEntityController>();
			var entities = new EntityFactory<LessonSubmissionEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Lesson Submission Entity crud tests here] end

		// % protected region % [Customise Tag Entity crud tests here] off begin
		[Fact]
		public async void TagEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<TagEntityController>();
			var entities = new EntityFactory<TagEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Tag Entity crud tests here] end

		// % protected region % [Customise User Entity crud tests here] off begin
		[Fact]
		public async void UserEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<UserEntityController>();
			var entities = new EntityFactory<UserEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise User Entity crud tests here] end

		// % protected region % [Customise Workflow Entity crud tests here] off begin
		[Fact]
		public async void WorkflowEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<WorkflowEntityController>();
			var entities = new EntityFactory<WorkflowEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Workflow Entity crud tests here] end

		// % protected region % [Customise Workflow State Entity crud tests here] off begin
		[Fact]
		public async void WorkflowStateEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<WorkflowStateEntityController>();
			var entities = new EntityFactory<WorkflowStateEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Workflow State Entity crud tests here] end

		// % protected region % [Customise Workflow Transition Entity crud tests here] off begin
		[Fact]
		public async void WorkflowTransitionEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<WorkflowTransitionEntityController>();
			var entities = new EntityFactory<WorkflowTransitionEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Workflow Transition Entity crud tests here] end

		// % protected region % [Customise Workflow Version Entity crud tests here] off begin
		[Fact]
		public async void WorkflowVersionEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<WorkflowVersionEntityController>();
			var entities = new EntityFactory<WorkflowVersionEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Workflow Version Entity crud tests here] end

		// % protected region % [Customise Lesson Entity Form Tile Entity crud tests here] off begin
		[Fact]
		public async void LessonEntityFormTileEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<LessonEntityFormTileEntityController>();
			var entities = new EntityFactory<LessonEntityFormTileEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Lesson Entity Form Tile Entity crud tests here] end

		// % protected region % [Customise Article Timeline Events Entity crud tests here] off begin
		[Fact]
		public async void ArticleTimelineEventsEntityControllerGetTest()
		{
			// Arrange
			using var controller = _serviceProvider.GetRequiredService<ArticleTimelineEventsEntityController>();
			var entities = new EntityFactory<ArticleTimelineEventsEntity>(10)
				.UseAttributes()
				.UseReferences()
				.UseOwner(Guid.NewGuid())
				.Generate()
				.ToList();
			_database.AddRange(entities);
			await _database.SaveChangesAsync();

			// Act
			var data = await controller.Get(null, default);

			// Assert
			data.Data.Select(d => d.Id).Should().Contain(entities.Select(d => d.Id));
		}
		// % protected region % [Customise Article Timeline Events Entity crud tests here] end

	// % protected region % [Add any additional tests here] off begin
	// % protected region % [Add any additional tests here] end
	}
}