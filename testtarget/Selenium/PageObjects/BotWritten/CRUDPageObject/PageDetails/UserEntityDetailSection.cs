/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Linq;
using System.Collections.Generic;
using APITests.EntityObjects.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumTests.PageObjects.Components;
using SeleniumTests.Setup;
using SeleniumTests.Utils;
using SeleniumTests.Enums;
using SeleniumTests.PageObjects.BotWritten;
// % protected region % [Custom imports] off begin
// % protected region % [Custom imports] end

namespace SeleniumTests.PageObjects.CRUDPageObject.PageDetails
{
	//This section is a mapping from an entity object to an entity create or detailed view page
	public class UserEntityDetailSection : BasePage, IEntityDetailSection
	{
		private readonly IWait<IWebDriver> _driverWait;
		private readonly IWebDriver _driver;
		private readonly bool _isFastText;
		private readonly ContextConfiguration _contextConfiguration;

		// reference elements
		private static By CreatedArticlesElementBy => By.XPath("//*[contains(@class, 'createdArticle')]//div[contains(@class, 'dropdown__container')]/a");
		private static By CreatedArticlesInputElementBy => By.XPath("//*[contains(@class, 'createdArticle')]/div/input");
		private static By LessonSubmissionssElementBy => By.XPath("//*[contains(@class, 'lessonSubmissions')]//div[contains(@class, 'dropdown__container')]/a");
		private static By LessonSubmissionssInputElementBy => By.XPath("//*[contains(@class, 'lessonSubmissions')]/div/input");
		private static By UpdatedArticlesElementBy => By.XPath("//*[contains(@class, 'updatedArticle')]//div[contains(@class, 'dropdown__container')]/a");
		private static By UpdatedArticlesInputElementBy => By.XPath("//*[contains(@class, 'updatedArticle')]/div/input");

		//FlatPickr Elements

		//Attribute Headers
		private readonly UserEntity _userEntity;

		//Attribute Header Titles
		private IWebElement FirstNameHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='First Name']"));
		private IWebElement LastNameHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Last Name']"));

		// User Entity specific web Elements
		private IWebElement UserEmailElement => FindElementExt("UserEmailElement");
		private IWebElement UserPasswordElement => FindElementExt("UserPasswordElement");
		private IWebElement UserConfirmPasswordElement => FindElementExt("UserConfirmPasswordElement");
		// Datepickers
		public IWebElement CreateAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.created > input[type='date']"));
		public IWebElement ModifiedAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.modified > input[type='date']"));

		public UserEntityDetailSection(ContextConfiguration contextConfiguration, UserEntity userEntity = null) : base(contextConfiguration)
		{
			_driver = contextConfiguration.WebDriver;
			_driverWait = contextConfiguration.WebDriverWait;
			_isFastText = contextConfiguration.SeleniumSettings.FastText;
			_contextConfiguration = contextConfiguration;
			_userEntity = userEntity;

			InitializeSelectors();
			// % protected region % [Add any extra construction requires] off begin
			// % protected region % [Add any extra construction requires] end
		}

		// initialise all selectors and grouping them with the selector type which is used
		private void InitializeSelectors()
		{
			// Attribute web elements
			selectorDict.Add("FirstNameElement", (selector: "//div[contains(@class, 'firstName')]//input", type: SelectorType.XPath));
			selectorDict.Add("LastNameElement", (selector: "//div[contains(@class, 'lastName')]//input", type: SelectorType.XPath));

			// Reference web elements
			selectorDict.Add("CreatedarticleElement", (selector: ".input-group__dropdown.createdArticles > .dropdown.dropdown__container", type: SelectorType.CSS));
			selectorDict.Add("LessonsubmissionsElement", (selector: ".input-group__dropdown.lessonSubmissionss > .dropdown.dropdown__container", type: SelectorType.CSS));
			selectorDict.Add("UpdatedarticleElement", (selector: ".input-group__dropdown.updatedArticles > .dropdown.dropdown__container", type: SelectorType.CSS));

			// User Entity specific web Elements
			selectorDict.Add("UserEmailElement", (selector: "div.email > input", type: SelectorType.CSS));
			selectorDict.Add("UserPasswordElement", (selector: "div.password> input", type: SelectorType.CSS));
			selectorDict.Add("UserConfirmPasswordElement", (selector: "div._confirmPassword > input", type: SelectorType.CSS));

			// Datepicker
			selectorDict.Add("CreateAtDatepickerField", (selector: "//div[contains(@class, 'created')]/input", type: SelectorType.XPath));
			selectorDict.Add("ModifiedAtDatepickerField", (selector: "//div[contains(@class, 'modified')]/input", type: SelectorType.XPath));
		}

		//outgoing Reference web elements

		//Attribute web Elements
		private IWebElement FirstNameElement => FindElementExt("FirstNameElement");
		private IWebElement LastNameElement => FindElementExt("LastNameElement");

		// Return an IWebElement that can be used to sort an attribute.
		public IWebElement GetHeaderTile(string attribute)
		{
			return attribute switch
			{
				"First Name" => FirstNameHeaderTitle,
				"Last Name" => LastNameHeaderTitle,
				_ => throw new Exception($"Cannot find header tile {attribute}"),
			};
		}

		// Return an IWebElement for an attribute input
		public IWebElement GetInputElement(string attribute)
		{
			switch (attribute)
			{
				case "First Name":
					return FirstNameElement;
				case "Last Name":
					return LastNameElement;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		public void SetInputElement(string attribute, string value)
		{
			switch (attribute)
			{
				case "First Name":
					SetFirstName(value);
					break;
				case "Last Name":
					SetLastName(value);
					break;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		private By GetErrorAttributeSectionAsBy(string attribute)
		{
			return attribute switch
			{
				"First Name" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.firstName > div > p"),
				"Last Name" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.lastName > div > p"),
				_ => throw new Exception($"No such attribute {attribute}"),
			};
		}

		public List<string> GetErrorMessagesForAttribute(string attribute)
		{
			var elementBy = GetErrorAttributeSectionAsBy(attribute);
			WaitUtils.elementState(_driverWait, elementBy, ElementState.VISIBLE);
			var element = _driver.FindElementExt(elementBy);
			var errors = new List<string>(element.Text.Split("\r\n"));
			// remove the item in the list which is the name of the attribute and not an error.
			errors.Remove(attribute);
			return errors;
		}

		public void Apply()
		{
			// % protected region % [Configure entity application here] off begin
			SetFirstName(_userEntity.FirstName);
			SetLastName(_userEntity.LastName);

			if (_userEntity.CreatedArticleIds != null)
			{
				SetCreatedArticles(_userEntity.CreatedArticleIds.Select(x => x.ToString()));
			}
			if (_userEntity.LessonSubmissionsIds != null)
			{
				SetLessonSubmissionss(_userEntity.LessonSubmissionsIds.Select(x => x.ToString()));
			}
			if (_userEntity.UpdatedArticleIds != null)
			{
				SetUpdatedArticles(_userEntity.UpdatedArticleIds.Select(x => x.ToString()));
			}

			if (_driver.Url == $"{_contextConfiguration.BaseUrl}/admin/userentity/create")
			{
				SetUserFields(_userEntity);
			}
			// % protected region % [Configure entity application here] end
		}

		public List<Guid> GetAssociation(string referenceName)
		{
			switch (referenceName)
			{
				case "createdarticle":
					return GetCreatedArticles();
				case "lessonsubmissions":
					return GetLessonSubmissionss();
				case "updatedarticle":
					return GetUpdatedArticles();
				default:
					throw new Exception($"Cannot find association type {referenceName}");
			}
		}

		// set associations
		private void SetCreatedArticles(IEnumerable<string> ids)
		{
			WaitUtils.elementState(_driverWait, CreatedArticlesInputElementBy, ElementState.VISIBLE);
			var createdArticlesInputElement = _driver.FindElementExt(CreatedArticlesInputElementBy);

			foreach(var id in ids)
			{
				createdArticlesInputElement.SendKeys(id);
				WaitForDropdownOptions();
				createdArticlesInputElement.SendKeys(Keys.Return);
			}
		}

		private void SetLessonSubmissionss(IEnumerable<string> ids)
		{
			WaitUtils.elementState(_driverWait, LessonSubmissionssInputElementBy, ElementState.VISIBLE);
			var lessonSubmissionssInputElement = _driver.FindElementExt(LessonSubmissionssInputElementBy);

			foreach(var id in ids)
			{
				lessonSubmissionssInputElement.SendKeys(id);
				WaitForDropdownOptions();
				lessonSubmissionssInputElement.SendKeys(Keys.Return);
			}
		}

		private void SetUpdatedArticles(IEnumerable<string> ids)
		{
			WaitUtils.elementState(_driverWait, UpdatedArticlesInputElementBy, ElementState.VISIBLE);
			var updatedArticlesInputElement = _driver.FindElementExt(UpdatedArticlesInputElementBy);

			foreach(var id in ids)
			{
				updatedArticlesInputElement.SendKeys(id);
				WaitForDropdownOptions();
				updatedArticlesInputElement.SendKeys(Keys.Return);
			}
		}


		// get associations
		private List<Guid> GetCreatedArticles()
		{
			var guids = new List<Guid>();
			WaitUtils.elementState(_driverWait, CreatedArticlesElementBy, ElementState.VISIBLE);
			var createdArticlesElement = _driver.FindElements(CreatedArticlesElementBy);

			foreach(var element in createdArticlesElement)
			{
				guids.Add(new Guid (element.GetAttribute("data-id")));
			}
			return guids;
		}
		private List<Guid> GetLessonSubmissionss()
		{
			var guids = new List<Guid>();
			WaitUtils.elementState(_driverWait, LessonSubmissionssElementBy, ElementState.VISIBLE);
			var lessonSubmissionssElement = _driver.FindElements(LessonSubmissionssElementBy);

			foreach(var element in lessonSubmissionssElement)
			{
				guids.Add(new Guid (element.GetAttribute("data-id")));
			}
			return guids;
		}
		private List<Guid> GetUpdatedArticles()
		{
			var guids = new List<Guid>();
			WaitUtils.elementState(_driverWait, UpdatedArticlesElementBy, ElementState.VISIBLE);
			var updatedArticlesElement = _driver.FindElements(UpdatedArticlesElementBy);

			foreach(var element in updatedArticlesElement)
			{
				guids.Add(new Guid (element.GetAttribute("data-id")));
			}
			return guids;
		}

		// wait for dropdown to be displaying options
		private void WaitForDropdownOptions()
		{
			var xpath = "//*/div[@aria-expanded='true']";
			var elementBy = WebElementUtils.GetElementAsBy(SelectorPathType.XPATH, xpath);
			WaitUtils.elementState(_driverWait, elementBy,ElementState.EXISTS);
		}

		private void SetFirstName (String value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "firstName", value, _isFastText);
			FirstNameElement.SendKeys(Keys.Tab);
			FirstNameElement.SendKeys(Keys.Escape);
		}

		private String GetFirstName =>
			FirstNameElement.Text;

		private void SetLastName (String value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "lastName", value, _isFastText);
			LastNameElement.SendKeys(Keys.Tab);
			LastNameElement.SendKeys(Keys.Escape);
		}

		private String GetLastName =>
			LastNameElement.Text;

		// set the email, password and confirm password fields
		private void SetUserFields(UserEntity userEntity)
		{
			UserEmailElement.SendKeys(userEntity.EmailAddress);
			UserPasswordElement.SendKeys(userEntity.Password);
			UserConfirmPasswordElement.SendKeys(userEntity.Password);
		}

		// % protected region % [Add any additional getters and setters of web elements] off begin
		// % protected region % [Add any additional getters and setters of web elements] end
	}
}