/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Linq;
using System.Collections.Generic;
using APITests.EntityObjects.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumTests.PageObjects.Components;
using SeleniumTests.Setup;
using SeleniumTests.Utils;
using SeleniumTests.Enums;
using SeleniumTests.PageObjects.BotWritten;
// % protected region % [Custom imports] off begin
// % protected region % [Custom imports] end

namespace SeleniumTests.PageObjects.CRUDPageObject.PageDetails
{
	//This section is a mapping from an entity object to an entity create or detailed view page
	public class LessonSubmissionEntityDetailSection : BasePage, IEntityDetailSection
	{
		private readonly IWait<IWebDriver> _driverWait;
		private readonly IWebDriver _driver;
		private readonly bool _isFastText;
		private readonly ContextConfiguration _contextConfiguration;

		// reference elements
		private static By UserIdElementBy => By.XPath("//*[contains(@class, 'user')]//div[contains(@class, 'dropdown__container')]");
		private static By UserIdInputElementBy => By.XPath("//*[contains(@class, 'user')]/div/input");

		//FlatPickr Elements

		//Attribute Headers
		private readonly LessonSubmissionEntity _lessonSubmissionEntity;

		//Attribute Header Titles
		private IWebElement CompletedHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Completed']"));
		private IWebElement ScoreHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Score']"));
		private IWebElement PassedHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Passed']"));

		// Datepickers
		public IWebElement CreateAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.created > input[type='date']"));
		public IWebElement ModifiedAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.modified > input[type='date']"));

		public LessonSubmissionEntityDetailSection(ContextConfiguration contextConfiguration, LessonSubmissionEntity lessonSubmissionEntity = null) : base(contextConfiguration)
		{
			_driver = contextConfiguration.WebDriver;
			_driverWait = contextConfiguration.WebDriverWait;
			_isFastText = contextConfiguration.SeleniumSettings.FastText;
			_contextConfiguration = contextConfiguration;
			_lessonSubmissionEntity = lessonSubmissionEntity;

			InitializeSelectors();
			// % protected region % [Add any extra construction requires] off begin
			// % protected region % [Add any extra construction requires] end
		}

		// initialise all selectors and grouping them with the selector type which is used
		private void InitializeSelectors()
		{
			// Attribute web elements
			selectorDict.Add("CompletedElement", (selector: "//div[contains(@class, 'completed')]//input", type: SelectorType.XPath));
			selectorDict.Add("ScoreElement", (selector: "//div[contains(@class, 'score')]//input", type: SelectorType.XPath));
			selectorDict.Add("PassedElement", (selector: "//div[contains(@class, 'passed')]//input", type: SelectorType.XPath));

			// Reference web elements
			selectorDict.Add("UserElement", (selector: ".input-group__dropdown.userId > .dropdown.dropdown__container", type: SelectorType.CSS));

			// Datepicker
			selectorDict.Add("CreateAtDatepickerField", (selector: "//div[contains(@class, 'created')]/input", type: SelectorType.XPath));
			selectorDict.Add("ModifiedAtDatepickerField", (selector: "//div[contains(@class, 'modified')]/input", type: SelectorType.XPath));
		}

		//outgoing Reference web elements
		//get the input path as set by the selector library
		private IWebElement UserElement => FindElementExt("UserElement");

		//Attribute web Elements
		private IWebElement CompletedElement => FindElementExt("CompletedElement");
		private IWebElement ScoreElement => FindElementExt("ScoreElement");
		private IWebElement PassedElement => FindElementExt("PassedElement");

		// Return an IWebElement that can be used to sort an attribute.
		public IWebElement GetHeaderTile(string attribute)
		{
			return attribute switch
			{
				"Completed" => CompletedHeaderTitle,
				"Score" => ScoreHeaderTitle,
				"Passed" => PassedHeaderTitle,
				_ => throw new Exception($"Cannot find header tile {attribute}"),
			};
		}

		// Return an IWebElement for an attribute input
		public IWebElement GetInputElement(string attribute)
		{
			switch (attribute)
			{
				case "Completed":
					return CompletedElement;
				case "Score":
					return ScoreElement;
				case "Passed":
					return PassedElement;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		public void SetInputElement(string attribute, string value)
		{
			switch (attribute)
			{
				case "Completed":
					SetCompleted(bool.Parse(value));
					break;
				case "Score":
					int? score = null;
					if (int.TryParse(value, out var intScore))
					{
						score = intScore;
					}
					SetScore(score);
					break;
				case "Passed":
					SetPassed(bool.Parse(value));
					break;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		private By GetErrorAttributeSectionAsBy(string attribute)
		{
			return attribute switch
			{
				"Completed" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.completed > div > p"),
				"Score" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.score > div > p"),
				"Passed" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.passed > div > p"),
				_ => throw new Exception($"No such attribute {attribute}"),
			};
		}

		public List<string> GetErrorMessagesForAttribute(string attribute)
		{
			var elementBy = GetErrorAttributeSectionAsBy(attribute);
			WaitUtils.elementState(_driverWait, elementBy, ElementState.VISIBLE);
			var element = _driver.FindElementExt(elementBy);
			var errors = new List<string>(element.Text.Split("\r\n"));
			// remove the item in the list which is the name of the attribute and not an error.
			errors.Remove(attribute);
			return errors;
		}

		public void Apply()
		{
			// % protected region % [Configure entity application here] off begin
			SetCompleted(_lessonSubmissionEntity.Completed);
			SetScore(_lessonSubmissionEntity.Score);
			SetPassed(_lessonSubmissionEntity.Passed);

			SetUserId(_lessonSubmissionEntity.UserId?.ToString());
			// % protected region % [Configure entity application here] end
		}

		public List<Guid> GetAssociation(string referenceName)
		{
			switch (referenceName)
			{
				case "user":
					return new List<Guid>() {GetUserId()};
				default:
					throw new Exception($"Cannot find association type {referenceName}");
			}
		}

		// set associations
		private void SetUserId(string id)
		{
			if (id == "") { return; }
			WaitUtils.elementState(_driverWait, UserIdInputElementBy, ElementState.VISIBLE);
			var userIdInputElement = _driver.FindElementExt(UserIdInputElementBy);

			if (id != null)
			{
				userIdInputElement.SendKeys(id);
				WaitForDropdownOptions();
				WaitUtils.elementState(_driverWait, By.XPath($"//*/div[@role='option']/span[text()='{id}']"), ElementState.EXISTS);
				userIdInputElement.SendKeys(Keys.Return);
			}
		}

		// get associations
		private Guid GetUserId()
		{
			WaitUtils.elementState(_driverWait, UserIdElementBy, ElementState.VISIBLE);
			var userIdElement = _driver.FindElementExt(UserIdElementBy);
			return new Guid(userIdElement.GetAttribute("data-id"));
		}

		// wait for dropdown to be displaying options
		private void WaitForDropdownOptions()
		{
			var xpath = "//*/div[@aria-expanded='true']";
			var elementBy = WebElementUtils.GetElementAsBy(SelectorPathType.XPATH, xpath);
			WaitUtils.elementState(_driverWait, elementBy,ElementState.EXISTS);
		}

		private void SetCompleted (Boolean? value)
		{
			if (value is bool boolValue)
			{
				if (CompletedElement.Selected != boolValue) {
					CompletedElement.Click();
				}
			}
		}

		private Boolean? GetCompleted =>
			CompletedElement.Selected;

		private void SetScore (int? value)
		{
			if (value is int intValue)
			{
				TypingUtils.InputEntityAttributeByClass(_driver, "score", intValue.ToString(), _isFastText);
			}
		}

		private int? GetScore =>
			int.Parse(ScoreElement.Text);

		private void SetPassed (Boolean? value)
		{
			if (value is bool boolValue)
			{
				if (PassedElement.Selected != boolValue) {
					PassedElement.Click();
				}
			}
		}

		private Boolean? GetPassed =>
			PassedElement.Selected;


		// % protected region % [Add any additional getters and setters of web elements] off begin
		// % protected region % [Add any additional getters and setters of web elements] end
	}
}