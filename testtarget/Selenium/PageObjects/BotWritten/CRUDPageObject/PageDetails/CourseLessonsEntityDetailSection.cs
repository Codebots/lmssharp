/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Linq;
using System.Collections.Generic;
using APITests.EntityObjects.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumTests.PageObjects.Components;
using SeleniumTests.Setup;
using SeleniumTests.Utils;
using SeleniumTests.Enums;
using SeleniumTests.PageObjects.BotWritten;
// % protected region % [Custom imports] off begin
// % protected region % [Custom imports] end

namespace SeleniumTests.PageObjects.CRUDPageObject.PageDetails
{
	//This section is a mapping from an entity object to an entity create or detailed view page
	public class CourseLessonsEntityDetailSection : BasePage, IEntityDetailSection
	{
		private readonly IWait<IWebDriver> _driverWait;
		private readonly IWebDriver _driver;
		private readonly bool _isFastText;
		private readonly ContextConfiguration _contextConfiguration;

		// reference elements
		private static By LessonIdElementBy => By.XPath("//*[contains(@class, 'lesson')]//div[contains(@class, 'dropdown__container')]");
		private static By LessonIdInputElementBy => By.XPath("//*[contains(@class, 'lesson')]/div/input");
		private static By CourseIdElementBy => By.XPath("//*[contains(@class, 'course')]//div[contains(@class, 'dropdown__container')]");
		private static By CourseIdInputElementBy => By.XPath("//*[contains(@class, 'course')]/div/input");

		//FlatPickr Elements

		//Attribute Headers
		private readonly CourseLessonsEntity _courseLessonsEntity;

		//Attribute Header Titles
		private IWebElement OrderHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Order']"));

		// Datepickers
		public IWebElement CreateAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.created > input[type='date']"));
		public IWebElement ModifiedAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.modified > input[type='date']"));

		public CourseLessonsEntityDetailSection(ContextConfiguration contextConfiguration, CourseLessonsEntity courseLessonsEntity = null) : base(contextConfiguration)
		{
			_driver = contextConfiguration.WebDriver;
			_driverWait = contextConfiguration.WebDriverWait;
			_isFastText = contextConfiguration.SeleniumSettings.FastText;
			_contextConfiguration = contextConfiguration;
			_courseLessonsEntity = courseLessonsEntity;

			InitializeSelectors();
			// % protected region % [Add any extra construction requires] off begin
			// % protected region % [Add any extra construction requires] end
		}

		// initialise all selectors and grouping them with the selector type which is used
		private void InitializeSelectors()
		{
			// Attribute web elements
			selectorDict.Add("OrderElement", (selector: "//div[contains(@class, 'order')]//input", type: SelectorType.XPath));

			// Reference web elements
			selectorDict.Add("LessonElement", (selector: ".input-group__dropdown.lessonId > .dropdown.dropdown__container", type: SelectorType.CSS));
			selectorDict.Add("CourseElement", (selector: ".input-group__dropdown.courseId > .dropdown.dropdown__container", type: SelectorType.CSS));

			// Datepicker
			selectorDict.Add("CreateAtDatepickerField", (selector: "//div[contains(@class, 'created')]/input", type: SelectorType.XPath));
			selectorDict.Add("ModifiedAtDatepickerField", (selector: "//div[contains(@class, 'modified')]/input", type: SelectorType.XPath));
		}

		//outgoing Reference web elements
		//get the input path as set by the selector library
		private IWebElement LessonElement => FindElementExt("LessonElement");
		//get the input path as set by the selector library
		private IWebElement CourseElement => FindElementExt("CourseElement");

		//Attribute web Elements
		private IWebElement OrderElement => FindElementExt("OrderElement");

		// Return an IWebElement that can be used to sort an attribute.
		public IWebElement GetHeaderTile(string attribute)
		{
			return attribute switch
			{
				"Order" => OrderHeaderTitle,
				_ => throw new Exception($"Cannot find header tile {attribute}"),
			};
		}

		// Return an IWebElement for an attribute input
		public IWebElement GetInputElement(string attribute)
		{
			switch (attribute)
			{
				case "Order":
					return OrderElement;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		public void SetInputElement(string attribute, string value)
		{
			switch (attribute)
			{
				case "Order":
					int? order = null;
					if (int.TryParse(value, out var intOrder))
					{
						order = intOrder;
					}
					SetOrder(order);
					break;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		private By GetErrorAttributeSectionAsBy(string attribute)
		{
			return attribute switch
			{
				"Order" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.order > div > p"),
				_ => throw new Exception($"No such attribute {attribute}"),
			};
		}

		public List<string> GetErrorMessagesForAttribute(string attribute)
		{
			var elementBy = GetErrorAttributeSectionAsBy(attribute);
			WaitUtils.elementState(_driverWait, elementBy, ElementState.VISIBLE);
			var element = _driver.FindElementExt(elementBy);
			var errors = new List<string>(element.Text.Split("\r\n"));
			// remove the item in the list which is the name of the attribute and not an error.
			errors.Remove(attribute);
			return errors;
		}

		public void Apply()
		{
			// % protected region % [Configure entity application here] off begin
			SetOrder(_courseLessonsEntity.Order);

			SetLessonId(_courseLessonsEntity.LessonId?.ToString());
			SetCourseId(_courseLessonsEntity.CourseId?.ToString());
			// % protected region % [Configure entity application here] end
		}

		public List<Guid> GetAssociation(string referenceName)
		{
			switch (referenceName)
			{
				case "lesson":
					return new List<Guid>() {GetLessonId()};
				case "course":
					return new List<Guid>() {GetCourseId()};
				default:
					throw new Exception($"Cannot find association type {referenceName}");
			}
		}

		// set associations
		private void SetLessonId(string id)
		{
			if (id == "") { return; }
			WaitUtils.elementState(_driverWait, LessonIdInputElementBy, ElementState.VISIBLE);
			var lessonIdInputElement = _driver.FindElementExt(LessonIdInputElementBy);

			if (id != null)
			{
				lessonIdInputElement.SendKeys(id);
				WaitForDropdownOptions();
				WaitUtils.elementState(_driverWait, By.XPath($"//*/div[@role='option']/span[text()='{id}']"), ElementState.EXISTS);
				lessonIdInputElement.SendKeys(Keys.Return);
			}
		}
		private void SetCourseId(string id)
		{
			if (id == "") { return; }
			WaitUtils.elementState(_driverWait, CourseIdInputElementBy, ElementState.VISIBLE);
			var courseIdInputElement = _driver.FindElementExt(CourseIdInputElementBy);

			if (id != null)
			{
				courseIdInputElement.SendKeys(id);
				WaitForDropdownOptions();
				WaitUtils.elementState(_driverWait, By.XPath($"//*/div[@role='option']/span[text()='{id}']"), ElementState.EXISTS);
				courseIdInputElement.SendKeys(Keys.Return);
			}
		}

		// get associations
		private Guid GetLessonId()
		{
			WaitUtils.elementState(_driverWait, LessonIdElementBy, ElementState.VISIBLE);
			var lessonIdElement = _driver.FindElementExt(LessonIdElementBy);
			return new Guid(lessonIdElement.GetAttribute("data-id"));
		}
		private Guid GetCourseId()
		{
			WaitUtils.elementState(_driverWait, CourseIdElementBy, ElementState.VISIBLE);
			var courseIdElement = _driver.FindElementExt(CourseIdElementBy);
			return new Guid(courseIdElement.GetAttribute("data-id"));
		}

		// wait for dropdown to be displaying options
		private void WaitForDropdownOptions()
		{
			var xpath = "//*/div[@aria-expanded='true']";
			var elementBy = WebElementUtils.GetElementAsBy(SelectorPathType.XPATH, xpath);
			WaitUtils.elementState(_driverWait, elementBy,ElementState.EXISTS);
		}

		private void SetOrder (int? value)
		{
			if (value is int intValue)
			{
				TypingUtils.InputEntityAttributeByClass(_driver, "order", intValue.ToString(), _isFastText);
			}
		}

		private int? GetOrder =>
			int.Parse(OrderElement.Text);


		// % protected region % [Add any additional getters and setters of web elements] off begin
		// % protected region % [Add any additional getters and setters of web elements] end
	}
}