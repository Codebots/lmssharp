/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Linq;
using System.Collections.Generic;
using APITests.EntityObjects.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumTests.PageObjects.Components;
using SeleniumTests.Setup;
using SeleniumTests.Utils;
using SeleniumTests.Enums;
using SeleniumTests.PageObjects.BotWritten;
// % protected region % [Custom imports] off begin
// % protected region % [Custom imports] end

namespace SeleniumTests.PageObjects.CRUDPageObject.PageDetails
{
	//This section is a mapping from an entity object to an entity create or detailed view page
	public class ArticleEntityDetailSection : WorkflowPage, IEntityDetailSection
	{
		private readonly IWait<IWebDriver> _driverWait;
		private readonly IWebDriver _driver;
		private readonly bool _isFastText;
		private readonly ContextConfiguration _contextConfiguration;

		// reference elements
		private static By BookIdElementBy => By.XPath("//*[contains(@class, 'book')]//div[contains(@class, 'dropdown__container')]");
		private static By BookIdInputElementBy => By.XPath("//*[contains(@class, 'book')]/div/input");
		private static By CreatedByIdElementBy => By.XPath("//*[contains(@class, 'createdBy')]//div[contains(@class, 'dropdown__container')]");
		private static By CreatedByIdInputElementBy => By.XPath("//*[contains(@class, 'createdBy')]/div/input");
		private static By UpdatedByIdElementBy => By.XPath("//*[contains(@class, 'updatedBy')]//div[contains(@class, 'dropdown__container')]");
		private static By UpdatedByIdInputElementBy => By.XPath("//*[contains(@class, 'updatedBy')]/div/input");
		private static By ArticlessElementBy => By.XPath("//*[contains(@class, 'articles')]//div[contains(@class, 'dropdown__container')]/a");
		private static By ArticlessInputElementBy => By.XPath("//*[contains(@class, 'articles')]/div/input");
		private static By WorkflowStatessElementBy => By.XPath("//*[contains(@class, 'workflowStates')]//div[contains(@class, 'dropdown__container')]/a");
		private static By WorkflowStatessInputElementBy => By.XPath("//*[contains(@class, 'workflowStates')]/div/input");

		//FlatPickr Elements

		//Attribute Headers
		private readonly ArticleEntity _articleEntity;

		//Attribute Header Titles
		private IWebElement TitleHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Title']"));
		private IWebElement SummaryHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Summary']"));
		private IWebElement ContentHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Content']"));

		// Datepickers
		public IWebElement CreateAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.created > input[type='date']"));
		public IWebElement ModifiedAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.modified > input[type='date']"));

		public ArticleEntityDetailSection(ContextConfiguration contextConfiguration, ArticleEntity articleEntity = null) : base(contextConfiguration)
		{
			_driver = contextConfiguration.WebDriver;
			_driverWait = contextConfiguration.WebDriverWait;
			_isFastText = contextConfiguration.SeleniumSettings.FastText;
			_contextConfiguration = contextConfiguration;
			_articleEntity = articleEntity;

			InitializeSelectors();
			// % protected region % [Add any extra construction requires] off begin
			// % protected region % [Add any extra construction requires] end
		}

		// initialise all selectors and grouping them with the selector type which is used
		private void InitializeSelectors()
		{
			// Attribute web elements
			selectorDict.Add("TitleElement", (selector: "//div[contains(@class, 'title')]//input", type: SelectorType.XPath));
			selectorDict.Add("SummaryElement", (selector: "//div[contains(@class, 'summary')]//input", type: SelectorType.XPath));
			selectorDict.Add("ContentElement", (selector: "//div[contains(@class, 'content')]//input", type: SelectorType.XPath));

			// Reference web elements
			selectorDict.Add("BookElement", (selector: ".input-group__dropdown.bookId > .dropdown.dropdown__container", type: SelectorType.CSS));
			selectorDict.Add("CreatedbyElement", (selector: ".input-group__dropdown.createdById > .dropdown.dropdown__container", type: SelectorType.CSS));
			selectorDict.Add("UpdatedbyElement", (selector: ".input-group__dropdown.updatedById > .dropdown.dropdown__container", type: SelectorType.CSS));
			selectorDict.Add("ArticlesElement", (selector: ".input-group__dropdown.articless > .dropdown.dropdown__container", type: SelectorType.CSS));

			// Datepicker
			selectorDict.Add("CreateAtDatepickerField", (selector: "//div[contains(@class, 'created')]/input", type: SelectorType.XPath));
			selectorDict.Add("ModifiedAtDatepickerField", (selector: "//div[contains(@class, 'modified')]/input", type: SelectorType.XPath));
		}

		//outgoing Reference web elements
		//get the input path as set by the selector library
		private IWebElement BookElement => FindElementExt("BookElement");
		//get the input path as set by the selector library
		private IWebElement CreatedByElement => FindElementExt("CreatedByElement");
		//get the input path as set by the selector library
		private IWebElement UpdatedByElement => FindElementExt("UpdatedByElement");

		//Attribute web Elements
		private IWebElement TitleElement => FindElementExt("TitleElement");
		private IWebElement SummaryElement => FindElementExt("SummaryElement");
		private IWebElement ContentElement => FindElementExt("ContentElement");

		// Return an IWebElement that can be used to sort an attribute.
		public IWebElement GetHeaderTile(string attribute)
		{
			return attribute switch
			{
				"Title" => TitleHeaderTitle,
				"Summary" => SummaryHeaderTitle,
				"Content" => ContentHeaderTitle,
				_ => throw new Exception($"Cannot find header tile {attribute}"),
			};
		}

		// Return an IWebElement for an attribute input
		public IWebElement GetInputElement(string attribute)
		{
			switch (attribute)
			{
				case "Title":
					return TitleElement;
				case "Summary":
					return SummaryElement;
				case "Content":
					return ContentElement;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		public void SetInputElement(string attribute, string value)
		{
			switch (attribute)
			{
				case "Title":
					SetTitle(value);
					break;
				case "Summary":
					SetSummary(value);
					break;
				case "Content":
					SetContent(value);
					break;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		private By GetErrorAttributeSectionAsBy(string attribute)
		{
			return attribute switch
			{
				"Title" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.title > div > p"),
				"Summary" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.summary > div > p"),
				"Content" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.content > div > p"),
				_ => throw new Exception($"No such attribute {attribute}"),
			};
		}

		public List<string> GetErrorMessagesForAttribute(string attribute)
		{
			var elementBy = GetErrorAttributeSectionAsBy(attribute);
			WaitUtils.elementState(_driverWait, elementBy, ElementState.VISIBLE);
			var element = _driver.FindElementExt(elementBy);
			var errors = new List<string>(element.Text.Split("\r\n"));
			// remove the item in the list which is the name of the attribute and not an error.
			errors.Remove(attribute);
			return errors;
		}

		public void Apply()
		{
			// % protected region % [Configure entity application here] off begin
			SetTitle(_articleEntity.Title);
			SetSummary(_articleEntity.Summary);
			SetContent(_articleEntity.Content);

			SetBookId(_articleEntity.BookId?.ToString());
			SetCreatedById(_articleEntity.CreatedById?.ToString());
			SetUpdatedById(_articleEntity.UpdatedById?.ToString());
			if (_articleEntity.ArticlesIds != null)
			{
				SetArticless(_articleEntity.ArticlesIds.Select(x => x.ToString()));
			}
			// % protected region % [Configure entity application here] end
		}

		public List<Guid> GetAssociation(string referenceName)
		{
			switch (referenceName)
			{
				case "book":
					return new List<Guid>() {GetBookId()};
				case "createdby":
					return new List<Guid>() {GetCreatedById()};
				case "updatedby":
					return new List<Guid>() {GetUpdatedById()};
				case "articles":
					return GetArticless();
				default:
					throw new Exception($"Cannot find association type {referenceName}");
			}
		}

		// set associations
		private void SetBookId(string id)
		{
			if (id == "") { return; }
			WaitUtils.elementState(_driverWait, BookIdInputElementBy, ElementState.VISIBLE);
			var bookIdInputElement = _driver.FindElementExt(BookIdInputElementBy);

			if (id != null)
			{
				bookIdInputElement.SendKeys(id);
				WaitForDropdownOptions();
				WaitUtils.elementState(_driverWait, By.XPath($"//*/div[@role='option']/span[text()='{id}']"), ElementState.EXISTS);
				bookIdInputElement.SendKeys(Keys.Return);
			}
		}
		private void SetCreatedById(string id)
		{
			if (id == "") { return; }
			WaitUtils.elementState(_driverWait, CreatedByIdInputElementBy, ElementState.VISIBLE);
			var createdByIdInputElement = _driver.FindElementExt(CreatedByIdInputElementBy);

			if (id != null)
			{
				createdByIdInputElement.SendKeys(id);
				WaitForDropdownOptions();
				WaitUtils.elementState(_driverWait, By.XPath($"//*/div[@role='option']/span[text()='{id}']"), ElementState.EXISTS);
				createdByIdInputElement.SendKeys(Keys.Return);
			}
		}
		private void SetUpdatedById(string id)
		{
			if (id == "") { return; }
			WaitUtils.elementState(_driverWait, UpdatedByIdInputElementBy, ElementState.VISIBLE);
			var updatedByIdInputElement = _driver.FindElementExt(UpdatedByIdInputElementBy);

			if (id != null)
			{
				updatedByIdInputElement.SendKeys(id);
				WaitForDropdownOptions();
				WaitUtils.elementState(_driverWait, By.XPath($"//*/div[@role='option']/span[text()='{id}']"), ElementState.EXISTS);
				updatedByIdInputElement.SendKeys(Keys.Return);
			}
		}
		private void SetArticless(IEnumerable<string> ids)
		{
			WaitUtils.elementState(_driverWait, ArticlessInputElementBy, ElementState.VISIBLE);
			var articlessInputElement = _driver.FindElementExt(ArticlessInputElementBy);

			foreach(var id in ids)
			{
				articlessInputElement.SendKeys(id);
				WaitForDropdownOptions();
				articlessInputElement.SendKeys(Keys.Return);
			}
		}


		// get associations
		private Guid GetBookId()
		{
			WaitUtils.elementState(_driverWait, BookIdElementBy, ElementState.VISIBLE);
			var bookIdElement = _driver.FindElementExt(BookIdElementBy);
			return new Guid(bookIdElement.GetAttribute("data-id"));
		}
		private Guid GetCreatedById()
		{
			WaitUtils.elementState(_driverWait, CreatedByIdElementBy, ElementState.VISIBLE);
			var createdByIdElement = _driver.FindElementExt(CreatedByIdElementBy);
			return new Guid(createdByIdElement.GetAttribute("data-id"));
		}
		private Guid GetUpdatedById()
		{
			WaitUtils.elementState(_driverWait, UpdatedByIdElementBy, ElementState.VISIBLE);
			var updatedByIdElement = _driver.FindElementExt(UpdatedByIdElementBy);
			return new Guid(updatedByIdElement.GetAttribute("data-id"));
		}
		private List<Guid> GetArticless()
		{
			var guids = new List<Guid>();
			WaitUtils.elementState(_driverWait, ArticlessElementBy, ElementState.VISIBLE);
			var articlessElement = _driver.FindElements(ArticlessElementBy);

			foreach(var element in articlessElement)
			{
				guids.Add(new Guid (element.GetAttribute("data-id")));
			}
			return guids;
		}

		// wait for dropdown to be displaying options
		private void WaitForDropdownOptions()
		{
			var xpath = "//*/div[@aria-expanded='true']";
			var elementBy = WebElementUtils.GetElementAsBy(SelectorPathType.XPATH, xpath);
			WaitUtils.elementState(_driverWait, elementBy,ElementState.EXISTS);
		}

		private void SetTitle (String value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "title", value, _isFastText);
			TitleElement.SendKeys(Keys.Tab);
			TitleElement.SendKeys(Keys.Escape);
		}

		private String GetTitle =>
			TitleElement.Text;

		private void SetSummary (String value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "summary", value, _isFastText);
			SummaryElement.SendKeys(Keys.Tab);
			SummaryElement.SendKeys(Keys.Escape);
		}

		private String GetSummary =>
			SummaryElement.Text;

		private void SetContent (String value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "content", value, _isFastText);
			ContentElement.SendKeys(Keys.Tab);
			ContentElement.SendKeys(Keys.Escape);
		}

		private String GetContent =>
			ContentElement.Text;


		// % protected region % [Add any additional getters and setters of web elements] off begin
		// % protected region % [Add any additional getters and setters of web elements] end
	}
}