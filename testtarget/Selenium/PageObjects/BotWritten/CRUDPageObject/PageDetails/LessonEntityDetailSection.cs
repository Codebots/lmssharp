/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using EntityObject.Enums;
using APITests.EntityObjects.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumTests.PageObjects.Components;
using SeleniumTests.Setup;
using SeleniumTests.Utils;
using SeleniumTests.Enums;
using SeleniumTests.PageObjects.BotWritten;
// % protected region % [Custom imports] off begin
// % protected region % [Custom imports] end

namespace SeleniumTests.PageObjects.CRUDPageObject.PageDetails
{
	//This section is a mapping from an entity object to an entity create or detailed view page
	public class LessonEntityDetailSection : BasePage, IEntityDetailSection
	{
		private readonly IWait<IWebDriver> _driverWait;
		private readonly IWebDriver _driver;
		private readonly bool _isFastText;
		private readonly ContextConfiguration _contextConfiguration;

		// reference elements
		private static By CourseLessonssElementBy => By.XPath("//*[contains(@class, 'courseLessons')]//div[contains(@class, 'dropdown__container')]/a");
		private static By CourseLessonssInputElementBy => By.XPath("//*[contains(@class, 'courseLessons')]/div/input");

		//FlatPickr Elements

		//Attribute Headers
		private readonly LessonEntity _lessonEntity;

		//Attribute Header Titles
		private IWebElement SummaryHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Summary']"));
		private IWebElement DescriptionHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Description']"));
		private IWebElement CoverImageHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Cover Image']"));
		private IWebElement DurationHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Duration']"));
		private IWebElement DifficultyHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Difficulty']"));
		private IWebElement NameHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Name']"));

		// Datepickers
		public IWebElement CreateAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.created > input[type='date']"));
		public IWebElement ModifiedAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.modified > input[type='date']"));

		public LessonEntityDetailSection(ContextConfiguration contextConfiguration, LessonEntity lessonEntity = null) : base(contextConfiguration)
		{
			_driver = contextConfiguration.WebDriver;
			_driverWait = contextConfiguration.WebDriverWait;
			_isFastText = contextConfiguration.SeleniumSettings.FastText;
			_contextConfiguration = contextConfiguration;
			_lessonEntity = lessonEntity;

			InitializeSelectors();
			// % protected region % [Add any extra construction requires] off begin
			// % protected region % [Add any extra construction requires] end
		}

		// initialise all selectors and grouping them with the selector type which is used
		private void InitializeSelectors()
		{
			// Attribute web elements
			selectorDict.Add("SummaryElement", (selector: "//div[contains(@class, 'summary')]//input", type: SelectorType.XPath));
			selectorDict.Add("DescriptionElement", (selector: "//div[contains(@class, 'description')]//input", type: SelectorType.XPath));
			selectorDict.Add("CoverImageElement", (selector: "//div[contains(@class, 'coverImage')]//input", type: SelectorType.XPath));
			selectorDict.Add("DurationElement", (selector: "//div[contains(@class, 'duration')]//input", type: SelectorType.XPath));
			selectorDict.Add("DifficultyElement", (selector: "//div[contains(@class, 'difficulty')]//input", type: SelectorType.XPath));

			// Reference web elements
			selectorDict.Add("CourselessonsElement", (selector: ".input-group__dropdown.courseLessonss > .dropdown.dropdown__container", type: SelectorType.CSS));

			// Form Entity specific web Element
			selectorDict.Add("NameElement", (selector: "div.name > input", type: SelectorType.CSS));

			// Datepicker
			selectorDict.Add("CreateAtDatepickerField", (selector: "//div[contains(@class, 'created')]/input", type: SelectorType.XPath));
			selectorDict.Add("ModifiedAtDatepickerField", (selector: "//div[contains(@class, 'modified')]/input", type: SelectorType.XPath));
		}

		//outgoing Reference web elements

		//Attribute web Elements
		private IWebElement SummaryElement => FindElementExt("SummaryElement");
		private IWebElement DescriptionElement => FindElementExt("DescriptionElement");
		private IWebElement CoverImageElement => FindElementExt("CoverImageElement");
		private IWebElement DurationElement => FindElementExt("DurationElement");
		private IWebElement DifficultyElement => FindElementExt("DifficultyElement");
		private IWebElement NameElement => FindElementExt("NameElement");

		// Return an IWebElement that can be used to sort an attribute.
		public IWebElement GetHeaderTile(string attribute)
		{
			return attribute switch
			{
				"Summary" => SummaryHeaderTitle,
				"Description" => DescriptionHeaderTitle,
				"Cover Image" => CoverImageHeaderTitle,
				"Duration" => DurationHeaderTitle,
				"Difficulty" => DifficultyHeaderTitle,
				"Name" => NameHeaderTitle,
				_ => throw new Exception($"Cannot find header tile {attribute}"),
			};
		}

		// Return an IWebElement for an attribute input
		public IWebElement GetInputElement(string attribute)
		{
			switch (attribute)
			{
				case "Name":
					return NameElement;
				case "Summary":
					return SummaryElement;
				case "Description":
					return DescriptionElement;
				case "Cover Image":
					return CoverImageElement;
				case "Duration":
					return DurationElement;
				case "Difficulty":
					return DifficultyElement;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		public void SetInputElement(string attribute, string value)
		{
			switch (attribute)
			{
				case "Name":
					SetName(value);
					break;
				case "Summary":
					SetSummary(value);
					break;
				case "Description":
					SetDescription(value);
					break;
				case "Cover Image":
					SetCoverImage(value);
					break;
				case "Duration":
					int? duration = null;
					if (int.TryParse(value, out var intDuration))
					{
						duration = intDuration;
					}
					SetDuration(duration);
					break;
				case "Difficulty":
					SetDifficulty((Difficulty)Enum.Parse(typeof(Difficulty), value));
					break;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		private By GetErrorAttributeSectionAsBy(string attribute)
		{
			return attribute switch
			{
				"Name" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "//div[contains(@class, 'name')]"),
				"Summary" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.summary > div > p"),
				"Description" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.description > div > p"),
				"Cover Image" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.coverImage > div > p"),
				"Duration" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.duration > div > p"),
				"Difficulty" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.difficulty > div > p"),
				_ => throw new Exception($"No such attribute {attribute}"),
			};
		}

		public List<string> GetErrorMessagesForAttribute(string attribute)
		{
			var elementBy = GetErrorAttributeSectionAsBy(attribute);
			WaitUtils.elementState(_driverWait, elementBy, ElementState.VISIBLE);
			var element = _driver.FindElementExt(elementBy);
			var errors = new List<string>(element.Text.Split("\r\n"));
			// remove the item in the list which is the name of the attribute and not an error.
			errors.Remove(attribute);
			return errors;
		}

		public void Apply()
		{
			// % protected region % [Configure entity application here] off begin
			SetName(_lessonEntity.Name);
			SetSummary(_lessonEntity.Summary);
			SetDescription(_lessonEntity.Description);
			SetCoverImage(_lessonEntity.CoverImageId.ToString());
			SetDuration(_lessonEntity.Duration);
			SetDifficulty(_lessonEntity.Difficulty);

			if (_lessonEntity.CourseLessonsIds != null)
			{
				SetCourseLessonss(_lessonEntity.CourseLessonsIds.Select(x => x.ToString()));
			}
			// % protected region % [Configure entity application here] end
		}

		public List<Guid> GetAssociation(string referenceName)
		{
			switch (referenceName)
			{
				case "courselessons":
					return GetCourseLessonss();
				default:
					throw new Exception($"Cannot find association type {referenceName}");
			}
		}

		// set associations
		private void SetCourseLessonss(IEnumerable<string> ids)
		{
			WaitUtils.elementState(_driverWait, CourseLessonssInputElementBy, ElementState.VISIBLE);
			var courseLessonssInputElement = _driver.FindElementExt(CourseLessonssInputElementBy);

			foreach(var id in ids)
			{
				courseLessonssInputElement.SendKeys(id);
				WaitForDropdownOptions();
				courseLessonssInputElement.SendKeys(Keys.Return);
			}
		}


		// get associations
		private List<Guid> GetCourseLessonss()
		{
			var guids = new List<Guid>();
			WaitUtils.elementState(_driverWait, CourseLessonssElementBy, ElementState.VISIBLE);
			var courseLessonssElement = _driver.FindElements(CourseLessonssElementBy);

			foreach(var element in courseLessonssElement)
			{
				guids.Add(new Guid (element.GetAttribute("data-id")));
			}
			return guids;
		}

		// wait for dropdown to be displaying options
		private void WaitForDropdownOptions()
		{
			var xpath = "//*/div[@aria-expanded='true']";
			var elementBy = WebElementUtils.GetElementAsBy(SelectorPathType.XPATH, xpath);
			WaitUtils.elementState(_driverWait, elementBy,ElementState.EXISTS);
		}

		private void SetSummary (String value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "summary", value, _isFastText);
			SummaryElement.SendKeys(Keys.Tab);
			SummaryElement.SendKeys(Keys.Escape);
		}

		private String GetSummary =>
			SummaryElement.Text;

		private void SetDescription (String value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "description", value, _isFastText);
			DescriptionElement.SendKeys(Keys.Tab);
			DescriptionElement.SendKeys(Keys.Escape);
		}

		private String GetDescription =>
			DescriptionElement.Text;

		private void SetCoverImage (String value)
		{
			const string script = "document.querySelector('.coverImageId>div>input').removeAttribute('style')";
			var js = (IJavaScriptExecutor)driver;
			js.ExecuteScript(script);
			var path = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "Resources/RedCircle.svg"));
			var fileUploadElement = driver.FindElementExt(By.CssSelector(".coverImageId>div>input"));
			fileUploadElement.SendKeys(path);
		}

		private String GetCoverImage =>
			CoverImageElement.Text;

		private void SetDuration (int? value)
		{
			if (value is int intValue)
			{
				TypingUtils.InputEntityAttributeByClass(_driver, "duration", intValue.ToString(), _isFastText);
			}
		}

		private int? GetDuration =>
			int.Parse(DurationElement.Text);

		private void SetDifficulty (Difficulty value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "difficulty", value.ToString(), _isFastText);
		}

		private Difficulty GetDifficulty =>
			(Difficulty)Enum.Parse(typeof(Difficulty), DifficultyElement.Text);
			

		// Set Name for form entity
		private void SetName (String value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "name", value, _isFastText);
			NameElement.SendKeys(Keys.Tab);
		}

		private String GetName => NameElement.Text;
		// % protected region % [Add any additional getters and setters of web elements] off begin
		// % protected region % [Add any additional getters and setters of web elements] end
	}
}