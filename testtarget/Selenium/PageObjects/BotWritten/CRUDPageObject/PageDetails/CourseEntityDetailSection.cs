/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using EntityObject.Enums;
using APITests.EntityObjects.Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumTests.PageObjects.Components;
using SeleniumTests.Setup;
using SeleniumTests.Utils;
using SeleniumTests.Enums;
using SeleniumTests.PageObjects.BotWritten;
// % protected region % [Custom imports] off begin
// % protected region % [Custom imports] end

namespace SeleniumTests.PageObjects.CRUDPageObject.PageDetails
{
	//This section is a mapping from an entity object to an entity create or detailed view page
	public class CourseEntityDetailSection : BasePage, IEntityDetailSection
	{
		private readonly IWait<IWebDriver> _driverWait;
		private readonly IWebDriver _driver;
		private readonly bool _isFastText;
		private readonly ContextConfiguration _contextConfiguration;

		// reference elements
		private static By CourseLessonssElementBy => By.XPath("//*[contains(@class, 'courseLessons')]//div[contains(@class, 'dropdown__container')]/a");
		private static By CourseLessonssInputElementBy => By.XPath("//*[contains(@class, 'courseLessons')]/div/input");
		private static By CourseCategoryIdElementBy => By.XPath("//*[contains(@class, 'courseCategory')]//div[contains(@class, 'dropdown__container')]");
		private static By CourseCategoryIdInputElementBy => By.XPath("//*[contains(@class, 'courseCategory')]/div/input");

		//FlatPickr Elements

		//Attribute Headers
		private readonly CourseEntity _courseEntity;

		//Attribute Header Titles
		private IWebElement NameHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Name']"));
		private IWebElement SummaryHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Summary']"));
		private IWebElement CoverImageHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Cover image']"));
		private IWebElement DifficultyHeaderTitle => _driver.FindElementExt(By.XPath("//th[text()='Difficulty']"));

		// Datepickers
		public IWebElement CreateAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.created > input[type='date']"));
		public IWebElement ModifiedAtDatepickerField => _driver.FindElementExt(By.CssSelector("div.modified > input[type='date']"));

		public CourseEntityDetailSection(ContextConfiguration contextConfiguration, CourseEntity courseEntity = null) : base(contextConfiguration)
		{
			_driver = contextConfiguration.WebDriver;
			_driverWait = contextConfiguration.WebDriverWait;
			_isFastText = contextConfiguration.SeleniumSettings.FastText;
			_contextConfiguration = contextConfiguration;
			_courseEntity = courseEntity;

			InitializeSelectors();
			// % protected region % [Add any extra construction requires] off begin
			// % protected region % [Add any extra construction requires] end
		}

		// initialise all selectors and grouping them with the selector type which is used
		private void InitializeSelectors()
		{
			// Attribute web elements
			selectorDict.Add("NameElement", (selector: "//div[contains(@class, 'name')]//input", type: SelectorType.XPath));
			selectorDict.Add("SummaryElement", (selector: "//div[contains(@class, 'summary')]//input", type: SelectorType.XPath));
			selectorDict.Add("CoverImageElement", (selector: "//div[contains(@class, 'coverImage')]//input", type: SelectorType.XPath));
			selectorDict.Add("DifficultyElement", (selector: "//div[contains(@class, 'difficulty')]//input", type: SelectorType.XPath));

			// Reference web elements
			selectorDict.Add("CourselessonsElement", (selector: ".input-group__dropdown.courseLessonss > .dropdown.dropdown__container", type: SelectorType.CSS));
			selectorDict.Add("CoursecategoryElement", (selector: ".input-group__dropdown.courseCategoryId > .dropdown.dropdown__container", type: SelectorType.CSS));

			// Datepicker
			selectorDict.Add("CreateAtDatepickerField", (selector: "//div[contains(@class, 'created')]/input", type: SelectorType.XPath));
			selectorDict.Add("ModifiedAtDatepickerField", (selector: "//div[contains(@class, 'modified')]/input", type: SelectorType.XPath));
		}

		//outgoing Reference web elements
		//get the input path as set by the selector library
		private IWebElement CourseCategoryElement => FindElementExt("CourseCategoryElement");

		//Attribute web Elements
		private IWebElement NameElement => FindElementExt("NameElement");
		private IWebElement SummaryElement => FindElementExt("SummaryElement");
		private IWebElement CoverImageElement => FindElementExt("CoverImageElement");
		private IWebElement DifficultyElement => FindElementExt("DifficultyElement");

		// Return an IWebElement that can be used to sort an attribute.
		public IWebElement GetHeaderTile(string attribute)
		{
			return attribute switch
			{
				"Name" => NameHeaderTitle,
				"Summary" => SummaryHeaderTitle,
				"Cover image" => CoverImageHeaderTitle,
				"Difficulty" => DifficultyHeaderTitle,
				_ => throw new Exception($"Cannot find header tile {attribute}"),
			};
		}

		// Return an IWebElement for an attribute input
		public IWebElement GetInputElement(string attribute)
		{
			switch (attribute)
			{
				case "Name":
					return NameElement;
				case "Summary":
					return SummaryElement;
				case "Cover image":
					return CoverImageElement;
				case "Difficulty":
					return DifficultyElement;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		public void SetInputElement(string attribute, string value)
		{
			switch (attribute)
			{
				case "Name":
					SetName(value);
					break;
				case "Summary":
					SetSummary(value);
					break;
				case "Cover image":
					SetCoverImage(value);
					break;
				case "Difficulty":
					SetDifficulty((Difficulty)Enum.Parse(typeof(Difficulty), value));
					break;
				default:
					throw new Exception($"Cannot find input element {attribute}");
			}
		}

		private By GetErrorAttributeSectionAsBy(string attribute)
		{
			return attribute switch
			{
				"Name" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.name > div > p"),
				"Summary" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.summary > div > p"),
				"Cover image" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.coverImage > div > p"),
				"Difficulty" => WebElementUtils.GetElementAsBy(SelectorPathType.CSS, "div.difficulty > div > p"),
				_ => throw new Exception($"No such attribute {attribute}"),
			};
		}

		public List<string> GetErrorMessagesForAttribute(string attribute)
		{
			var elementBy = GetErrorAttributeSectionAsBy(attribute);
			WaitUtils.elementState(_driverWait, elementBy, ElementState.VISIBLE);
			var element = _driver.FindElementExt(elementBy);
			var errors = new List<string>(element.Text.Split("\r\n"));
			// remove the item in the list which is the name of the attribute and not an error.
			errors.Remove(attribute);
			return errors;
		}

		public void Apply()
		{
			// % protected region % [Configure entity application here] off begin
			SetName(_courseEntity.Name);
			SetSummary(_courseEntity.Summary);
			SetCoverImage(_courseEntity.CoverImageId.ToString());
			SetDifficulty(_courseEntity.Difficulty);

			if (_courseEntity.CourseLessonsIds != null)
			{
				SetCourseLessonss(_courseEntity.CourseLessonsIds.Select(x => x.ToString()));
			}
			SetCourseCategoryId(_courseEntity.CourseCategoryId?.ToString());
			// % protected region % [Configure entity application here] end
		}

		public List<Guid> GetAssociation(string referenceName)
		{
			switch (referenceName)
			{
				case "courselessons":
					return GetCourseLessonss();
				case "coursecategory":
					return new List<Guid>() {GetCourseCategoryId()};
				default:
					throw new Exception($"Cannot find association type {referenceName}");
			}
		}

		// set associations
		private void SetCourseLessonss(IEnumerable<string> ids)
		{
			WaitUtils.elementState(_driverWait, CourseLessonssInputElementBy, ElementState.VISIBLE);
			var courseLessonssInputElement = _driver.FindElementExt(CourseLessonssInputElementBy);

			foreach(var id in ids)
			{
				courseLessonssInputElement.SendKeys(id);
				WaitForDropdownOptions();
				courseLessonssInputElement.SendKeys(Keys.Return);
			}
		}

		private void SetCourseCategoryId(string id)
		{
			if (id == "") { return; }
			WaitUtils.elementState(_driverWait, CourseCategoryIdInputElementBy, ElementState.VISIBLE);
			var courseCategoryIdInputElement = _driver.FindElementExt(CourseCategoryIdInputElementBy);

			if (id != null)
			{
				courseCategoryIdInputElement.SendKeys(id);
				WaitForDropdownOptions();
				WaitUtils.elementState(_driverWait, By.XPath($"//*/div[@role='option']/span[text()='{id}']"), ElementState.EXISTS);
				courseCategoryIdInputElement.SendKeys(Keys.Return);
			}
		}

		// get associations
		private List<Guid> GetCourseLessonss()
		{
			var guids = new List<Guid>();
			WaitUtils.elementState(_driverWait, CourseLessonssElementBy, ElementState.VISIBLE);
			var courseLessonssElement = _driver.FindElements(CourseLessonssElementBy);

			foreach(var element in courseLessonssElement)
			{
				guids.Add(new Guid (element.GetAttribute("data-id")));
			}
			return guids;
		}
		private Guid GetCourseCategoryId()
		{
			WaitUtils.elementState(_driverWait, CourseCategoryIdElementBy, ElementState.VISIBLE);
			var courseCategoryIdElement = _driver.FindElementExt(CourseCategoryIdElementBy);
			return new Guid(courseCategoryIdElement.GetAttribute("data-id"));
		}

		// wait for dropdown to be displaying options
		private void WaitForDropdownOptions()
		{
			var xpath = "//*/div[@aria-expanded='true']";
			var elementBy = WebElementUtils.GetElementAsBy(SelectorPathType.XPATH, xpath);
			WaitUtils.elementState(_driverWait, elementBy,ElementState.EXISTS);
		}

		private void SetName (String value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "name", value, _isFastText);
			NameElement.SendKeys(Keys.Tab);
			NameElement.SendKeys(Keys.Escape);
		}

		private String GetName =>
			NameElement.Text;

		private void SetSummary (String value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "summary", value, _isFastText);
			SummaryElement.SendKeys(Keys.Tab);
			SummaryElement.SendKeys(Keys.Escape);
		}

		private String GetSummary =>
			SummaryElement.Text;

		private void SetCoverImage (String value)
		{
			const string script = "document.querySelector('.coverImageId>div>input').removeAttribute('style')";
			var js = (IJavaScriptExecutor)driver;
			js.ExecuteScript(script);
			var path = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "Resources/RedCircle.svg"));
			var fileUploadElement = driver.FindElementExt(By.CssSelector(".coverImageId>div>input"));
			fileUploadElement.SendKeys(path);
		}

		private String GetCoverImage =>
			CoverImageElement.Text;

		private void SetDifficulty (Difficulty value)
		{
			TypingUtils.InputEntityAttributeByClass(_driver, "difficulty", value.ToString(), _isFastText);
		}

		private Difficulty GetDifficulty =>
			(Difficulty)Enum.Parse(typeof(Difficulty), DifficultyElement.Text);
			

		// % protected region % [Add any additional getters and setters of web elements] off begin
		// % protected region % [Add any additional getters and setters of web elements] end
	}
}