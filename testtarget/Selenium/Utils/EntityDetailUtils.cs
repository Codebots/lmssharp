/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

using System;
using SeleniumTests.PageObjects.BotWritten;
using SeleniumTests.PageObjects.CRUDPageObject.PageDetails;
using SeleniumTests.Setup;

namespace SeleniumTests.Utils
{
	internal static class EntityDetailUtils
	{
		public static IDetailSection GetEntityDetailsSection(string entityName, ContextConfiguration contextConfiguration)
		{
			switch (entityName)
			{
				case "AdministratorEntity":
					return new AdministratorEntityDetailSection(contextConfiguration);
				case "ArticleEntity":
					return new ArticleEntityDetailSection(contextConfiguration);
				case "BookEntity":
					return new BookEntityDetailSection(contextConfiguration);
				case "ContentFileEntity":
					return new ContentFileEntityDetailSection(contextConfiguration);
				case "CourseEntity":
					return new CourseEntityDetailSection(contextConfiguration);
				case "CourseCategoryEntity":
					return new CourseCategoryEntityDetailSection(contextConfiguration);
				case "CourseLessonsEntity":
					return new CourseLessonsEntityDetailSection(contextConfiguration);
				case "LessonEntity":
					return new LessonEntityDetailSection(contextConfiguration);
				case "LessonSubmissionEntity":
					return new LessonSubmissionEntityDetailSection(contextConfiguration);
				case "TagEntity":
					return new TagEntityDetailSection(contextConfiguration);
				case "UserEntity":
					return new UserEntityDetailSection(contextConfiguration);
				case "WorkflowEntity":
					return new WorkflowEntityDetailSection(contextConfiguration);
				case "WorkflowTransitionEntity":
					return new WorkflowTransitionEntityDetailSection(contextConfiguration);
				case "WorkflowVersionEntity":
					return new WorkflowVersionEntityDetailSection(contextConfiguration);
				case "ArticleTimelineEventsEntity":
					return new ArticleTimelineEventsEntityDetailSection(contextConfiguration);
				default:
					throw new Exception($"Cannot find detail section for type {entityName}");
			}
		}

		public static WorkflowPage GetWorkflowEntityDetailsSection(string entityName, ContextConfiguration contextConfiguration)
		{
			switch (entityName)
			{
				case "ArticleEntity":
					return new ArticleEntityDetailSection(contextConfiguration);
				default:
					throw new Exception($"Cannot find detail section for type {entityName}");
			}
		}
	}
}