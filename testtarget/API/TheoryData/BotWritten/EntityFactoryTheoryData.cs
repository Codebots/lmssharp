/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using APITests.Factories;
using Xunit;
// % protected region % [Add any extra imports here] off begin
// % protected region % [Add any extra imports here] end

// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

namespace APITests.TheoryData.BotWritten
{
	public class UserEntityFactorySingleTheoryData : TheoryData<UserEntityFactory>
	{
		public UserEntityFactorySingleTheoryData()
		{
			// % protected region % [Modify UserEntityFactorySingleTheoryData entities here] off begin
			Add(new UserEntityFactory("AdministratorEntity"));
			Add(new UserEntityFactory("UserEntity"));
			// % protected region % [Modify UserEntityFactorySingleTheoryData entities here] end
		}
	}

	public class EntityFactorySingleTheoryData : TheoryData<EntityFactory, int>
	{
		public EntityFactorySingleTheoryData()
		{
			// % protected region % [Modify UserEntityFactorySingleTheoryData entities here] off begin
			Add(new EntityFactory("AdministratorEntity"), 1);
			Add(new EntityFactory("ArticleEntity"), 1);
			Add(new EntityFactory("BookEntity"), 1);
			Add(new EntityFactory("ContentFileEntity"), 1);
			Add(new EntityFactory("CourseEntity"), 1);
			Add(new EntityFactory("CourseCategoryEntity"), 1);
			Add(new EntityFactory("CourseLessonsEntity"), 1);
			Add(new EntityFactory("LessonEntity"), 1);
			Add(new EntityFactory("TagEntity"), 1);
			Add(new EntityFactory("UserEntity"), 1);
			Add(new EntityFactory("WorkflowEntity"), 1);
			Add(new EntityFactory("WorkflowStateEntity"), 1);
			Add(new EntityFactory("WorkflowTransitionEntity"), 1);
			Add(new EntityFactory("WorkflowVersionEntity"), 1);
			Add(new EntityFactory("ArticleTimelineEventsEntity"), 1);
			// % protected region % [Modify UserEntityFactorySingleTheoryData entities here] end
		}
	}

	public class NonUserEntityFactorySingleTheoryData : TheoryData<EntityFactory, int>
	{
		public NonUserEntityFactorySingleTheoryData()
		{
			// % protected region % [Modify UserEntityFactorySingleTheoryData entities here] off begin
			Add(new EntityFactory("AdministratorEntity"), 1);
			Add(new EntityFactory("ArticleEntity"), 1);
			Add(new EntityFactory("BookEntity"), 1);
			Add(new EntityFactory("ContentFileEntity"), 1);
			Add(new EntityFactory("CourseEntity"), 1);
			Add(new EntityFactory("CourseCategoryEntity"), 1);
			Add(new EntityFactory("CourseLessonsEntity"), 1);
			Add(new EntityFactory("LessonEntity"), 1);
			Add(new EntityFactory("TagEntity"), 1);
			Add(new EntityFactory("UserEntity"), 1);
			Add(new EntityFactory("WorkflowEntity"), 1);
			Add(new EntityFactory("WorkflowStateEntity"), 1);
			Add(new EntityFactory("WorkflowTransitionEntity"), 1);
			Add(new EntityFactory("WorkflowVersionEntity"), 1);
			Add(new EntityFactory("ArticleTimelineEventsEntity"), 1);
			// % protected region % [Modify UserEntityFactorySingleTheoryData entities here] end
		}
	}

	public class EntityFactoryTheoryData : TheoryData<EntityFactory>
	{
		public EntityFactoryTheoryData()
		{
			// % protected region % [Modify UserEntityFactorySingleTheoryData entities here] off begin
			Add(new EntityFactory("AdministratorEntity"));
			Add(new EntityFactory("ArticleEntity"));
			Add(new EntityFactory("BookEntity"));
			Add(new EntityFactory("ContentFileEntity"));
			Add(new EntityFactory("CourseEntity"));
			Add(new EntityFactory("CourseCategoryEntity"));
			Add(new EntityFactory("CourseLessonsEntity"));
			Add(new EntityFactory("LessonEntity"));
			Add(new EntityFactory("TagEntity"));
			Add(new EntityFactory("UserEntity"));
			Add(new EntityFactory("WorkflowEntity"));
			Add(new EntityFactory("WorkflowStateEntity"));
			Add(new EntityFactory("WorkflowTransitionEntity"));
			Add(new EntityFactory("WorkflowVersionEntity"));
			Add(new EntityFactory("ArticleTimelineEventsEntity"));
			// % protected region % [Modify UserEntityFactorySingleTheoryData entities here] end
		}
	}

	public class EntityFactoryMultipleTheoryData : TheoryData<EntityFactory, int>
	{
		public EntityFactoryMultipleTheoryData()
		{
			// % protected region % [Modify UserEntityFactorySingleTheoryData entities here] off begin
			var numEntities = 3;
			Add(new EntityFactory("ArticleEntity"), numEntities);
			Add(new EntityFactory("BookEntity"), numEntities);
			Add(new EntityFactory("ContentFileEntity"), numEntities);
			Add(new EntityFactory("CourseEntity"), numEntities);
			Add(new EntityFactory("CourseCategoryEntity"), numEntities);
			Add(new EntityFactory("CourseLessonsEntity"), numEntities);
			Add(new EntityFactory("LessonEntity"), numEntities);
			Add(new EntityFactory("TagEntity"), numEntities);
			Add(new EntityFactory("UserEntity"), numEntities);
			Add(new EntityFactory("WorkflowEntity"), numEntities);
			Add(new EntityFactory("WorkflowStateEntity"), numEntities);
			Add(new EntityFactory("WorkflowTransitionEntity"), numEntities);
			Add(new EntityFactory("WorkflowVersionEntity"), numEntities);
			Add(new EntityFactory("ArticleTimelineEventsEntity"), numEntities);
			// % protected region % [Modify UserEntityFactorySingleTheoryData entities here] end
		}
	}

	// % protected region % [Add any further custom EntityFactoryTheoryData here] off begin
	// % protected region % [Add any further custom EntityFactoryTheoryData here] end

}