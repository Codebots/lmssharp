/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ServersideCourseLessonsEntity = Lmssharp.Models.CourseLessonsEntity;

namespace APITests.EntityObjects.Models
{
	/// <summary>
	/// A join table between courses and lessons.
	/// </summary>
	public class CourseLessonsEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public int? Order { get; set; }

		public Guid? LessonId { get; set; }
		public Guid? CourseId { get; set; }

		public CourseLessonsEntityDto(CourseLessonsEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Order = model.Order;
			LessonId = model.LessonId;
			CourseId = model.CourseId;
		}

		public CourseLessonsEntityDto(ServersideCourseLessonsEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Order = model.Order;
			LessonId = model.LessonId;
			CourseId = model.CourseId;
		}

		public CourseLessonsEntity GetTesttargetCourseLessonsEntity()
		{
			return new CourseLessonsEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Order = Order,
				LessonId = LessonId,
				CourseId = CourseId,
			};
		}

		public ServersideCourseLessonsEntity GetServersideCourseLessonsEntity()
		{
			return new ServersideCourseLessonsEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Order = Order,
				LessonId = LessonId,
				CourseId = CourseId,
			};
		}

		public static ServersideCourseLessonsEntity Convert(CourseLessonsEntity model)
		{
			var dto = new CourseLessonsEntityDto(model);
			return dto.GetServersideCourseLessonsEntity();
		}

		public static CourseLessonsEntity Convert(ServersideCourseLessonsEntity model)
		{
			var dto = new CourseLessonsEntityDto(model);
			return dto.GetTesttargetCourseLessonsEntity();
		}
	}
}