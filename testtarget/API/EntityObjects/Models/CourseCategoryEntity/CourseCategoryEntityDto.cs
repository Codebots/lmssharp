/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ServersideCourseCategoryEntity = Lmssharp.Models.CourseCategoryEntity;

namespace APITests.EntityObjects.Models
{
	public class CourseCategoryEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public String Name { get; set; }
		public String Colour { get; set; }
		public String Summary { get; set; }

		public ICollection<CourseEntity> Coursess { get; set; }

		public CourseCategoryEntityDto(CourseCategoryEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Name = model.Name;
			Colour = model.Colour;
			Summary = model.Summary;
			Coursess = model.Coursess;
		}

		public CourseCategoryEntityDto(ServersideCourseCategoryEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Name = model.Name;
			Colour = model.Colour;
			Summary = model.Summary;
			Coursess = model.Coursess.Select(CourseEntityDto.Convert).ToList();
		}

		public CourseCategoryEntity GetTesttargetCourseCategoryEntity()
		{
			return new CourseCategoryEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Name = Name,
				Colour = Colour,
				Summary = Summary,
				Coursess = Coursess,
			};
		}

		public ServersideCourseCategoryEntity GetServersideCourseCategoryEntity()
		{
			return new ServersideCourseCategoryEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Name = Name,
				Colour = Colour,
				Summary = Summary,
				Coursess = Coursess?.Select(CourseEntityDto.Convert).ToList(),
			};
		}

		public static ServersideCourseCategoryEntity Convert(CourseCategoryEntity model)
		{
			var dto = new CourseCategoryEntityDto(model);
			return dto.GetServersideCourseCategoryEntity();
		}

		public static CourseCategoryEntity Convert(ServersideCourseCategoryEntity model)
		{
			var dto = new CourseCategoryEntityDto(model);
			return dto.GetTesttargetCourseCategoryEntity();
		}
	}
}