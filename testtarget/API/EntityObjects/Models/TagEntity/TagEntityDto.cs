/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ServersideTagEntity = Lmssharp.Models.TagEntity;

namespace APITests.EntityObjects.Models
{
	/// <summary>
	/// A tag for an article
	/// </summary>
	public class TagEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public String Name { get; set; }

		public ICollection<ArticlesTags> Tagss { get; set; }

		public TagEntityDto(TagEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Name = model.Name;
			Tagss = model.Tagss;
		}

		public TagEntityDto(ServersideTagEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Name = model.Name;
			Tagss  = model.Tagss == null ? null :ArticlesTagsDto.Convert(model.Tagss);
		}

		public TagEntity GetTesttargetTagEntity()
		{
			return new TagEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Name = Name,
				Tagss = Tagss,
			};
		}

		public ServersideTagEntity GetServersideTagEntity()
		{
			return new ServersideTagEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Name = Name,
				Tagss = Tagss == null ? null :ArticlesTagsDto.Convert(Tagss),
			};
		}

		public static ServersideTagEntity Convert(TagEntity model)
		{
			var dto = new TagEntityDto(model);
			return dto.GetServersideTagEntity();
		}

		public static TagEntity Convert(ServersideTagEntity model)
		{
			var dto = new TagEntityDto(model);
			return dto.GetTesttargetTagEntity();
		}
	}
}