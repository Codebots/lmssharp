/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ServersideWorkflowEntity = Lmssharp.Models.WorkflowEntity;

namespace APITests.EntityObjects.Models
{
	/// <summary>
	/// Workflow within the application
	/// </summary>
	public class WorkflowEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public String Name { get; set; }

		public ICollection<WorkflowVersionEntity> Versionss { get; set; }
		public Guid? CurrentVersionId { get; set; }

		public WorkflowEntityDto(WorkflowEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Name = model.Name;
			Versionss = model.Versionss;
			CurrentVersionId = model.CurrentVersionId;
		}

		public WorkflowEntityDto(ServersideWorkflowEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Name = model.Name;
			Versionss = model.Versionss.Select(WorkflowVersionEntityDto.Convert).ToList();
			CurrentVersionId = model.CurrentVersionId;
		}

		public WorkflowEntity GetTesttargetWorkflowEntity()
		{
			return new WorkflowEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Name = Name,
				Versionss = Versionss,
				CurrentVersionId = CurrentVersionId,
			};
		}

		public ServersideWorkflowEntity GetServersideWorkflowEntity()
		{
			return new ServersideWorkflowEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Name = Name,
				Versionss = Versionss?.Select(WorkflowVersionEntityDto.Convert).ToList(),
				CurrentVersionId = CurrentVersionId,
			};
		}

		public static ServersideWorkflowEntity Convert(WorkflowEntity model)
		{
			var dto = new WorkflowEntityDto(model);
			return dto.GetServersideWorkflowEntity();
		}

		public static WorkflowEntity Convert(ServersideWorkflowEntity model)
		{
			var dto = new WorkflowEntityDto(model);
			return dto.GetTesttargetWorkflowEntity();
		}
	}
}