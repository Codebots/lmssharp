/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Lmssharp.Enums;
using TestEnums = EntityObject.Enums;
using ServersideLessonEntity = Lmssharp.Models.LessonEntity;

namespace APITests.EntityObjects.Models
{
	/// <summary>
	/// A lesson is a part of a course
	/// </summary>
	public class LessonEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public string Name { get; set; }
		public String Summary { get; set; }
		public String Description { get; set; }
		public Guid? CoverImageId { get; set; }
		public int? Duration { get; set; }
		public Difficulty Difficulty { get; set; }

		public ICollection<CourseLessonsEntity> CourseLessonss { get; set; }

		public LessonEntityDto(LessonEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Name = model.Name;
			Summary = model.Summary;
			Description = model.Description;
			CoverImageId = model.CoverImageId;
			Duration = model.Duration;
			Difficulty = (Difficulty)model.Difficulty;
			CourseLessonss = model.CourseLessonss;
		}

		public LessonEntityDto(ServersideLessonEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Name = model.Name;
			Summary = model.Summary;
			Description = model.Description;
			CoverImageId = model.CoverImageId;
			Duration = model.Duration;
			Difficulty = model.Difficulty;
			CourseLessonss = model.CourseLessonss.Select(CourseLessonsEntityDto.Convert).ToList();
		}

		public LessonEntity GetTesttargetLessonEntity()
		{
			return new LessonEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Name = Name,
				Summary = Summary,
				Description = Description,
				CoverImageId = CoverImageId,
				Duration = Duration,
				Difficulty = (TestEnums.Difficulty)Difficulty,
				CourseLessonss = CourseLessonss,
			};
		}

		public ServersideLessonEntity GetServersideLessonEntity()
		{
			return new ServersideLessonEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Name = Name,
				Summary = Summary,
				Description = Description,
				CoverImageId = CoverImageId,
				Duration = Duration,
				Difficulty = Difficulty,
				CourseLessonss = CourseLessonss?.Select(CourseLessonsEntityDto.Convert).ToList(),
			};
		}

		public static ServersideLessonEntity Convert(LessonEntity model)
		{
			var dto = new LessonEntityDto(model);
			return dto.GetServersideLessonEntity();
		}

		public static LessonEntity Convert(ServersideLessonEntity model)
		{
			var dto = new LessonEntityDto(model);
			return dto.GetTesttargetLessonEntity();
		}
	}
}