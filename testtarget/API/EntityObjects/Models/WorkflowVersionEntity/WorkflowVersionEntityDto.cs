/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ServersideWorkflowVersionEntity = Lmssharp.Models.WorkflowVersionEntity;

namespace APITests.EntityObjects.Models
{
	/// <summary>
	/// Version of Workflow
	/// </summary>
	public class WorkflowVersionEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public String WorkflowName { get; set; }
		public String WorkflowDescription { get; set; }
		public int? VersionNumber { get; set; }
		public Boolean? ArticleAssociation { get; set; }

		public ICollection<WorkflowStateEntity> Statess { get; set; }
		public Guid WorkflowId { get; set; }

		public WorkflowVersionEntityDto(WorkflowVersionEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			WorkflowName = model.WorkflowName;
			WorkflowDescription = model.WorkflowDescription;
			VersionNumber = model.VersionNumber;
			ArticleAssociation = model.ArticleAssociation;
			Statess = model.Statess;
			WorkflowId = model.WorkflowId;
		}

		public WorkflowVersionEntityDto(ServersideWorkflowVersionEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			WorkflowName = model.WorkflowName;
			WorkflowDescription = model.WorkflowDescription;
			VersionNumber = model.VersionNumber;
			ArticleAssociation = model.ArticleAssociation;
			Statess = model.Statess.Select(WorkflowStateEntityDto.Convert).ToList();
			WorkflowId = model.WorkflowId;
		}

		public WorkflowVersionEntity GetTesttargetWorkflowVersionEntity()
		{
			return new WorkflowVersionEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				WorkflowName = WorkflowName,
				WorkflowDescription = WorkflowDescription,
				VersionNumber = VersionNumber,
				ArticleAssociation = ArticleAssociation,
				Statess = Statess,
				WorkflowId = WorkflowId,
			};
		}

		public ServersideWorkflowVersionEntity GetServersideWorkflowVersionEntity()
		{
			return new ServersideWorkflowVersionEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				WorkflowName = WorkflowName,
				WorkflowDescription = WorkflowDescription,
				VersionNumber = VersionNumber,
				ArticleAssociation = ArticleAssociation,
				Statess = Statess?.Select(WorkflowStateEntityDto.Convert).ToList(),
				WorkflowId = WorkflowId,
			};
		}

		public static ServersideWorkflowVersionEntity Convert(WorkflowVersionEntity model)
		{
			var dto = new WorkflowVersionEntityDto(model);
			return dto.GetServersideWorkflowVersionEntity();
		}

		public static WorkflowVersionEntity Convert(ServersideWorkflowVersionEntity model)
		{
			var dto = new WorkflowVersionEntityDto(model);
			return dto.GetTesttargetWorkflowVersionEntity();
		}
	}
}