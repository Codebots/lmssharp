/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ServersideLessonSubmissionEntity = Lmssharp.Models.LessonSubmissionEntity;

namespace APITests.EntityObjects.Models
{
	/// <summary>
	/// User submissions for a lesson
	/// </summary>
	public class LessonSubmissionEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public Boolean? Completed { get; set; }
		public int? Score { get; set; }
		public Boolean? Passed { get; set; }

		public Guid? UserId { get; set; }

		public LessonSubmissionEntityDto(LessonSubmissionEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Completed = model.Completed;
			Score = model.Score;
			Passed = model.Passed;
			UserId = model.UserId;
		}

		public LessonSubmissionEntityDto(ServersideLessonSubmissionEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Completed = model.Completed;
			Score = model.Score;
			Passed = model.Passed;
			UserId = model.UserId;
		}

		public LessonSubmissionEntity GetTesttargetLessonSubmissionEntity()
		{
			return new LessonSubmissionEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Completed = Completed,
				Score = Score,
				Passed = Passed,
				UserId = UserId,
			};
		}

		public ServersideLessonSubmissionEntity GetServersideLessonSubmissionEntity()
		{
			return new ServersideLessonSubmissionEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Completed = Completed,
				Score = Score,
				Passed = Passed,
				UserId = UserId,
			};
		}

		public static ServersideLessonSubmissionEntity Convert(LessonSubmissionEntity model)
		{
			var dto = new LessonSubmissionEntityDto(model);
			return dto.GetServersideLessonSubmissionEntity();
		}

		public static LessonSubmissionEntity Convert(ServersideLessonSubmissionEntity model)
		{
			var dto = new LessonSubmissionEntityDto(model);
			return dto.GetTesttargetLessonSubmissionEntity();
		}
	}
}