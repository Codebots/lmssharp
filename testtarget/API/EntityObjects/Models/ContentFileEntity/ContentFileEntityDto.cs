/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ServersideContentFileEntity = Lmssharp.Models.ContentFileEntity;

namespace APITests.EntityObjects.Models
{
	/// <summary>
	/// Files for the content of an article
	/// </summary>
	public class ContentFileEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public Guid? FileId { get; set; }


		public ContentFileEntityDto(ContentFileEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			FileId = model.FileId;
		}

		public ContentFileEntityDto(ServersideContentFileEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			FileId = model.FileId;
		}

		public ContentFileEntity GetTesttargetContentFileEntity()
		{
			return new ContentFileEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				FileId = FileId,
			};
		}

		public ServersideContentFileEntity GetServersideContentFileEntity()
		{
			return new ServersideContentFileEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				FileId = FileId,
			};
		}

		public static ServersideContentFileEntity Convert(ContentFileEntity model)
		{
			var dto = new ContentFileEntityDto(model);
			return dto.GetServersideContentFileEntity();
		}

		public static ContentFileEntity Convert(ServersideContentFileEntity model)
		{
			var dto = new ContentFileEntityDto(model);
			return dto.GetTesttargetContentFileEntity();
		}
	}
}