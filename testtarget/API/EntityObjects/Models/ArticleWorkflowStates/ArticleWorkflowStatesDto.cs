/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Lmssharp.Security;
using Lmssharp.Security.Acl;
using ServersideArticleWorkflowStates = Lmssharp.Models.ArticleWorkflowStates;

namespace APITests.EntityObjects.Models
{
	public class ArticleWorkflowStatesDto
	{
		//public Guid Owner { get; set; }
		public Guid ArticleId { get; set; }
		public Guid WorkflowStatesId { get; set; }

		public ArticleWorkflowStatesDto(ArticleWorkflowStates model)
		{
			//Owner = model.Owner;
			ArticleId = model.ArticleId;
			WorkflowStatesId = model.WorkflowStatesId;
		}

		public ArticleWorkflowStatesDto(ServersideArticleWorkflowStates model)
		{
			//Owner = model.Owner;
			ArticleId = model.ArticleId;
			WorkflowStatesId = model.WorkflowStatesId;
		}

		public ServersideArticleWorkflowStates GetServersideArticleWorkflowStates ()
		{
			return new ServersideArticleWorkflowStates()
			{
				//Owner = Owner,
				ArticleId = ArticleId,
				WorkflowStatesId = WorkflowStatesId,
			};
		}

		public ArticleWorkflowStates GetTesttargetArticleWorkflowStates ()
		{
			return new ArticleWorkflowStates()
			{
				//Owner = Owner,
				ArticleId = ArticleId,
				WorkflowStatesId = WorkflowStatesId,
			};
		}

		public static ICollection<ServersideArticleWorkflowStates> Convert(ICollection<ArticleWorkflowStates> collection)
		{
			var newCollection = new List<ServersideArticleWorkflowStates>();


			foreach (var item in collection)
			{
				newCollection.Add(new ArticleWorkflowStatesDto(item).GetServersideArticleWorkflowStates());
			}
			return newCollection;
		}

		public static ICollection<ArticleWorkflowStates> Convert(ICollection<ServersideArticleWorkflowStates> collection)
		{
			var newCollection = new List<ArticleWorkflowStates>();

			foreach (var item in collection)
			{
				newCollection.Add(new ArticleWorkflowStatesDto(item).GetTesttargetArticleWorkflowStates());
			}
			return newCollection;
		}
	}
}