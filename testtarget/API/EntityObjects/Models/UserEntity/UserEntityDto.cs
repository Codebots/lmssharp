/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ServersideUserEntity = Lmssharp.Models.UserEntity;

namespace APITests.EntityObjects.Models
{
	/// <summary>
	/// Users of the library
	/// </summary>
	public class UserEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public String FirstName { get; set; }
		public String LastName { get; set; }

		public ICollection<ArticleEntity> CreatedArticles { get; set; }
		public ICollection<LessonSubmissionEntity> LessonSubmissionss { get; set; }
		public ICollection<ArticleEntity> UpdatedArticles { get; set; }

		public UserEntityDto(UserEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			FirstName = model.FirstName;
			LastName = model.LastName;
			CreatedArticles = model.CreatedArticles;
			LessonSubmissionss = model.LessonSubmissionss;
			UpdatedArticles = model.UpdatedArticles;
		}

		public UserEntityDto(ServersideUserEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			FirstName = model.FirstName;
			LastName = model.LastName;
			CreatedArticles = model.CreatedArticles.Select(ArticleEntityDto.Convert).ToList();
			LessonSubmissionss = model.LessonSubmissionss.Select(LessonSubmissionEntityDto.Convert).ToList();
			UpdatedArticles = model.UpdatedArticles.Select(ArticleEntityDto.Convert).ToList();
		}

		public UserEntity GetTesttargetUserEntity()
		{
			return new UserEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				FirstName = FirstName,
				LastName = LastName,
				CreatedArticles = CreatedArticles,
				LessonSubmissionss = LessonSubmissionss,
				UpdatedArticles = UpdatedArticles,
			};
		}

		public ServersideUserEntity GetServersideUserEntity()
		{
			return new ServersideUserEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				FirstName = FirstName,
				LastName = LastName,
				CreatedArticles = CreatedArticles?.Select(ArticleEntityDto.Convert).ToList(),
				LessonSubmissionss = LessonSubmissionss?.Select(LessonSubmissionEntityDto.Convert).ToList(),
				UpdatedArticles = UpdatedArticles?.Select(ArticleEntityDto.Convert).ToList(),
			};
		}

		public static ServersideUserEntity Convert(UserEntity model)
		{
			var dto = new UserEntityDto(model);
			return dto.GetServersideUserEntity();
		}

		public static UserEntity Convert(ServersideUserEntity model)
		{
			var dto = new UserEntityDto(model);
			return dto.GetTesttargetUserEntity();
		}
	}
}