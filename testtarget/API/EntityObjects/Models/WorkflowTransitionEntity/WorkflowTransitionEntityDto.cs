/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ServersideWorkflowTransitionEntity = Lmssharp.Models.WorkflowTransitionEntity;

namespace APITests.EntityObjects.Models
{
	/// <summary>
	/// The transtions within the workflows
	/// </summary>
	public class WorkflowTransitionEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public String TransitionName { get; set; }

		public Guid SourceStateId { get; set; }
		public Guid TargetStateId { get; set; }

		public WorkflowTransitionEntityDto(WorkflowTransitionEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			TransitionName = model.TransitionName;
			SourceStateId = model.SourceStateId;
			TargetStateId = model.TargetStateId;
		}

		public WorkflowTransitionEntityDto(ServersideWorkflowTransitionEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			TransitionName = model.TransitionName;
			SourceStateId = model.SourceStateId;
			TargetStateId = model.TargetStateId;
		}

		public WorkflowTransitionEntity GetTesttargetWorkflowTransitionEntity()
		{
			return new WorkflowTransitionEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				TransitionName = TransitionName,
				SourceStateId = SourceStateId,
				TargetStateId = TargetStateId,
			};
		}

		public ServersideWorkflowTransitionEntity GetServersideWorkflowTransitionEntity()
		{
			return new ServersideWorkflowTransitionEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				TransitionName = TransitionName,
				SourceStateId = SourceStateId,
				TargetStateId = TargetStateId,
			};
		}

		public static ServersideWorkflowTransitionEntity Convert(WorkflowTransitionEntity model)
		{
			var dto = new WorkflowTransitionEntityDto(model);
			return dto.GetServersideWorkflowTransitionEntity();
		}

		public static WorkflowTransitionEntity Convert(ServersideWorkflowTransitionEntity model)
		{
			var dto = new WorkflowTransitionEntityDto(model);
			return dto.GetTesttargetWorkflowTransitionEntity();
		}
	}
}