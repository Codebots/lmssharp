/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ServersideWorkflowStateEntity = Lmssharp.Models.WorkflowStateEntity;

namespace APITests.EntityObjects.Models
{
	/// <summary>
	/// State within a workflow
	/// </summary>
	public class WorkflowStateEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public int? DisplayIndex { get; set; }
		public String StepName { get; set; }
		public String StateDescription { get; set; }
		public Boolean? IsStartState { get; set; }

		public Guid WorkflowVersionId { get; set; }
		public ICollection<WorkflowTransitionEntity> OutgoingTransitionss { get; set; }
		public ICollection<WorkflowTransitionEntity> IncomingTransitionss { get; set; }
		public ICollection<ArticleWorkflowStates> Articles { get; set; }

		public WorkflowStateEntityDto(WorkflowStateEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			DisplayIndex = model.DisplayIndex;
			StepName = model.StepName;
			StateDescription = model.StateDescription;
			IsStartState = model.IsStartState;
			WorkflowVersionId = model.WorkflowVersionId;
			OutgoingTransitionss = model.OutgoingTransitionss;
			IncomingTransitionss = model.IncomingTransitionss;
			Articles = model.Articles;
		}

		public WorkflowStateEntityDto(ServersideWorkflowStateEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			DisplayIndex = model.DisplayIndex;
			StepName = model.StepName;
			StateDescription = model.StateDescription;
			IsStartState = model.IsStartState;
			WorkflowVersionId = model.WorkflowVersionId;
			OutgoingTransitionss = model.OutgoingTransitionss.Select(WorkflowTransitionEntityDto.Convert).ToList();
			IncomingTransitionss = model.IncomingTransitionss.Select(WorkflowTransitionEntityDto.Convert).ToList();
			Articles  = model.Articles == null ? null :ArticleWorkflowStatesDto.Convert(model.Articles);
		}

		public WorkflowStateEntity GetTesttargetWorkflowStateEntity()
		{
			return new WorkflowStateEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				DisplayIndex = DisplayIndex,
				StepName = StepName,
				StateDescription = StateDescription,
				IsStartState = IsStartState,
				WorkflowVersionId = WorkflowVersionId,
				OutgoingTransitionss = OutgoingTransitionss,
				IncomingTransitionss = IncomingTransitionss,
				Articles = Articles,
			};
		}

		public ServersideWorkflowStateEntity GetServersideWorkflowStateEntity()
		{
			return new ServersideWorkflowStateEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				DisplayIndex = DisplayIndex,
				StepName = StepName,
				StateDescription = StateDescription,
				IsStartState = IsStartState,
				WorkflowVersionId = WorkflowVersionId,
				OutgoingTransitionss = OutgoingTransitionss?.Select(WorkflowTransitionEntityDto.Convert).ToList(),
				IncomingTransitionss = IncomingTransitionss?.Select(WorkflowTransitionEntityDto.Convert).ToList(),
				Articles = Articles == null ? null :ArticleWorkflowStatesDto.Convert(Articles),
			};
		}

		public static ServersideWorkflowStateEntity Convert(WorkflowStateEntity model)
		{
			var dto = new WorkflowStateEntityDto(model);
			return dto.GetServersideWorkflowStateEntity();
		}

		public static WorkflowStateEntity Convert(ServersideWorkflowStateEntity model)
		{
			var dto = new WorkflowStateEntityDto(model);
			return dto.GetTesttargetWorkflowStateEntity();
		}
	}
}