/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using Lmssharp.Enums;
using TestEnums = EntityObject.Enums;
using ServersideCourseEntity = Lmssharp.Models.CourseEntity;

namespace APITests.EntityObjects.Models
{
	/// <summary>
	/// A course is a collection of many lessons
	/// </summary>
	public class CourseEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public String Name { get; set; }
		public String Summary { get; set; }
		public Guid? CoverImageId { get; set; }
		public Difficulty Difficulty { get; set; }

		public ICollection<CourseLessonsEntity> CourseLessonss { get; set; }
		public Guid? CourseCategoryId { get; set; }

		public CourseEntityDto(CourseEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Name = model.Name;
			Summary = model.Summary;
			CoverImageId = model.CoverImageId;
			Difficulty = (Difficulty)model.Difficulty;
			CourseLessonss = model.CourseLessonss;
			CourseCategoryId = model.CourseCategoryId;
		}

		public CourseEntityDto(ServersideCourseEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Name = model.Name;
			Summary = model.Summary;
			CoverImageId = model.CoverImageId;
			Difficulty = model.Difficulty;
			CourseLessonss = model.CourseLessonss.Select(CourseLessonsEntityDto.Convert).ToList();
			CourseCategoryId = model.CourseCategoryId;
		}

		public CourseEntity GetTesttargetCourseEntity()
		{
			return new CourseEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Name = Name,
				Summary = Summary,
				CoverImageId = CoverImageId,
				Difficulty = (TestEnums.Difficulty)Difficulty,
				CourseLessonss = CourseLessonss,
				CourseCategoryId = CourseCategoryId,
			};
		}

		public ServersideCourseEntity GetServersideCourseEntity()
		{
			return new ServersideCourseEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Name = Name,
				Summary = Summary,
				CoverImageId = CoverImageId,
				Difficulty = Difficulty,
				CourseLessonss = CourseLessonss?.Select(CourseLessonsEntityDto.Convert).ToList(),
				CourseCategoryId = CourseCategoryId,
			};
		}

		public static ServersideCourseEntity Convert(CourseEntity model)
		{
			var dto = new CourseEntityDto(model);
			return dto.GetServersideCourseEntity();
		}

		public static CourseEntity Convert(ServersideCourseEntity model)
		{
			var dto = new CourseEntityDto(model);
			return dto.GetTesttargetCourseEntity();
		}
	}
}