/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using ServersideArticleEntity = Lmssharp.Models.ArticleEntity;

namespace APITests.EntityObjects.Models
{
	public class ArticleEntityDto
	{
		public Guid Id { get; set; }
		public DateTime Created { get; set; }
		public DateTime Modified { get; set; }
		public String Title { get; set; }
		public String Summary { get; set; }
		public String Content { get; set; }

		public Guid? BookId { get; set; }
		public Guid? CreatedById { get; set; }
		public Guid? UpdatedById { get; set; }
		public ICollection<ArticleTimelineEventsEntity> LoggedEvents { get; set; }
		public ICollection<ArticlesTags> Articless { get; set; }
		public ICollection<ArticleWorkflowStates> WorkflowStatess { get; set; }

		public ArticleEntityDto(ArticleEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Title = model.Title;
			Summary = model.Summary;
			Content = model.Content;
			BookId = model.BookId;
			CreatedById = model.CreatedById;
			UpdatedById = model.UpdatedById;
			LoggedEvents = model.LoggedEvents;
			Articless = model.Articless;
			WorkflowStatess = model.WorkflowStatess;
		}

		public ArticleEntityDto(ServersideArticleEntity model)
		{
			Id = model.Id;
			Created = model.Created;
			Modified = model.Modified;
			Title = model.Title;
			Summary = model.Summary;
			Content = model.Content;
			BookId = model.BookId;
			CreatedById = model.CreatedById;
			UpdatedById = model.UpdatedById;
			LoggedEvents = model.LoggedEvents.Select(ArticleTimelineEventsEntityDto.Convert).ToList();
			Articless  = model.Articless == null ? null :ArticlesTagsDto.Convert(model.Articless);
			WorkflowStatess  = model.WorkflowStatess == null ? null :ArticleWorkflowStatesDto.Convert(model.WorkflowStatess);
		}

		public ArticleEntity GetTesttargetArticleEntity()
		{
			return new ArticleEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Title = Title,
				Summary = Summary,
				Content = Content,
				BookId = BookId,
				CreatedById = CreatedById,
				UpdatedById = UpdatedById,
				LoggedEvents = LoggedEvents,
				Articless = Articless,
				WorkflowStatess = WorkflowStatess,
			};
		}

		public ServersideArticleEntity GetServersideArticleEntity()
		{
			return new ServersideArticleEntity
			{
				Id = Id,
				Created = Created,
				Modified = Modified,
				Title = Title,
				Summary = Summary,
				Content = Content,
				BookId = BookId,
				CreatedById = CreatedById,
				UpdatedById = UpdatedById,
				LoggedEvents = LoggedEvents?.Select(ArticleTimelineEventsEntityDto.Convert).ToList(),
				Articless = Articless == null ? null :ArticlesTagsDto.Convert(Articless),
				WorkflowStatess = WorkflowStatess == null ? null :ArticleWorkflowStatesDto.Convert(WorkflowStatess),
			};
		}

		public static ServersideArticleEntity Convert(ArticleEntity model)
		{
			var dto = new ArticleEntityDto(model);
			return dto.GetServersideArticleEntity();
		}

		public static ArticleEntity Convert(ServersideArticleEntity model)
		{
			var dto = new ArticleEntityDto(model);
			return dto.GetTesttargetArticleEntity();
		}
	}
}