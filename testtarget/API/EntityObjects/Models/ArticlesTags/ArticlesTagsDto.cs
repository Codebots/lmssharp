/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Lmssharp.Security;
using Lmssharp.Security.Acl;
using ServersideArticlesTags = Lmssharp.Models.ArticlesTags;

namespace APITests.EntityObjects.Models
{
	public class ArticlesTagsDto
	{
		//public Guid Owner { get; set; }
		public Guid ArticlesId { get; set; }
		public Guid TagsId { get; set; }

		public ArticlesTagsDto(ArticlesTags model)
		{
			//Owner = model.Owner;
			ArticlesId = model.ArticlesId;
			TagsId = model.TagsId;
		}

		public ArticlesTagsDto(ServersideArticlesTags model)
		{
			//Owner = model.Owner;
			ArticlesId = model.ArticlesId;
			TagsId = model.TagsId;
		}

		public ServersideArticlesTags GetServersideArticlesTags ()
		{
			return new ServersideArticlesTags()
			{
				//Owner = Owner,
				ArticlesId = ArticlesId,
				TagsId = TagsId,
			};
		}

		public ArticlesTags GetTesttargetArticlesTags ()
		{
			return new ArticlesTags()
			{
				//Owner = Owner,
				ArticlesId = ArticlesId,
				TagsId = TagsId,
			};
		}

		public static ICollection<ServersideArticlesTags> Convert(ICollection<ArticlesTags> collection)
		{
			var newCollection = new List<ServersideArticlesTags>();


			foreach (var item in collection)
			{
				newCollection.Add(new ArticlesTagsDto(item).GetServersideArticlesTags());
			}
			return newCollection;
		}

		public static ICollection<ArticlesTags> Convert(ICollection<ServersideArticlesTags> collection)
		{
			var newCollection = new List<ArticlesTags>();

			foreach (var item in collection)
			{
				newCollection.Add(new ArticlesTagsDto(item).GetTesttargetArticlesTags());
			}
			return newCollection;
		}
	}
}