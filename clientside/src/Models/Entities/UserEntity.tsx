/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
import * as React from 'react';
import _ from 'lodash';
import moment from 'moment';
import { action, observable, runInAction } from 'mobx';
import { IAttributeGroup, Model, IModelAttributes, attribute, entity, jsonReplacerFn } from 'Models/Model';
import * as Validators from 'Validators';
import * as Models from '../Entities';
import { CRUD } from '../CRUDOptions';
import * as AttrUtils from "Util/AttributeUtils";
import { IAcl } from 'Models/Security/IAcl';
import { makeFetchManyToManyFunc, makeFetchOneToManyFunc, makeJoinEqualsFunc, makeEnumFetchFunction } from 'Util/EntityUtils';
import { AdministratorUserEntity } from 'Models/Security/Acl/AdministratorUserEntity';
import { UserUserEntity } from 'Models/Security/Acl/UserUserEntity';
import * as Enums from '../Enums';
import { IOrderByCondition } from 'Views/Components/ModelCollection/ModelQuery';
import { EntityFormMode } from 'Views/Components/Helpers/Common';
import { SERVER_URL } from 'Constants';
import {SuperAdministratorScheme} from '../Security/Acl/SuperAdministratorScheme';
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

export interface IUserEntityAttributes extends IModelAttributes {
	email: string;
	firstName: string;
	lastName: string;

	createdArticles: Array<Models.ArticleEntity | Models.IArticleEntityAttributes>;
	lessonSubmissionss: Array<Models.LessonSubmissionEntity | Models.ILessonSubmissionEntityAttributes>;
	updatedArticles: Array<Models.ArticleEntity | Models.IArticleEntityAttributes>;
	// % protected region % [Add any custom attributes to the interface here] off begin
	// % protected region % [Add any custom attributes to the interface here] end
}

// % protected region % [Customise your entity metadata here] off begin
@entity('UserEntity', 'User')
// % protected region % [Customise your entity metadata here] end
export default class UserEntity extends Model implements IUserEntityAttributes {
	public static acls: IAcl[] = [
		new SuperAdministratorScheme(),
		new AdministratorUserEntity(),
		new UserUserEntity(),
		// % protected region % [Add any further ACL entries here] off begin
		// % protected region % [Add any further ACL entries here] end
	];

	/**
	 * Fields to exclude from the JSON serialization in create operations.
	 */
	public static excludeFromCreate: string[] = [
		// % protected region % [Add any custom create exclusions here] off begin
		// % protected region % [Add any custom create exclusions here] end
	];

	/**
	 * Fields to exclude from the JSON serialization in update operations.
	 */
	public static excludeFromUpdate: string[] = [
		'email',
		// % protected region % [Add any custom update exclusions here] off begin
		// % protected region % [Add any custom update exclusions here] end
	];

	// % protected region % [Modify props to the crud options here for attribute 'Email'] off begin
	@Validators.Email()
	@Validators.Required()
	@observable
	@attribute()
	@CRUD({
		name: 'Email',
		displayType: 'displayfield',
		order: 10,
		createFieldType: 'textfield',
		headerColumn: true,
		searchable: true,
		searchFunction: 'like',
		searchTransform: AttrUtils.standardiseString,
	})
	public email: string;
	// % protected region % [Modify props to the crud options here for attribute 'Email'] end

	// % protected region % [Modify props to the crud options here for attribute 'Password'] off begin
	@Validators.Length(6)
	@observable
	@CRUD({
		name: 'Password',
		displayType: 'hidden',
		order: 20,
		createFieldType: 'password',
	})
	public password: string;
	// % protected region % [Modify props to the crud options here for attribute 'Password'] end

	// % protected region % [Modify props to the crud options here for attribute 'Confirm Password'] off begin
	@Validators.Custom('Password Match', (e: string, target: UserEntity) => {
		return new Promise(resolve => resolve(target.password !== e ? "Password fields do not match" : null))
	})
	@observable
	@CRUD({
		name: 'Confirm Password',
		displayType: 'hidden',
		order: 30,
		createFieldType: 'password',
	})
	public _confirmPassword: string;
	// % protected region % [Modify props to the crud options here for attribute 'Confirm Password'] end

	// % protected region % [Modify props to the crud options here for attribute 'First Name'] off begin
	/**
	 * First name of the user
	 */
	
	@observable
	@attribute()
	@CRUD({
		name: 'First Name',
		displayType: 'textfield',
		order: 40,
		headerColumn: true,
		searchable: true,
		searchFunction: 'like',
		searchTransform: AttrUtils.standardiseString,
	})
	public firstName: string;
	// % protected region % [Modify props to the crud options here for attribute 'First Name'] end

	// % protected region % [Modify props to the crud options here for attribute 'Last Name'] off begin
	/**
	 * Last name of the user
	 */
	
	@observable
	@attribute()
	@CRUD({
		name: 'Last Name',
		displayType: 'textfield',
		order: 50,
		headerColumn: true,
		searchable: true,
		searchFunction: 'like',
		searchTransform: AttrUtils.standardiseString,
	})
	public lastName: string;
	// % protected region % [Modify props to the crud options here for attribute 'Last Name'] end

	@observable
	@attribute({isReference: true})
	@CRUD({
		// % protected region % [Modify props to the crud options here for reference 'Created Article'] off begin
		name: "Created Articles",
		displayType: 'reference-multicombobox',
		order: 60,
		referenceTypeFunc: () => Models.ArticleEntity,
		referenceResolveFunction: makeFetchOneToManyFunc({
			relationName: 'createdArticles',
			oppositeEntity: () => Models.ArticleEntity,
		}),
		// % protected region % [Modify props to the crud options here for reference 'Created Article'] end
	})
	public createdArticles: Models.ArticleEntity[] = [];

	@observable
	@attribute({isReference: true})
	@CRUD({
		// % protected region % [Modify props to the crud options here for reference 'Lesson Submissions'] off begin
		name: "Lesson Submissionss",
		displayType: 'reference-multicombobox',
		order: 70,
		referenceTypeFunc: () => Models.LessonSubmissionEntity,
		referenceResolveFunction: makeFetchOneToManyFunc({
			relationName: 'lessonSubmissionss',
			oppositeEntity: () => Models.LessonSubmissionEntity,
		}),
		// % protected region % [Modify props to the crud options here for reference 'Lesson Submissions'] end
	})
	public lessonSubmissionss: Models.LessonSubmissionEntity[] = [];

	@observable
	@attribute({isReference: true})
	@CRUD({
		// % protected region % [Modify props to the crud options here for reference 'Updated article'] off begin
		name: "Updated articles",
		displayType: 'reference-multicombobox',
		order: 80,
		referenceTypeFunc: () => Models.ArticleEntity,
		referenceResolveFunction: makeFetchOneToManyFunc({
			relationName: 'updatedArticles',
			oppositeEntity: () => Models.ArticleEntity,
		}),
		// % protected region % [Modify props to the crud options here for reference 'Updated article'] end
	})
	public updatedArticles: Models.ArticleEntity[] = [];

	// % protected region % [Add any custom attributes to the model here] off begin
	// % protected region % [Add any custom attributes to the model here] end

	constructor(attributes?: Partial<IUserEntityAttributes>) {
		// % protected region % [Add any extra constructor logic before calling super here] off begin
		// % protected region % [Add any extra constructor logic before calling super here] end

		super(attributes);

		// % protected region % [Add any extra constructor logic after calling super here] off begin
		// % protected region % [Add any extra constructor logic after calling super here] end
	}

	/**
	 * Assigns fields from a passed in JSON object to the fields in this model.
	 * Any reference objects that are passed in are converted to models if they are not already.
	 * This function is called from the constructor to assign the initial fields.
	 */
	@action
	public assignAttributes(attributes?: Partial<IUserEntityAttributes>) {
		// % protected region % [Override assign attributes here] off begin
		super.assignAttributes(attributes);

		if (attributes) {
			if (attributes.email) {
				this.email = attributes.email;
			}
			if (attributes.firstName) {
				this.firstName = attributes.firstName;
			}
			if (attributes.lastName) {
				this.lastName = attributes.lastName;
			}
			if (attributes.createdArticles) {
				for (const model of attributes.createdArticles) {
					if (model instanceof Models.ArticleEntity) {
						this.createdArticles.push(model);
					} else {
						this.createdArticles.push(new Models.ArticleEntity(model));
					}
				}
			}
			if (attributes.lessonSubmissionss) {
				for (const model of attributes.lessonSubmissionss) {
					if (model instanceof Models.LessonSubmissionEntity) {
						this.lessonSubmissionss.push(model);
					} else {
						this.lessonSubmissionss.push(new Models.LessonSubmissionEntity(model));
					}
				}
			}
			if (attributes.updatedArticles) {
				for (const model of attributes.updatedArticles) {
					if (model instanceof Models.ArticleEntity) {
						this.updatedArticles.push(model);
					} else {
						this.updatedArticles.push(new Models.ArticleEntity(model));
					}
				}
			}
			// % protected region % [Override assign attributes here] end

			// % protected region % [Add any extra assign attributes logic here] off begin
			// % protected region % [Add any extra assign attributes logic here] end
		}
	}

	/**
	 * Additional fields that are added to GraphQL queries when using the
	 * the managed model APIs.
	 */
	// % protected region % [Customize Default Expands here] off begin
	public defaultExpands = `
		createdArticles {
			${Models.ArticleEntity.getAttributes().join('\n')}
		}
		lessonSubmissionss {
			${Models.LessonSubmissionEntity.getAttributes().join('\n')}
		}
		updatedArticles {
			${Models.ArticleEntity.getAttributes().join('\n')}
		}
	`;
	// % protected region % [Customize Default Expands here] end

	/**
	 * The save method that is called from the admin CRUD components.
	 */
	// % protected region % [Customize Save From Crud here] off begin
	public async saveFromCrud(formMode: EntityFormMode) {
		const relationPath = {
			createdArticles: {},
			lessonSubmissionss: {},
			updatedArticles: {},
		};

		if (formMode === 'create') {
			relationPath['password'] = {};

			if (this.password !== this._confirmPassword) {
				throw "Password fields do not match";
			}
		}
		return this.save(
			relationPath,
			{
				graphQlInputType: formMode === 'create'
					? `[${this.getModelName()}CreateInput]`
					: `[${this.getModelName()}Input]`,
				options: [
					{
						key: 'mergeReferences',
						graphQlType: '[String]',
						value: [
							'createdArticles',
							'lessonSubmissionss',
							'updatedArticles',
						]
					},
				],
			}
		);
	}
	// % protected region % [Customize Save From Crud here] end

	/**
	 * Returns the string representation of this entity to display on the UI.
	 */
	public getDisplayName() {
		// % protected region % [Customise the display name for this entity] off begin
		return this.email;
		// % protected region % [Customise the display name for this entity] end
	}


	// % protected region % [Add any further custom model features here] off begin
	// % protected region % [Add any further custom model features here] end
}