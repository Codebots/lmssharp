/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */

import { IAdministratorEntityAttributes as IAdministratorEntityAttributesImport } from './AdministratorEntity';
import { IArticleEntityAttributes as IArticleEntityAttributesImport } from './ArticleEntity';
import { IBookEntityAttributes as IBookEntityAttributesImport } from './BookEntity';
import { IContentFileEntityAttributes as IContentFileEntityAttributesImport } from './ContentFileEntity';
import { ICourseEntityAttributes as ICourseEntityAttributesImport } from './CourseEntity';
import { ICourseCategoryEntityAttributes as ICourseCategoryEntityAttributesImport } from './CourseCategoryEntity';
import { ICourseLessonsEntityAttributes as ICourseLessonsEntityAttributesImport } from './CourseLessonsEntity';
import { ILessonEntityAttributes as ILessonEntityAttributesImport } from './LessonEntity';
import { ILessonSubmissionEntityAttributes as ILessonSubmissionEntityAttributesImport } from './LessonSubmissionEntity';
import { ITagEntityAttributes as ITagEntityAttributesImport } from './TagEntity';
import { IUserEntityAttributes as IUserEntityAttributesImport } from './UserEntity';
import { IWorkflowEntityAttributes as IWorkflowEntityAttributesImport } from './WorkflowEntity';
import { IWorkflowStateEntityAttributes as IWorkflowStateEntityAttributesImport } from './WorkflowStateEntity';
import { IWorkflowTransitionEntityAttributes as IWorkflowTransitionEntityAttributesImport } from './WorkflowTransitionEntity';
import { IWorkflowVersionEntityAttributes as IWorkflowVersionEntityAttributesImport } from './WorkflowVersionEntity';
import { ILessonEntityFormTileEntityAttributes as ILessonEntityFormTileEntityAttributesImport } from './LessonEntityFormTileEntity';
import { IArticleTimelineEventsEntityAttributes as IArticleTimelineEventsEntityAttributesImport } from './ArticleTimelineEventsEntity';
import { IArticlesTagsAttributes as IArticlesTagsAttributesImport } from './ArticlesTags';
import { IArticleWorkflowStatesAttributes as IArticleWorkflowStatesAttributesImport } from './ArticleWorkflowStates';

export { default as User } from './User';

export { default as AdministratorEntity } from './AdministratorEntity';
export type IAdministratorEntityAttributes = IAdministratorEntityAttributesImport;

export { default as ArticleEntity } from './ArticleEntity';
export type IArticleEntityAttributes = IArticleEntityAttributesImport;

export { default as BookEntity } from './BookEntity';
export type IBookEntityAttributes = IBookEntityAttributesImport;

export { default as ContentFileEntity } from './ContentFileEntity';
export type IContentFileEntityAttributes = IContentFileEntityAttributesImport;

export { default as CourseEntity } from './CourseEntity';
export type ICourseEntityAttributes = ICourseEntityAttributesImport;

export { default as CourseCategoryEntity } from './CourseCategoryEntity';
export type ICourseCategoryEntityAttributes = ICourseCategoryEntityAttributesImport;

export { default as CourseLessonsEntity } from './CourseLessonsEntity';
export type ICourseLessonsEntityAttributes = ICourseLessonsEntityAttributesImport;

export { default as LessonEntity } from './LessonEntity';
export type ILessonEntityAttributes = ILessonEntityAttributesImport;

export { default as LessonSubmissionEntity } from './LessonSubmissionEntity';
export type ILessonSubmissionEntityAttributes = ILessonSubmissionEntityAttributesImport;

export { default as TagEntity } from './TagEntity';
export type ITagEntityAttributes = ITagEntityAttributesImport;

export { default as UserEntity } from './UserEntity';
export type IUserEntityAttributes = IUserEntityAttributesImport;

export { default as WorkflowEntity } from './WorkflowEntity';
export type IWorkflowEntityAttributes = IWorkflowEntityAttributesImport;

export { default as WorkflowStateEntity } from './WorkflowStateEntity';
export type IWorkflowStateEntityAttributes = IWorkflowStateEntityAttributesImport;

export { default as WorkflowTransitionEntity } from './WorkflowTransitionEntity';
export type IWorkflowTransitionEntityAttributes = IWorkflowTransitionEntityAttributesImport;

export { default as WorkflowVersionEntity } from './WorkflowVersionEntity';
export type IWorkflowVersionEntityAttributes = IWorkflowVersionEntityAttributesImport;

export { default as LessonEntityFormTileEntity } from './LessonEntityFormTileEntity';
export type ILessonEntityFormTileEntityAttributes = ILessonEntityFormTileEntityAttributesImport;

export { default as ArticleTimelineEventsEntity } from './ArticleTimelineEventsEntity';
export type IArticleTimelineEventsEntityAttributes = IArticleTimelineEventsEntityAttributesImport;

export { default as ArticlesTags } from './ArticlesTags';
export type IArticlesTagsAttributes = IArticlesTagsAttributesImport;

export { default as ArticleWorkflowStates } from './ArticleWorkflowStates';
export type IArticleWorkflowStatesAttributes = IArticleWorkflowStatesAttributesImport;

