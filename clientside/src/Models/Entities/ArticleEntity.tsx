/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
import * as React from 'react';
import _ from 'lodash';
import moment from 'moment';
import { action, observable, runInAction } from 'mobx';
import { IAttributeGroup, Model, IModelAttributes, attribute, entity, jsonReplacerFn } from 'Models/Model';
import * as Validators from 'Validators';
import * as Models from '../Entities';
import { CRUD } from '../CRUDOptions';
import * as AttrUtils from "Util/AttributeUtils";
import { IAcl } from 'Models/Security/IAcl';
import { makeFetchManyToManyFunc, makeFetchOneToManyFunc, makeJoinEqualsFunc, makeEnumFetchFunction } from 'Util/EntityUtils';
import { AdministratorArticleEntity } from 'Models/Security/Acl/AdministratorArticleEntity';
import { VisitorsArticleEntity } from 'Models/Security/Acl/VisitorsArticleEntity';
import { UserArticleEntity } from 'Models/Security/Acl/UserArticleEntity';
import * as Enums from '../Enums';
import { IOrderByCondition } from 'Views/Components/ModelCollection/ModelQuery';
import { EntityFormMode } from 'Views/Components/Helpers/Common';
import { TimelineModel } from 'Timelines/TimelineModel';
import { SERVER_URL } from 'Constants';
import {SuperAdministratorScheme} from '../Security/Acl/SuperAdministratorScheme';
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

export interface IArticleEntityAttributes extends IModelAttributes {
	title: string;
	summary: string;
	content: string;

	bookId?: string;
	book?: Models.BookEntity | Models.IBookEntityAttributes;
	createdById?: string;
	createdBy?: Models.UserEntity | Models.IUserEntityAttributes;
	updatedById?: string;
	updatedBy?: Models.UserEntity | Models.IUserEntityAttributes;
	loggedEvents: Array<Models.ArticleTimelineEventsEntity | Models.IArticleTimelineEventsEntityAttributes>;
	articless: Array<Models.ArticlesTags | Models.IArticlesTagsAttributes>;
	workflowStatess: Array<Models.ArticleWorkflowStates | Models.IArticleWorkflowStatesAttributes>;
	// % protected region % [Add any custom attributes to the interface here] off begin
	// % protected region % [Add any custom attributes to the interface here] end
}

// % protected region % [Customise your entity metadata here] off begin
@entity('ArticleEntity', 'Article')
// % protected region % [Customise your entity metadata here] end
export default class ArticleEntity extends Model implements IArticleEntityAttributes, TimelineModel  {
	public static acls: IAcl[] = [
		new SuperAdministratorScheme(),
		new AdministratorArticleEntity(),
		new VisitorsArticleEntity(),
		new UserArticleEntity(),
		// % protected region % [Add any further ACL entries here] off begin
		// % protected region % [Add any further ACL entries here] end
	];

	/**
	 * Fields to exclude from the JSON serialization in create operations.
	 */
	public static excludeFromCreate: string[] = [
		// % protected region % [Add any custom create exclusions here] off begin
		// % protected region % [Add any custom create exclusions here] end
	];

	/**
	 * Fields to exclude from the JSON serialization in update operations.
	 */
	public static excludeFromUpdate: string[] = [
		// % protected region % [Add any custom update exclusions here] off begin
		// % protected region % [Add any custom update exclusions here] end
	];

	// % protected region % [Modify props to the crud options here for attribute 'Title'] off begin
	/**
	 * Title of the article
	 */
	
	@observable
	@attribute()
	@CRUD({
		name: 'Title',
		displayType: 'textfield',
		order: 10,
		headerColumn: true,
		searchable: true,
		searchFunction: 'like',
		searchTransform: AttrUtils.standardiseString,
	})
	public title: string;
	// % protected region % [Modify props to the crud options here for attribute 'Title'] end

	// % protected region % [Modify props to the crud options here for attribute 'Summary'] off begin
	/**
	 * Short summary of the article
	 */
	
	@observable
	@attribute()
	@CRUD({
		name: 'Summary',
		displayType: 'textfield',
		order: 20,
		headerColumn: true,
		searchable: true,
		searchFunction: 'like',
		searchTransform: AttrUtils.standardiseString,
	})
	public summary: string;
	// % protected region % [Modify props to the crud options here for attribute 'Summary'] end

	// % protected region % [Modify props to the crud options here for attribute 'Content'] off begin
	/**
	 * The main content of the article
	 */
	
	@observable
	@attribute()
	@CRUD({
		name: 'Content',
		displayType: 'textfield',
		order: 30,
		headerColumn: true,
		searchable: true,
		searchFunction: 'like',
		searchTransform: AttrUtils.standardiseString,
	})
	public content: string;
	// % protected region % [Modify props to the crud options here for attribute 'Content'] end

	@observable
	@CRUD({
		// % protected region % [Modify props to the crud options here for attribute 'Worflow-States'] off begin
		name: 'Workflow States',
		displayType: 'workflow-data',
		order: 40,
		// % protected region % [Modify props to the crud options here for attribute 'Worflow-States'] end
	})
	public workflowStatess: Models.ArticleWorkflowStates[] = [];
	@observable
	@attribute()
	@CRUD({
		// % protected region % [Modify props to the crud options here for reference 'Book'] off begin
		name: 'Book',
		displayType: 'reference-combobox',
		order: 50,
		referenceTypeFunc: () => Models.BookEntity,
		// % protected region % [Modify props to the crud options here for reference 'Book'] end
	})
	public bookId?: string;
	@observable
	@attribute({isReference: true})
	public book: Models.BookEntity;

	@observable
	@attribute()
	@CRUD({
		// % protected region % [Modify props to the crud options here for reference 'Created by'] off begin
		name: 'Created by',
		displayType: 'reference-combobox',
		order: 60,
		referenceTypeFunc: () => Models.UserEntity,
		// % protected region % [Modify props to the crud options here for reference 'Created by'] end
	})
	public createdById?: string;
	@observable
	@attribute({isReference: true})
	public createdBy: Models.UserEntity;

	@observable
	@attribute()
	@CRUD({
		// % protected region % [Modify props to the crud options here for reference 'Updated by'] off begin
		name: 'Updated by',
		displayType: 'reference-combobox',
		order: 70,
		referenceTypeFunc: () => Models.UserEntity,
		// % protected region % [Modify props to the crud options here for reference 'Updated by'] end
	})
	public updatedById?: string;
	@observable
	@attribute({isReference: true})
	public updatedBy: Models.UserEntity;

	@observable
	@attribute({isReference: true})
	@CRUD({
		// % protected region % [Modify props to the crud options here for reference 'Logged Event'] off begin
		name: "Logged Events",
		displayType: 'hidden',
		order: 80,
		referenceTypeFunc: () => Models.ArticleTimelineEventsEntity,
		referenceResolveFunction: makeFetchOneToManyFunc({
			relationName: 'loggedEvents',
			oppositeEntity: () => Models.ArticleTimelineEventsEntity,
		}),
		// % protected region % [Modify props to the crud options here for reference 'Logged Event'] end
	})
	public loggedEvents: Models.ArticleTimelineEventsEntity[] = [];

	@observable
	@attribute({isReference: true})
	@CRUD({
		// % protected region % [Modify props to the crud options here for reference 'Articles'] off begin
		name: 'Articles',
		displayType: 'reference-multicombobox',
		order: 90,
		isJoinEntity: true,
		referenceTypeFunc: () => Models.ArticlesTags,
		optionEqualFunc: makeJoinEqualsFunc('articlesId'),
		referenceResolveFunction: makeFetchManyToManyFunc({
			entityName: 'articleEntity',
			oppositeEntityName: 'tagEntity',
			relationName: 'tags',
			relationOppositeName: 'articles',
			entity: () => Models.ArticleEntity,
			joinEntity: () => Models.ArticlesTags,
			oppositeEntity: () => Models.TagEntity,
		}),
		// % protected region % [Modify props to the crud options here for reference 'Articles'] end
	})
	public articless: Models.ArticlesTags[] = [];

	@observable
	public workflowBehaviourStateIds: Array<string>;
	// % protected region % [Add any custom attributes to the model here] off begin
	// % protected region % [Add any custom attributes to the model here] end

	constructor(attributes?: Partial<IArticleEntityAttributes>) {
		// % protected region % [Add any extra constructor logic before calling super here] off begin
		// % protected region % [Add any extra constructor logic before calling super here] end

		super(attributes);

		// % protected region % [Add any extra constructor logic after calling super here] off begin
		// % protected region % [Add any extra constructor logic after calling super here] end
	}

	/**
	 * Assigns fields from a passed in JSON object to the fields in this model.
	 * Any reference objects that are passed in are converted to models if they are not already.
	 * This function is called from the constructor to assign the initial fields.
	 */
	@action
	public assignAttributes(attributes?: Partial<IArticleEntityAttributes>) {
		// % protected region % [Override assign attributes here] off begin
		super.assignAttributes(attributes);

		if (attributes) {
			if (attributes.title) {
				this.title = attributes.title;
			}
			if (attributes.summary) {
				this.summary = attributes.summary;
			}
			if (attributes.content) {
				this.content = attributes.content;
			}
			if (attributes.book) {
				if (attributes.book instanceof Models.BookEntity) {
					this.book = attributes.book;
					this.bookId = attributes.book.id;
				} else {
					this.book = new Models.BookEntity(attributes.book);
					this.bookId = this.book.id;
				}
			} else if (attributes.bookId !== undefined) {
				this.bookId = attributes.bookId;
			}
			if (attributes.createdBy) {
				if (attributes.createdBy instanceof Models.UserEntity) {
					this.createdBy = attributes.createdBy;
					this.createdById = attributes.createdBy.id;
				} else {
					this.createdBy = new Models.UserEntity(attributes.createdBy);
					this.createdById = this.createdBy.id;
				}
			} else if (attributes.createdById !== undefined) {
				this.createdById = attributes.createdById;
			}
			if (attributes.updatedBy) {
				if (attributes.updatedBy instanceof Models.UserEntity) {
					this.updatedBy = attributes.updatedBy;
					this.updatedById = attributes.updatedBy.id;
				} else {
					this.updatedBy = new Models.UserEntity(attributes.updatedBy);
					this.updatedById = this.updatedBy.id;
				}
			} else if (attributes.updatedById !== undefined) {
				this.updatedById = attributes.updatedById;
			}
			if (attributes.loggedEvents) {
				for (const model of attributes.loggedEvents) {
					if (model instanceof Models.ArticleTimelineEventsEntity) {
						this.loggedEvents.push(model);
					} else {
						this.loggedEvents.push(new Models.ArticleTimelineEventsEntity(model));
					}
				}
			}
			if (attributes.articless) {
				for (const model of attributes.articless) {
					if (model instanceof Models.ArticlesTags) {
						this.articless.push(model);
					} else {
						this.articless.push(new Models.ArticlesTags(model));
					}
				}
			}
			if (attributes.workflowStatess) {
				for (const model of attributes.workflowStatess) {
					if (model instanceof Models.ArticleWorkflowStates) {
						this.workflowStatess.push(model);
					} else {
						this.workflowStatess.push(new Models.ArticleWorkflowStates(model));
					}
				}
			}
			// % protected region % [Override assign attributes here] end

			// % protected region % [Add any extra assign attributes logic here] off begin
			// % protected region % [Add any extra assign attributes logic here] end
		}
	}

	/**
	 * Additional fields that are added to GraphQL queries when using the
	 * the managed model APIs.
	 */
	// % protected region % [Customize Default Expands here] off begin
	public defaultExpands = `
		workflowStatess {
			${Models.ArticleWorkflowStates.getAttributes().join('\n')}
			workflowStates {
				${Models.WorkflowStateEntity.getAttributes().join('\n')}
			}
		}
		articless {
			${Models.ArticlesTags.getAttributes().join('\n')}
			articles {
				${Models.TagEntity.getAttributes().join('\n')}
			}
		}
		book {
			${Models.BookEntity.getAttributes().join('\n')}
		}
		createdBy {
			${Models.UserEntity.getAttributes().join('\n')}
		}
		updatedBy {
			${Models.UserEntity.getAttributes().join('\n')}
		}
		loggedEvents {
			${Models.ArticleTimelineEventsEntity.getAttributes().join('\n')}
		}
	`;
	// % protected region % [Customize Default Expands here] end

	/**
	 * The save method that is called from the admin CRUD components.
	 */
	// % protected region % [Customize Save From Crud here] off begin
	public async saveFromCrud(formMode: EntityFormMode) {
		const relationPath = {
			workflowStatess: {},
			articless: {},
			loggedEvents: {},
		};

		relationPath['workflowBehaviourStateIds'] = {};

		return this.save(
			relationPath,
			{
				options: [
					{
						key: 'mergeReferences',
						graphQlType: '[String]',
						value: [
							'loggedEvents',
							'articless',
							'workflowStatess',
						]
					},
				],
			}
		);
	}
	// % protected region % [Customize Save From Crud here] end

	/**
	 * Returns the string representation of this entity to display on the UI.
	 */
	public getDisplayName() {
		// % protected region % [Customise the display name for this entity] off begin
		return this.id;
		// % protected region % [Customise the display name for this entity] end
	}

	/**
	 * Gets the timeline event entity type for this form.
	 */
	public getTimelineEventEntity = () => {
		// % protected region % [Modify the getTimelineEventEntity here] off begin
		return Models.ArticleTimelineEventsEntity;
		// % protected region % [Modify the getTimelineEventEntity here] end
	}


	// % protected region % [Add any further custom model features here] off begin
	// % protected region % [Add any further custom model features here] end
}