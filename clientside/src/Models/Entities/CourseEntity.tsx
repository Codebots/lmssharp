/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
import * as React from 'react';
import _ from 'lodash';
import moment from 'moment';
import { action, observable, runInAction } from 'mobx';
import { IAttributeGroup, Model, IModelAttributes, attribute, entity, jsonReplacerFn } from 'Models/Model';
import * as Validators from 'Validators';
import * as Models from '../Entities';
import { CRUD } from '../CRUDOptions';
import * as AttrUtils from "Util/AttributeUtils";
import { IAcl } from 'Models/Security/IAcl';
import { makeFetchManyToManyFunc, makeFetchOneToManyFunc, makeJoinEqualsFunc, makeEnumFetchFunction } from 'Util/EntityUtils';
import { AdministratorCourseEntity } from 'Models/Security/Acl/AdministratorCourseEntity';
import { VisitorsCourseEntity } from 'Models/Security/Acl/VisitorsCourseEntity';
import { UserCourseEntity } from 'Models/Security/Acl/UserCourseEntity';
import * as Enums from '../Enums';
import { IOrderByCondition } from 'Views/Components/ModelCollection/ModelQuery';
import { EntityFormMode } from 'Views/Components/Helpers/Common';
import { SERVER_URL } from 'Constants';
import {SuperAdministratorScheme} from '../Security/Acl/SuperAdministratorScheme';
// % protected region % [Add any further imports here] off begin
// % protected region % [Add any further imports here] end

export interface ICourseEntityAttributes extends IModelAttributes {
	name: string;
	summary: string;
	coverImageId: string;
	coverImage: Blob;
	difficulty: Enums.difficulty;

	courseLessonss: Array<Models.CourseLessonsEntity | Models.ICourseLessonsEntityAttributes>;
	courseCategoryId?: string;
	courseCategory?: Models.CourseCategoryEntity | Models.ICourseCategoryEntityAttributes;
	// % protected region % [Add any custom attributes to the interface here] off begin
	// % protected region % [Add any custom attributes to the interface here] end
}

// % protected region % [Customise your entity metadata here] off begin
@entity('CourseEntity', 'Course')
// % protected region % [Customise your entity metadata here] end
export default class CourseEntity extends Model implements ICourseEntityAttributes {
	public static acls: IAcl[] = [
		new SuperAdministratorScheme(),
		new AdministratorCourseEntity(),
		new VisitorsCourseEntity(),
		new UserCourseEntity(),
		// % protected region % [Add any further ACL entries here] off begin
		// % protected region % [Add any further ACL entries here] end
	];

	/**
	 * Fields to exclude from the JSON serialization in create operations.
	 */
	public static excludeFromCreate: string[] = [
		// % protected region % [Add any custom create exclusions here] off begin
		// % protected region % [Add any custom create exclusions here] end
	];

	/**
	 * Fields to exclude from the JSON serialization in update operations.
	 */
	public static excludeFromUpdate: string[] = [
		// % protected region % [Add any custom update exclusions here] off begin
		// % protected region % [Add any custom update exclusions here] end
	];

	// % protected region % [Modify props to the crud options here for attribute 'Name'] off begin
	/**
	 * Name of the course
	 */
	
	@observable
	@attribute()
	@CRUD({
		name: 'Name',
		displayType: 'textfield',
		order: 10,
		headerColumn: true,
		searchable: true,
		searchFunction: 'like',
		searchTransform: AttrUtils.standardiseString,
	})
	public name: string;
	// % protected region % [Modify props to the crud options here for attribute 'Name'] end

	// % protected region % [Modify props to the crud options here for attribute 'Summary'] off begin
	/**
	 * A short summary of the course
	 */
	
	@observable
	@attribute()
	@CRUD({
		name: 'Summary',
		displayType: 'textfield',
		order: 20,
		headerColumn: true,
		searchable: true,
		searchFunction: 'like',
		searchTransform: AttrUtils.standardiseString,
	})
	public summary: string;
	// % protected region % [Modify props to the crud options here for attribute 'Summary'] end

	// % protected region % [Modify props to the crud options here for attribute 'Cover image'] off begin
	/**
	 * A cover image for the course
	 */
	
	@observable
	@attribute({file: 'coverImage'})
	@CRUD({
		name: 'Cover image',
		displayType: 'file',
		order: 30,
		headerColumn: true,
		searchable: true,
		searchFunction: 'equal',
		searchTransform: AttrUtils.standardiseUuid,
		inputProps: {
			imageOnly: true,
		},
		fileAttribute: 'coverImage',
		displayFunction: attr => attr ? <img src={`${SERVER_URL}/api/files/${attr}`} style={{maxWidth: '300px'}} /> : 'No File Attached',
	})
	public coverImageId: string;
	@observable
	public coverImage: Blob;
	// % protected region % [Modify props to the crud options here for attribute 'Cover image'] end

	// % protected region % [Modify props to the crud options here for attribute 'Difficulty'] off begin
	/**
	 * The difficulty of the course
	 */
	
	@observable
	@attribute()
	@CRUD({
		name: 'Difficulty',
		displayType: 'enum-combobox',
		order: 40,
		headerColumn: true,
		searchable: true,
		searchFunction: 'equal',
		searchTransform: (attr: string) => {
			return AttrUtils.standardiseEnum(attr, Enums.difficultyOptions);
		},
		enumResolveFunction: makeEnumFetchFunction(Enums.difficultyOptions),
		displayFunction: (attribute: Enums.difficulty) => Enums.difficultyOptions[attribute],
	})
	public difficulty: Enums.difficulty;
	// % protected region % [Modify props to the crud options here for attribute 'Difficulty'] end

	@observable
	@attribute({isReference: true})
	@CRUD({
		// % protected region % [Modify props to the crud options here for reference 'Course Lessons'] off begin
		name: "Course Lessonss",
		displayType: 'reference-multicombobox',
		order: 50,
		referenceTypeFunc: () => Models.CourseLessonsEntity,
		referenceResolveFunction: makeFetchOneToManyFunc({
			relationName: 'courseLessonss',
			oppositeEntity: () => Models.CourseLessonsEntity,
		}),
		// % protected region % [Modify props to the crud options here for reference 'Course Lessons'] end
	})
	public courseLessonss: Models.CourseLessonsEntity[] = [];

	@observable
	@attribute()
	@CRUD({
		// % protected region % [Modify props to the crud options here for reference 'Course Category'] off begin
		name: 'Course Category',
		displayType: 'reference-combobox',
		order: 60,
		referenceTypeFunc: () => Models.CourseCategoryEntity,
		// % protected region % [Modify props to the crud options here for reference 'Course Category'] end
	})
	public courseCategoryId?: string;
	@observable
	@attribute({isReference: true})
	public courseCategory: Models.CourseCategoryEntity;

	// % protected region % [Add any custom attributes to the model here] off begin
	// % protected region % [Add any custom attributes to the model here] end

	constructor(attributes?: Partial<ICourseEntityAttributes>) {
		// % protected region % [Add any extra constructor logic before calling super here] off begin
		// % protected region % [Add any extra constructor logic before calling super here] end

		super(attributes);

		// % protected region % [Add any extra constructor logic after calling super here] off begin
		// % protected region % [Add any extra constructor logic after calling super here] end
	}

	/**
	 * Assigns fields from a passed in JSON object to the fields in this model.
	 * Any reference objects that are passed in are converted to models if they are not already.
	 * This function is called from the constructor to assign the initial fields.
	 */
	@action
	public assignAttributes(attributes?: Partial<ICourseEntityAttributes>) {
		// % protected region % [Override assign attributes here] off begin
		super.assignAttributes(attributes);

		if (attributes) {
			if (attributes.name) {
				this.name = attributes.name;
			}
			if (attributes.summary) {
				this.summary = attributes.summary;
			}
			if (attributes.coverImage) {
				this.coverImage = attributes.coverImage;
			}
			if (attributes.coverImageId) {
				this.coverImageId = attributes.coverImageId;
			}
			if (attributes.difficulty) {
				this.difficulty = attributes.difficulty;
			}
			if (attributes.courseLessonss) {
				for (const model of attributes.courseLessonss) {
					if (model instanceof Models.CourseLessonsEntity) {
						this.courseLessonss.push(model);
					} else {
						this.courseLessonss.push(new Models.CourseLessonsEntity(model));
					}
				}
			}
			if (attributes.courseCategory) {
				if (attributes.courseCategory instanceof Models.CourseCategoryEntity) {
					this.courseCategory = attributes.courseCategory;
					this.courseCategoryId = attributes.courseCategory.id;
				} else {
					this.courseCategory = new Models.CourseCategoryEntity(attributes.courseCategory);
					this.courseCategoryId = this.courseCategory.id;
				}
			} else if (attributes.courseCategoryId !== undefined) {
				this.courseCategoryId = attributes.courseCategoryId;
			}
			// % protected region % [Override assign attributes here] end

			// % protected region % [Add any extra assign attributes logic here] off begin
			// % protected region % [Add any extra assign attributes logic here] end
		}
	}

	/**
	 * Additional fields that are added to GraphQL queries when using the
	 * the managed model APIs.
	 */
	// % protected region % [Customize Default Expands here] off begin
	public defaultExpands = `
		courseLessonss {
			${Models.CourseLessonsEntity.getAttributes().join('\n')}
		}
		courseCategory {
			${Models.CourseCategoryEntity.getAttributes().join('\n')}
			${Models.CourseCategoryEntity.getFiles().map(f => f.name).join('\n')}
		}
	`;
	// % protected region % [Customize Default Expands here] end

	/**
	 * The save method that is called from the admin CRUD components.
	 */
	// % protected region % [Customize Save From Crud here] off begin
	public async saveFromCrud(formMode: EntityFormMode) {
		const relationPath = {
			courseLessonss: {},
		};
		return this.save(
			relationPath,
			{
				options: [
					{
						key: 'mergeReferences',
						graphQlType: '[String]',
						value: [
							'courseLessonss',
						]
					},
				],
				contentType: 'multipart/form-data',
			}
		);
	}
	// % protected region % [Customize Save From Crud here] end

	/**
	 * Returns the string representation of this entity to display on the UI.
	 */
	public getDisplayName() {
		// % protected region % [Customise the display name for this entity] off begin
		return this.id;
		// % protected region % [Customise the display name for this entity] end
	}


	// % protected region % [Add any further custom model features here] off begin
	// % protected region % [Add any further custom model features here] end
}