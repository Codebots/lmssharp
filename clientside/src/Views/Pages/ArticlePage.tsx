/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
import * as React from 'react';
import { ArticleEntity } from 'Models/Entities';
import SecuredPage from 'Views/Components/Security/SecuredPage';
import EntityCRUD from 'Views/Components/CRUD/EntityCRUD';
import { observer } from 'mobx-react';
import { RouteComponentProps } from 'react-router';
import { getFrontendNavLinks } from 'Views/FrontendNavLinks';
import Navigation, { Orientation } from 'Views/Components/Navigation/Navigation';
// % protected region % [Add any extra imports here] off begin
// % protected region % [Add any extra imports here] end

export interface ArticlePageProps extends RouteComponentProps {
	// % protected region % [Add any extra props here] off begin
	// % protected region % [Add any extra props here] end
}

@observer
// % protected region % [Add any customisations to default class definition here] off begin
class ArticlePage extends React.Component<ArticlePageProps> {
// % protected region % [Add any customisations to default class definition here] end


	// % protected region % [Add class properties here] off begin
	// % protected region % [Add class properties here] end

	render() {
		let contents = (
			<SecuredPage>
				<Navigation
					linkGroups={getFrontendNavLinks(this.props)}
					orientation={Orientation.VERTICAL}
					match={this.props.match}
					location={this.props.location}
					history={this.props.history}
					staticContext={this.props.staticContext}
				/>
				<div className="body-content">
					<EntityCRUD
						{...this.props}
						modelType={ArticleEntity}
						URLExtension="500103f4-3148-4e43-807d-1d331113b582"
						// % protected region % [Add props to crud component 500103f4-3148-4e43-807d-1d331113b582 here] off begin
						// % protected region % [Add props to crud component 500103f4-3148-4e43-807d-1d331113b582 here] end
					/>
					{
					// % protected region % [Add code for 09e8e49c-3930-4494-a2cf-3772db199676 here] off begin
					}
					{
					// % protected region % [Add code for 09e8e49c-3930-4494-a2cf-3772db199676 here] end
					}
					{
					// % protected region % [Add code for c9c0ad74-5237-452e-8344-175bd3e14ba0 here] off begin
					}
					{
					// % protected region % [Add code for c9c0ad74-5237-452e-8344-175bd3e14ba0 here] end
					}
					{
					// % protected region % [Add code for 2c7de149-dbe9-4aad-8cdb-1613e4212e9f here] off begin
					}
					{
					// % protected region % [Add code for 2c7de149-dbe9-4aad-8cdb-1613e4212e9f here] end
					}
					{
					// % protected region % [Add code for a5fa5373-ae06-4bc7-9298-1a2872d38f34 here] off begin
					}
					{
					// % protected region % [Add code for a5fa5373-ae06-4bc7-9298-1a2872d38f34 here] end
					}
				</div>
			</SecuredPage>
		);

		// % protected region % [Override contents here] off begin
		// % protected region % [Override contents here] end

		return contents;
	}
}

// % protected region % [Override export here] off begin
export default ArticlePage;
// % protected region % [Override export here] end
