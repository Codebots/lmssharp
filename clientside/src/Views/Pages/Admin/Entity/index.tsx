/*
 * @bot-written
 * 
 * WARNING AND NOTICE
 * Any access, download, storage, and/or use of this source code is subject to the terms and conditions of the
 * Full Software Licence as accepted by you before being granted access to this source code and other materials,
 * the terms of which can be accessed on the Codebots website at https://codebots.com/full-software-licence. Any
 * commercial use in contravention of the terms of the Full Software Licence may be pursued by Codebots through
 * licence termination and further legal action, and be required to indemnify Codebots for any loss or damage,
 * including interest and costs. You are deemed to have accepted the terms of the Full Software Licence on any
 * access, download, storage, and/or use of this source code.
 * 
 * BOT WARNING
 * This file is bot-written.
 * Any changes out side of "protected regions" will be lost next time the bot makes any changes.
 */
export { default as AdministratorEntityPage } from './AdministratorEntityPage';
export { default as ArticleEntityPage } from './ArticleEntityPage';
export { default as BookEntityPage } from './BookEntityPage';
export { default as ContentFileEntityPage } from './ContentFileEntityPage';
export { default as CourseEntityPage } from './CourseEntityPage';
export { default as CourseCategoryEntityPage } from './CourseCategoryEntityPage';
export { default as CourseLessonsEntityPage } from './CourseLessonsEntityPage';
export { default as LessonEntityPage } from './LessonEntityPage';
export { default as LessonSubmissionEntityPage } from './LessonSubmissionEntityPage';
export { default as TagEntityPage } from './TagEntityPage';
export { default as UserEntityPage } from './UserEntityPage';
export { default as WorkflowEntityPage } from './WorkflowEntityPage';
export { default as WorkflowStateEntityPage } from './WorkflowStateEntityPage';
export { default as WorkflowTransitionEntityPage } from './WorkflowTransitionEntityPage';
export { default as WorkflowVersionEntityPage } from './WorkflowVersionEntityPage';
export { default as LessonEntityFormTileEntityPage } from './LessonEntityFormTileEntityPage';
export { default as ArticleTimelineEventsEntityPage } from './ArticleTimelineEventsEntityPage';
// % protected region % [Add any further admin page exports here] off begin
// % protected region % [Add any further admin page exports here] end